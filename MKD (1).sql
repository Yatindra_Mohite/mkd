-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 19, 2018 at 06:19 AM
-- Server version: 5.7.22
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MKD`
--

-- --------------------------------------------------------

--
-- Table structure for table `AdminOffer`
--

CREATE TABLE `AdminOffer` (
  `Id` int(11) NOT NULL,
  `OfferTitle` varchar(30) NOT NULL,
  `Icon` varchar(500) NOT NULL,
  `Description` varchar(256) NOT NULL,
  `Terms` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `Status` int(11) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  `OrgId` int(11) NOT NULL,
  `StoreId` int(11) NOT NULL,
  `AutoApply` int(11) NOT NULL,
  `MaxAvailLimit` int(11) NOT NULL,
  `MaxAmount` decimal(10,0) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `ProductCategoryId` int(11) NOT NULL,
  `ProductSubCategoryId` int(11) NOT NULL,
  `OfferType` varchar(30) NOT NULL,
  `OfferValue` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AdminSetting`
--

CREATE TABLE `AdminSetting` (
  `Id` int(11) NOT NULL,
  `ReferralAmount` decimal(10,0) NOT NULL,
  `ReferralExpiryDays` int(11) NOT NULL,
  `ReferralEnable` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AdminSetting`
--

INSERT INTO `AdminSetting` (`Id`, `ReferralAmount`, `ReferralExpiryDays`, `ReferralEnable`) VALUES
(1, '100', 10, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `AppVersion`
--

CREATE TABLE `AppVersion` (
  `Id` int(10) NOT NULL,
  `CurrentVersion` int(10) NOT NULL,
  `MinVersion` int(10) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AppVersion`
--

INSERT INTO `AppVersion` (`Id`, `CurrentVersion`, `MinVersion`, `CreateOn`) VALUES
(1, 1, 1, '2018-05-02 00:00:00'),
(2, 2, 1, '2018-05-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `Area`
--

CREATE TABLE `Area` (
  `id` int(11) NOT NULL,
  `StateId` int(10) NOT NULL,
  `CityId` int(10) NOT NULL,
  `AreaName` varchar(100) NOT NULL,
  `CreateOn` date NOT NULL,
  `UpdateOn` date NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Area`
--

INSERT INTO `Area` (`id`, `StateId`, `CityId`, `AreaName`, `CreateOn`, `UpdateOn`, `Status`) VALUES
(1, 1, 1, 'Vijay Nagar', '2018-05-07', '2018-05-07', 1),
(2, 1, 1, 'Geeta Bhawan', '2018-05-07', '2018-05-07', 1),
(3, 1, 2, 'MahakalNagar', '2018-05-07', '2018-05-07', 1),
(4, 1, 2, 'Vishnupuri', '2018-05-07', '2018-05-07', 1),
(5, 2, 3, 'Baliya', '2018-05-07', '2018-05-07', 1),
(6, 2, 4, 'Bhind', '2018-05-07', '2018-05-07', 1),
(7, 2, 4, 'Murena', '2018-05-07', '2018-05-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `City`
--

CREATE TABLE `City` (
  `id` int(11) NOT NULL,
  `CityName` varchar(100) NOT NULL,
  `StateId` int(11) NOT NULL,
  `CreateOn` date NOT NULL,
  `UpdateOn` date NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `City`
--

INSERT INTO `City` (`id`, `CityName`, `StateId`, `CreateOn`, `UpdateOn`, `Status`) VALUES
(1, 'Indore', 1, '2018-05-07', '2018-05-07', 1),
(2, 'Ujjain', 1, '2018-05-07', '2018-05-07', 1),
(3, 'Bihar', 2, '2018-05-07', '2018-05-07', 1),
(4, 'Kaanpur', 2, '2018-05-07', '2018-05-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ContactUs`
--

CREATE TABLE `ContactUs` (
  `Id` int(11) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `Email` varchar(256) NOT NULL,
  `ContactNum` varchar(20) NOT NULL,
  `Message` varchar(1000) NOT NULL,
  `AddedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ContactUs`
--

INSERT INTO `ContactUs` (`Id`, `Name`, `Email`, `ContactNum`, `Message`, `AddedOn`) VALUES
(1, 'Mann ka dabba', 'mannkadabba@gmail.com', '+911234567890', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `FAQ`
--

CREATE TABLE `FAQ` (
  `Id` int(11) NOT NULL,
  `Question` varchar(256) NOT NULL,
  `Answer` varchar(2000) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `Sequence` int(11) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Feedback`
--

CREATE TABLE `Feedback` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `ProductId` int(11) DEFAULT NULL,
  `StoreId` int(11) NOT NULL,
  `Message` varchar(1000) NOT NULL,
  `Rating` decimal(10,0) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `IsPrivate` int(11) NOT NULL,
  `OrderId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LocalArea`
--

CREATE TABLE `LocalArea` (
  `LocalAreaId` int(10) NOT NULL,
  `AreaId` int(10) NOT NULL,
  `LocalAreaName` varchar(300) NOT NULL,
  `Status` int(1) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `UpdateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `LocalArea`
--

INSERT INTO `LocalArea` (`LocalAreaId`, `AreaId`, `LocalAreaName`, `Status`, `CreateOn`, `UpdateOn`) VALUES
(1, 0, 'OTHER', 1, '2018-06-02 00:00:00', '2018-06-02 00:00:00'),
(2, 1, 'Malviya Nagar', 1, '2018-06-02 00:00:00', '2018-06-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `MkdValidityPlan`
--

CREATE TABLE `MkdValidityPlan` (
  `ValidityId` int(10) NOT NULL,
  `MinAmount` varchar(20) NOT NULL,
  `MaxAmount` varchar(20) NOT NULL,
  `ValidityDays` varchar(10) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `UpdateOn` datetime NOT NULL,
  `Status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MkdValidityPlan`
--

INSERT INTO `MkdValidityPlan` (`ValidityId`, `MinAmount`, `MaxAmount`, `ValidityDays`, `CreateOn`, `UpdateOn`, `Status`) VALUES
(1, '250', '250', '3', '2018-05-23 00:00:00', '2018-06-21 00:00:00', b'1'),
(2, '500', '500', '7', '2018-05-23 00:00:00', '2018-06-12 00:00:00', b'1'),
(3, '250', '', '3', '2018-05-23 00:00:00', '2018-05-23 00:00:00', b'1'),
(4, '750', '750', '9', '0000-00-00 00:00:00', '2018-06-12 00:00:00', b'1'),
(5, '1000', '1000', '12', '2018-05-24 00:00:00', '2018-06-12 00:00:00', b'1'),
(6, '1250', '1250', '15', '2018-05-24 00:00:00', '2018-06-12 00:00:00', b'1'),
(7, '1500', '1500', '18', '2018-05-24 00:00:00', '2018-06-12 00:00:00', b'1'),
(8, '1750', '1750', '24', '2018-05-24 00:00:00', '2018-06-21 00:00:00', b'1'),
(11, '4001', '4500', '50', '2018-05-29 00:00:00', '2018-05-29 00:00:00', b'1'),
(12, '4501', '5000', '55', '2018-05-29 00:00:00', '2018-05-29 00:00:00', b'1'),
(13, '2000', '2000', '25', '2018-06-12 00:00:00', '2018-06-12 00:00:00', b'1'),
(14, '2250', '2250', '29', '2018-06-12 00:00:00', '2018-06-12 00:00:00', b'1'),
(15, '2000', '2000', '28', '2018-06-21 00:00:00', '2018-06-21 00:00:00', b'1'),
(16, '2000', '2000', '28', '2018-06-21 00:00:00', '2018-06-21 00:00:00', b'1'),
(17, '5000', '5000', '90', '2018-06-21 00:00:00', '2018-06-28 00:00:00', b'1'),
(18, '2000', '2000', '28', '2018-06-21 00:00:00', '2018-06-21 00:00:00', b'1'),
(19, '1500', '2000', '30', '2018-06-22 00:00:00', '2018-06-22 00:00:00', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `noti_id` int(11) NOT NULL,
  `sender_id` int(10) NOT NULL,
  `receiver_id` int(10) NOT NULL,
  `noti_type` int(1) NOT NULL COMMENT '1=low bal, 2=offer, 3=order delivered ',
  `data` text NOT NULL,
  `rate_review` varchar(200) NOT NULL,
  `msg` varchar(200) NOT NULL,
  `order_type` varchar(15) NOT NULL,
  `amount` varchar(10) NOT NULL,
  `create_at` varchar(100) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`noti_id`, `sender_id`, `receiver_id`, `noti_type`, `data`, `rate_review`, `msg`, `order_type`, `amount`, `create_at`, `status`) VALUES
(1, 0, 3, 1, '', '', 'Your wallet balance is low.', '', '', '1529577502832', 1),
(2, 0, 3, 2, '', '', 'New offers available for you.', '', '', '1529577502833', 1),
(3, 0, 3, 3, '[{"count":"2","rate":"5","name":"Tuvar dal"},{"count":"2","rate":"3","name":"Roti"}]', 'nice order', '', 'Breakfast', '750.00', '1529577502834', 1);

-- --------------------------------------------------------

--
-- Table structure for table `OrderItems`
--

CREATE TABLE `OrderItems` (
  `Id` int(11) NOT NULL,
  `UserId` int(10) NOT NULL,
  `CategoryId` int(10) NOT NULL,
  `WeekDay` int(1) NOT NULL,
  `DayTime` int(1) NOT NULL,
  `SubCategoryId` int(10) NOT NULL,
  `Quantity` int(50) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `UpdateOn` datetime NOT NULL,
  `food_status` int(1) NOT NULL COMMENT '1=self,2=admin,0=no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `OrderItems`
--

INSERT INTO `OrderItems` (`Id`, `UserId`, `CategoryId`, `WeekDay`, `DayTime`, `SubCategoryId`, `Quantity`, `CreateOn`, `UpdateOn`, `food_status`) VALUES
(1, 9, 5, 5, 1, 17, 2, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 1),
(2, 9, 6, 5, 1, 23, 1, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 1),
(3, 9, 1, 5, 2, 1, 1, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 1),
(4, 9, 2, 5, 3, 11, 4, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 1),
(5, 9, 3, 5, 3, 4, 1, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 1),
(6, 9, 4, 5, 3, 18, 2, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 1),
(7, 9, 2, 5, 3, 11, 1, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 1),
(8, 9, 2, 5, 2, 0, 0, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 2),
(9, 9, 3, 5, 2, 5, 2, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 2),
(10, 9, 4, 5, 2, 0, 0, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 2),
(11, 9, 1, 5, 3, 0, 0, '2018-07-13 15:59:39', '2018-07-13 15:59:39', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Page_content`
--

CREATE TABLE `Page_content` (
  `id` int(11) NOT NULL,
  `page_title` varchar(50) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Page_content`
--

INSERT INTO `Page_content` (`id`, `page_title`, `content`) VALUES
(1, 'Privacy policy', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.'),
(2, 'Tersms&condition', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.'),
(3, 'About us', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.');

-- --------------------------------------------------------

--
-- Table structure for table `PauseResume`
--

CREATE TABLE `PauseResume` (
  `PRId` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `PauseDate` varchar(20) NOT NULL,
  `ResumeDate` varchar(20) NOT NULL,
  `pause_date` datetime NOT NULL,
  `resume_date` datetime NOT NULL,
  `Status` int(1) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PauseResume`
--

INSERT INTO `PauseResume` (`PRId`, `user_id`, `PauseDate`, `ResumeDate`, `pause_date`, `resume_date`, `Status`, `CreateOn`) VALUES
(1, 2, '30-MAY-2018', '02-JUN-2018', '2018-05-30 00:00:00', '2018-06-02 00:00:00', 0, '2018-05-28 19:29:15'),
(2, 3, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '2018-06-12 18:02:17'),
(3, 20, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '2018-06-26 17:51:12'),
(4, 27, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '2018-05-31 11:31:26'),
(5, 40, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '2018-06-07 12:43:38'),
(6, 43, '08-JUN-2018', '09-JUN-2018', '2018-06-08 00:00:00', '2018-06-09 00:00:00', 0, '2018-06-07 13:56:54'),
(7, 44, '21-JUN-2018', '29-JUN-2018', '2018-06-21 00:00:00', '2018-06-29 00:00:00', 0, '2018-06-07 17:47:33'),
(8, 46, '09-JUN-2018', '10-JUN-2018', '2018-06-09 00:00:00', '2018-06-10 00:00:00', 0, '2018-06-08 13:00:39'),
(9, 49, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '2018-06-08 17:44:57'),
(10, 52, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '2018-06-12 12:59:34'),
(11, 59, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '2018-06-20 15:34:37'),
(12, 50, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '2018-06-20 16:06:21'),
(13, 60, '22-JUN-2018', '', '2018-06-22 00:00:00', '1970-01-01 00:00:00', 0, '2018-06-21 12:00:08'),
(14, 6, '11-JUL-2018', '26-JUL-2018', '2018-07-11 00:00:00', '2018-07-26 00:00:00', 0, '2018-07-02 21:03:58');

-- --------------------------------------------------------

--
-- Table structure for table `Product`
--

CREATE TABLE `Product` (
  `Id` int(11) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `ProductAbbr` varchar(20) NOT NULL,
  `ProductCode` varchar(20) NOT NULL,
  `IsFeatured` int(11) NOT NULL,
  `SEOContent` varchar(500) DEFAULT NULL,
  `ProductUrl` varchar(256) DEFAULT NULL,
  `Logo` varchar(256) NOT NULL,
  `Banner` varchar(256) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `AddedOn` datetime NOT NULL,
  `StoreId` int(11) DEFAULT NULL,
  `Amount` decimal(10,0) NOT NULL,
  `TakeOutAmount` decimal(10,0) NOT NULL,
  `UserId` int(11) DEFAULT NULL,
  `IsMaster` bit(1) NOT NULL DEFAULT b'1' COMMENT 'If Master, then thali  by admin',
  `Description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Product`
--

INSERT INTO `Product` (`Id`, `CategoryId`, `Name`, `ProductAbbr`, `ProductCode`, `IsFeatured`, `SEOContent`, `ProductUrl`, `Logo`, `Banner`, `Status`, `AddedOn`, `StoreId`, `Amount`, `TakeOutAmount`, `UserId`, `IsMaster`, `Description`) VALUES
(1, 1, 'Daal Makhani', '', '', 0, NULL, NULL, '', '', 1, '2018-06-16 15:30:53', NULL, '100', '0', NULL, b'0', 'Full plate'),
(2, 1, 'Daal Tadka', '', '', 0, NULL, NULL, '', '', 1, '2018-05-08 13:19:43', NULL, '80', '0', NULL, b'0', 'Tadka daal'),
(3, 3, 'Jira rice', '', '', 0, NULL, NULL, '', '', 1, '2018-06-16 16:07:50', NULL, '120', '0', NULL, b'0', 'full jira'),
(4, 3, 'Plain rice', '', '', 0, NULL, NULL, '', '', 1, '2018-06-16 16:09:57', NULL, '90', '0', NULL, b'0', 'small size plate 1'),
(5, 1, 'Daal kadhayi', '', '', 0, NULL, NULL, '', '', 1, '2018-05-09 11:33:15', NULL, '210', '0', NULL, b'1', 'nice for all'),
(6, 1, 'Plain daal', '', '', 0, NULL, NULL, '', '', 1, '2018-05-09 11:33:49', NULL, '150', '0', NULL, b'1', 'tuar daal'),
(7, 1, 'Rosted Daal', '', '', 0, NULL, NULL, '', '', 1, '2018-06-20 12:18:18', NULL, '80', '0', NULL, b'1', 'Roasted daal 1'),
(11, 2, 'Plain Roti', '', '', 0, '', '', '', '', 1, '2018-06-16 15:15:59', 0, '3', '0', 0, b'1', '2 per roti'),
(12, 2, 'Butter Roti', '', '', 0, '', '', '', '', 1, '2018-06-16 15:36:59', 0, '5', '0', 0, b'1', 'per roti'),
(13, 2, 'Naan', '', '', 0, '', '', '', '', 1, '2018-06-16 16:15:19', 0, '12', '0', 0, b'1', 'per naan 12 rs'),
(14, 2, 'Paratha', '', '', 0, '', '', '', '', 1, '2018-06-16 15:34:57', 0, '16', '0', 0, b'1', 'half '),
(15, 5, 'Poha', '', '', 0, '', '', '', '', 1, '2018-06-05 12:44:32', 0, '5', '0', 0, b'1', '1 plate'),
(16, 5, 'Upma', '', '', 0, '', '', '', '', 1, '2018-06-05 12:44:32', 0, '10', '0', 0, b'1', '1 plate'),
(17, 5, 'Khamand', '', '', 0, '', '', '', '', 1, '2018-06-18 06:09:02', 0, '10', '0', 0, b'1', '1 pkt'),
(18, 4, 'Aalu Gobhi', '', '', 0, '', '', '', '', 1, '2018-06-12 15:11:13', 0, '25', '0', 0, b'1', 'one plate'),
(19, 4, 'bhindi', '', '', 0, '', '', '', '', 1, '2018-06-05 12:46:32', 0, '35', '0', 0, b'1', 'one plate'),
(20, 4, 'paneer', '', '', 0, '', '', '', '', 1, '2018-06-05 12:46:32', 0, '80', '0', 0, b'1', 'one plate'),
(21, 6, 'Ice-cream', '', '', 0, '', '', '', '', 1, '2018-06-16 15:57:39', 0, '120', '0', 0, b'1', 'family pack'),
(22, 6, 'Onion', '', '', 0, '', '', '', '', 1, '2018-06-11 07:52:07', 0, '10', '0', 0, b'1', '1 plate'),
(23, 6, 'Green Salad', '', '', 0, '', '', '', '', 1, '2018-06-11 07:52:07', 0, '20', '0', 0, b'1', 'one plate'),
(24, 8, 'Bundi Raita', '', '', 0, '', '', '', '', 1, '2018-06-21 07:53:53', 0, '20', '0', 0, b'1', 'full '),
(25, 7, 'Veg Soup', '', '', 0, '', '', '', '', 1, '2018-06-21 08:10:28', 0, '35', '0', 0, b'1', 'full plate'),
(26, 6, 'kulfi', '', '', 0, '', '', '', '', 1, '2018-06-22 12:40:42', 0, '40', '0', 0, b'1', 'kesar rabri'),
(27, 1, 'Daal Makhani', '', '', 0, '', '', '', '', 1, '2018-06-25 13:11:15', 0, '50', '0', 0, b'1', 'Dal makhani');

-- --------------------------------------------------------

--
-- Table structure for table `ProductCategory`
--

CREATE TABLE `ProductCategory` (
  `Id` int(11) NOT NULL,
  `CategoryName` varchar(50) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `Sequence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ProductCategory`
--

INSERT INTO `ProductCategory` (`Id`, `CategoryName`, `AddedOn`, `Sequence`) VALUES
(1, 'Daal', '2018-06-14 00:00:00', 1),
(2, 'Roti', '2018-05-08 00:00:00', 2),
(3, 'Rice', '2018-05-08 00:00:00', 3),
(4, 'Vegetables', '2018-06-25 00:00:00', 4),
(5, 'Breakfast -Menu', '2018-06-20 00:00:00', 5),
(6, 'TopUp', '2018-06-11 00:00:00', 6),
(7, 'Soup', '2018-06-20 00:00:00', 6),
(8, 'Raita', '2018-06-20 00:00:00', 8);

-- --------------------------------------------------------

--
-- Table structure for table `ProductImages`
--

CREATE TABLE `ProductImages` (
  `Id` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `Title` varchar(256) DEFAULT NULL,
  `ContentType` varchar(50) DEFAULT NULL,
  `FileUrl` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Promotions`
--

CREATE TABLE `Promotions` (
  `Id` int(11) NOT NULL,
  `Title` varchar(256) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `Status` int(1) NOT NULL,
  `BannerUrl` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Promotions`
--

INSERT INTO `Promotions` (`Id`, `Title`, `AddedOn`, `Status`, `BannerUrl`) VALUES
(1, '', '2018-05-23 00:00:00', 1, 'e2d9f7fa749302209ac982060eaedae0.jpg'),
(2, '', '2018-05-23 00:00:00', 1, '8d3bb93db30721ec0305c2fba5e5ae99.jpeg'),
(3, '', '2018-05-23 00:00:00', 1, '345643dc9d5a4c94ff5eaa1d50f58597.jpg'),
(4, '', '2018-05-23 00:00:00', 1, 'e2d9f7fa749302209ac982060eaedae01.jpg'),
(5, '', '2018-05-23 00:00:00', 1, '8d3bb93db30721ec0305c2fba5e5ae991.jpeg'),
(6, '', '2018-05-23 00:00:00', 1, '345643dc9d5a4c94ff5eaa1d50f585971.jpg'),
(7, '', '2018-06-20 00:00:00', 1, '08efaffa87fe1f2789da82930fd8233d.jpg'),
(8, '', '2018-07-03 00:00:00', 1, 'dd077b183c2a3bf56c71b1b654dac04b.jpg'),
(9, '', '2018-07-03 00:00:00', 1, 'b527a7a688f0ec365efc40d5f45729f8.png');

-- --------------------------------------------------------

--
-- Table structure for table `ReferralByDeviceId`
--

CREATE TABLE `ReferralByDeviceId` (
  `Id` int(10) NOT NULL,
  `Refer_user_id` int(10) NOT NULL,
  `Referral_user_id` int(10) NOT NULL,
  `Device_id` varchar(100) NOT NULL DEFAULT '',
  `Refer_Code` varchar(10) NOT NULL,
  `Refer_Amount` varchar(20) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ReferralByDeviceId`
--

INSERT INTO `ReferralByDeviceId` (`Id`, `Refer_user_id`, `Referral_user_id`, `Device_id`, `Refer_Code`, `Refer_Amount`, `CreateOn`) VALUES
(1, 3, 4, '11111111111111111', 'MKD189521', '40', '2018-05-23 16:26:18'),
(2, 20, 21, '9ae99f901a195166', 'MKD41104', '286.5', '2018-05-30 18:09:14'),
(3, 21, 22, '9ae99f901a195166', 'MKD96615', '10', '2018-05-30 18:23:03'),
(4, 22, 23, '9ae99f901a195166', 'MKD68260', '10', '2018-05-30 18:59:13'),
(5, 19, 27, '9ae99f901a195166', 'MKD37930', '10', '2018-05-31 10:53:30'),
(6, 27, 28, '9ae99f901a195166', 'MKD24732', '10', '2018-05-31 11:13:18'),
(7, 19, 29, 'e8327967ec53e729', 'MKD37930', '10', '2018-05-31 11:19:59'),
(8, 31, 32, 'e8327967ec53e729', 'MKD52147', '10', '2018-05-31 12:57:03'),
(9, 40, 41, 'e8327967ec53e729', 'MKD98103', '10', '2018-06-07 12:50:28'),
(10, 43, 44, 'e8327967ec53e729', 'MKD20125', '10', '2018-06-07 13:05:42'),
(11, 46, 47, 'e8327967ec53e729', 'MKD29150', '10', '2018-06-08 13:14:19'),
(12, 47, 49, 'e8327967ec53e729', 'MKD52868', '77.8', '2018-06-08 13:21:26'),
(13, 53, 54, 'ae15f9792808c9af', 'mkd40297', '500', '2018-06-11 22:22:31'),
(14, 52, 56, 'c7085d31f6ae9520', 'MKD97165', '112.9', '2018-06-11 22:29:17');

-- --------------------------------------------------------

--
-- Table structure for table `State`
--

CREATE TABLE `State` (
  `id` int(11) NOT NULL,
  `StateName` varchar(100) NOT NULL,
  `CreateOn` date NOT NULL,
  `UpdateOn` date NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `State`
--

INSERT INTO `State` (`id`, `StateName`, `CreateOn`, `UpdateOn`, `Status`) VALUES
(1, 'Madhya Pradesh', '2018-05-07', '2018-05-07', 1),
(2, 'Uttar Pradesh', '2018-05-07', '2018-05-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `SubscriptionPlan`
--

CREATE TABLE `SubscriptionPlan` (
  `id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `WeedDay` int(1) NOT NULL,
  `MainCategory` int(1) NOT NULL COMMENT '1=break fast, 2=lunch, 3=dinner',
  `Price` varchar(20) NOT NULL,
  `ValidTillMonthCount` varchar(20) NOT NULL,
  `ProductCategory` text NOT NULL,
  `ProductCategoryQty` varchar(250) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `Status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SubscriptionPlan`
--

INSERT INTO `SubscriptionPlan` (`id`, `Name`, `WeedDay`, `MainCategory`, `Price`, `ValidTillMonthCount`, `ProductCategory`, `ProductCategoryQty`, `CreateOn`, `Status`) VALUES
(1, 'STANDARD MEAL', 0, 2, '1200', '1', 'Daal,Rice', '2,1', '2018-05-15 08:14:14', 1),
(2, 'EXECUTIVE MEAL', 0, 2, '1250', '2', 'Daal', '1', '2018-05-15 08:15:12', 1),
(3, 'PREMIUM THALI', 0, 2, '1500', '1', 'Daal,Rice', '1,2', '2018-05-15 08:16:04', 1),
(4, 'STANDARD MEAL', 0, 3, '1200', '1', 'Daal,Rice', '1,1', '2018-05-15 14:15:31', 1),
(5, 'EXECUTIVE MEAL', 0, 3, '1250', '1', 'Daal,Rice,Roti', '2,0,1', '2018-05-18 13:39:53', 1),
(6, 'PREMIUM THALI', 0, 3, '1500', '1', 'Vegitables,Daal,Rice,Roti', '2,1,1,0', '2018-05-18 13:40:51', 1),
(7, '', 0, 1, '850', '1', '', '', '2018-05-18 14:03:50', 1),
(8, '', 0, 2, '60', '1', '1', '1', '2018-06-12 15:08:50', 1),
(9, 'Bumper Offer', 0, 2, '800', '2', '4,1', '2,2', '2018-06-21 07:35:34', 1),
(10, 'Lunch', 0, 2, '65', '1', '1,2,3,4', '1,4,1,1', '2018-06-22 08:53:20', 1),
(11, '', 0, 1, '0', '1', '5', '1', '2018-06-22 08:54:28', 1),
(12, 'Standaard Thali', 0, 1, '60', '1', '1,2,4,3', '1,4,1,1', '2018-06-25 13:15:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Tax`
--

CREATE TABLE `Tax` (
  `TaxId` int(2) NOT NULL,
  `TaxValue` varchar(10) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `UpdateOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Tax`
--

INSERT INTO `Tax` (`TaxId`, `TaxValue`, `CreateOn`, `UpdateOn`) VALUES
(1, '5', '2018-06-26 00:00:00', '2018-06-26 06:53:12');

-- --------------------------------------------------------

--
-- Table structure for table `TiffenProductSchedule`
--

CREATE TABLE `TiffenProductSchedule` (
  `Id` int(11) NOT NULL,
  `WeekDay` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `IsInBreakFast` bit(1) NOT NULL,
  `IsInLunch` bit(1) NOT NULL,
  `IsInDinner` bit(1) NOT NULL,
  `IsEnabled` bit(1) NOT NULL,
  `AddedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Admin Thali';

--
-- Dumping data for table `TiffenProductSchedule`
--

INSERT INTO `TiffenProductSchedule` (`Id`, `WeekDay`, `ProductId`, `IsInBreakFast`, `IsInLunch`, `IsInDinner`, `IsEnabled`, `AddedOn`) VALUES
(3, 1, 7, b'0', b'1', b'0', b'1', '2018-05-09 12:51:02'),
(4, 1, 5, b'0', b'1', b'0', b'1', '2018-05-09 12:51:02'),
(5, 1, 4, b'0', b'1', b'0', b'1', '2018-05-09 12:51:02'),
(6, 1, 3, b'0', b'1', b'0', b'1', '2018-05-09 12:51:02'),
(7, 1, 1, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(8, 1, 6, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(9, 1, 2, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(10, 1, 7, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(11, 1, 3, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(12, 1, 4, b'0', b'0', b'1', b'1', '2018-05-09 12:51:14'),
(13, 1, 15, b'1', b'0', b'0', b'1', '2018-06-05 12:47:18'),
(14, 2, 15, b'1', b'0', b'0', b'1', '2018-06-05 12:47:43'),
(15, 2, 16, b'1', b'0', b'0', b'1', '2018-06-05 12:47:43'),
(16, 3, 16, b'1', b'0', b'0', b'1', '2018-06-05 12:47:54'),
(17, 4, 17, b'1', b'0', b'0', b'1', '2018-06-05 12:48:04'),
(18, 4, 15, b'1', b'0', b'0', b'1', '2018-06-05 12:48:04'),
(19, 5, 17, b'1', b'0', b'0', b'1', '2018-06-05 12:48:14'),
(20, 6, 15, b'1', b'0', b'0', b'1', '2018-06-05 12:48:24'),
(21, 6, 16, b'1', b'0', b'0', b'1', '2018-06-05 12:48:24'),
(22, 7, 17, b'1', b'0', b'0', b'1', '2018-06-05 12:48:33'),
(23, 7, 15, b'1', b'0', b'0', b'1', '2018-06-05 12:48:33'),
(24, 7, 16, b'1', b'0', b'0', b'1', '2018-06-05 12:48:33'),
(25, 2, 1, b'0', b'1', b'0', b'1', '2018-06-05 12:50:34'),
(26, 2, 11, b'0', b'1', b'0', b'1', '2018-06-05 12:50:34'),
(27, 2, 3, b'0', b'1', b'0', b'1', '2018-06-05 12:50:34'),
(28, 2, 18, b'0', b'1', b'0', b'1', '2018-06-05 12:50:34'),
(29, 3, 1, b'0', b'1', b'0', b'1', '2018-06-05 12:50:58'),
(30, 3, 2, b'0', b'1', b'0', b'1', '2018-06-05 12:50:58'),
(31, 3, 12, b'0', b'1', b'0', b'1', '2018-06-05 12:50:58'),
(32, 3, 4, b'0', b'1', b'0', b'1', '2018-06-05 12:50:58'),
(33, 3, 18, b'0', b'1', b'0', b'1', '2018-06-05 12:50:58'),
(34, 3, 19, b'0', b'1', b'0', b'1', '2018-06-05 12:50:58'),
(35, 4, 6, b'0', b'1', b'0', b'1', '2018-06-05 12:51:19'),
(36, 4, 2, b'0', b'1', b'0', b'1', '2018-06-05 12:51:19'),
(37, 4, 13, b'0', b'1', b'0', b'1', '2018-06-05 12:51:19'),
(38, 4, 11, b'0', b'1', b'0', b'1', '2018-06-05 12:51:19'),
(39, 4, 3, b'0', b'1', b'0', b'1', '2018-06-05 12:51:19'),
(40, 4, 4, b'0', b'1', b'0', b'1', '2018-06-05 12:51:19'),
(41, 4, 19, b'0', b'1', b'0', b'1', '2018-06-05 12:51:19'),
(42, 4, 20, b'0', b'1', b'0', b'1', '2018-06-05 12:51:19'),
(43, 5, 1, b'0', b'1', b'0', b'1', '2018-06-05 12:51:32'),
(44, 5, 11, b'0', b'1', b'0', b'1', '2018-06-05 12:51:32'),
(45, 5, 3, b'0', b'1', b'0', b'1', '2018-06-05 12:51:32'),
(46, 5, 18, b'0', b'1', b'0', b'1', '2018-06-05 12:51:32'),
(47, 5, 19, b'0', b'1', b'0', b'1', '2018-06-05 12:51:32'),
(48, 6, 1, b'0', b'1', b'0', b'1', '2018-06-05 12:53:47'),
(49, 6, 12, b'0', b'1', b'0', b'1', '2018-06-05 12:53:47'),
(50, 6, 4, b'0', b'1', b'0', b'1', '2018-06-05 12:53:47'),
(51, 6, 20, b'0', b'1', b'0', b'1', '2018-06-05 12:53:47'),
(52, 7, 6, b'0', b'1', b'0', b'1', '2018-06-05 12:54:18'),
(53, 7, 2, b'0', b'1', b'0', b'1', '2018-06-05 12:54:18'),
(54, 7, 1, b'0', b'1', b'0', b'1', '2018-06-05 12:54:18'),
(55, 7, 13, b'0', b'1', b'0', b'1', '2018-06-05 12:54:18'),
(56, 7, 11, b'0', b'1', b'0', b'1', '2018-06-05 12:54:18'),
(57, 7, 12, b'0', b'1', b'0', b'1', '2018-06-05 12:54:18'),
(58, 7, 3, b'0', b'1', b'0', b'1', '2018-06-05 12:54:18'),
(59, 7, 4, b'0', b'1', b'0', b'1', '2018-06-05 12:54:18'),
(60, 7, 18, b'0', b'1', b'0', b'1', '2018-06-05 12:54:18'),
(61, 7, 19, b'0', b'1', b'0', b'1', '2018-06-05 12:54:18'),
(62, 2, 1, b'0', b'0', b'1', b'1', '2018-06-05 13:08:25'),
(63, 2, 11, b'0', b'0', b'1', b'1', '2018-06-05 13:08:25'),
(64, 2, 3, b'0', b'0', b'1', b'1', '2018-06-05 13:08:25'),
(65, 2, 18, b'0', b'0', b'1', b'1', '2018-06-05 13:08:25'),
(66, 2, 5, b'0', b'0', b'1', b'1', '2018-06-05 13:09:15'),
(68, 2, 12, b'0', b'0', b'1', b'1', '2018-06-05 13:09:15'),
(70, 2, 19, b'0', b'0', b'1', b'1', '2018-06-05 13:09:15'),
(71, 3, 2, b'0', b'0', b'1', b'1', '2018-06-05 13:10:49'),
(72, 3, 1, b'0', b'0', b'1', b'1', '2018-06-05 13:10:49'),
(73, 3, 13, b'0', b'0', b'1', b'1', '2018-06-05 13:10:49'),
(74, 3, 4, b'0', b'0', b'1', b'1', '2018-06-05 13:10:49'),
(75, 3, 19, b'0', b'0', b'1', b'1', '2018-06-05 13:10:49'),
(76, 4, 5, b'0', b'0', b'1', b'1', '2018-06-05 13:11:06'),
(77, 4, 12, b'0', b'0', b'1', b'1', '2018-06-05 13:11:06'),
(78, 5, 7, b'0', b'0', b'1', b'1', '2018-06-05 13:11:20'),
(79, 5, 11, b'0', b'0', b'1', b'1', '2018-06-05 13:11:20'),
(80, 5, 14, b'0', b'0', b'1', b'1', '2018-06-05 13:11:20'),
(81, 5, 4, b'0', b'0', b'1', b'1', '2018-06-05 13:11:20'),
(82, 5, 18, b'0', b'0', b'1', b'1', '2018-06-05 13:11:20'),
(83, 5, 19, b'0', b'0', b'1', b'1', '2018-06-05 13:11:20'),
(84, 6, 1, b'0', b'0', b'1', b'1', '2018-06-05 13:11:45'),
(85, 6, 11, b'0', b'0', b'1', b'1', '2018-06-05 13:11:45'),
(86, 6, 3, b'0', b'0', b'1', b'1', '2018-06-05 13:11:45'),
(87, 6, 18, b'0', b'0', b'1', b'1', '2018-06-05 13:11:45'),
(88, 7, 2, b'0', b'0', b'1', b'1', '2018-06-05 13:11:59'),
(89, 7, 5, b'0', b'0', b'1', b'1', '2018-06-05 13:11:59'),
(90, 7, 1, b'0', b'0', b'1', b'1', '2018-06-05 13:11:59'),
(91, 7, 11, b'0', b'0', b'1', b'1', '2018-06-05 13:11:59'),
(92, 7, 12, b'0', b'0', b'1', b'1', '2018-06-05 13:11:59'),
(93, 7, 3, b'0', b'0', b'1', b'1', '2018-06-05 13:11:59'),
(94, 7, 4, b'0', b'0', b'1', b'1', '2018-06-05 13:11:59'),
(95, 7, 19, b'0', b'0', b'1', b'1', '2018-06-05 13:11:59'),
(96, 7, 20, b'0', b'0', b'1', b'1', '2018-06-05 13:11:59'),
(97, 1, 17, b'1', b'0', b'0', b'1', '2018-06-12 15:12:59'),
(99, 1, 16, b'1', b'0', b'0', b'1', '2018-06-12 15:12:59'),
(100, 1, 7, b'1', b'0', b'0', b'1', '2018-06-25 13:13:05'),
(101, 1, 5, b'1', b'0', b'0', b'1', '2018-06-25 13:13:05'),
(102, 1, 1, b'1', b'0', b'0', b'1', '2018-06-25 13:13:05');

-- --------------------------------------------------------

--
-- Table structure for table `TiffenTiming`
--

CREATE TABLE `TiffenTiming` (
  `Id` int(11) NOT NULL,
  `DayTime` int(1) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `Image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TiffenTiming`
--

INSERT INTO `TiffenTiming` (`Id`, `DayTime`, `StartTime`, `EndTime`, `Image`) VALUES
(1, 1, '09:00:00', '11:30:00', 'e2d9f7fa749302209ac982060eaedae0.jpg'),
(2, 2, '12:15:00', '15:00:00', '8d3bb93db30721ec0305c2fba5e5ae99.jpeg'),
(3, 3, '19:00:00', '21:30:00', '345643dc9d5a4c94ff5eaa1d50f58597.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `TiffinStartEndStatus`
--

CREATE TABLE `TiffinStartEndStatus` (
  `Id` int(10) NOT NULL,
  `UserId` int(10) NOT NULL,
  `WeekDay` int(1) NOT NULL,
  `BreakFast` int(1) NOT NULL COMMENT '1=on',
  `Lunch` int(1) NOT NULL COMMENT '1=on',
  `Dinner` int(1) NOT NULL COMMENT '1=on',
  `Address1` int(10) NOT NULL,
  `Address2` int(10) NOT NULL,
  `Address3` int(10) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TiffinStartEndStatus`
--

INSERT INTO `TiffinStartEndStatus` (`Id`, `UserId`, `WeekDay`, `BreakFast`, `Lunch`, `Dinner`, `Address1`, `Address2`, `Address3`, `CreateOn`) VALUES
(1, 9, 5, 1, 1, 1, 2, 2, 2, '2018-07-02 21:03:18'),
(2, 10, 2, 0, 0, 0, 0, 3, 0, '2018-07-17 13:49:39');

-- --------------------------------------------------------

--
-- Table structure for table `TiffinUserWeeklyMeal`
--

CREATE TABLE `TiffinUserWeeklyMeal` (
  `WeeklyId` int(10) NOT NULL,
  `OrderStatusId` int(8) NOT NULL,
  `DayTime` int(1) NOT NULL,
  `TotalAmount` varchar(50) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `mobileno` varchar(10) DEFAULT NULL,
  `emailid` varchar(256) NOT NULL,
  `dob` varchar(12) NOT NULL,
  `gender` int(1) NOT NULL COMMENT '1=men, 2=women',
  `isemailverified` int(1) NOT NULL,
  `ismobileverified` int(1) NOT NULL,
  `password` varchar(256) NOT NULL,
  `mobileverificationcode` varchar(100) NOT NULL,
  `emailverificationcode` varchar(100) NOT NULL,
  `pass_code` varchar(55) NOT NULL,
  `registrationOn` datetime NOT NULL,
  `updateOn` datetime NOT NULL,
  `token` varchar(100) NOT NULL,
  `devicetype` varchar(10) NOT NULL,
  `devicetoken` varchar(256) NOT NULL,
  `deviceid` varchar(50) NOT NULL,
  `social_id` varchar(55) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `colorFlag` varchar(20) NOT NULL DEFAULT 'Green',
  `ReferCode` varchar(10) NOT NULL,
  `ReferralCode` varchar(10) NOT NULL,
  `wallet` varchar(20) NOT NULL,
  `ReferWallet` varchar(20) NOT NULL,
  `ValidityCount` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `image`, `mobileno`, `emailid`, `dob`, `gender`, `isemailverified`, `ismobileverified`, `password`, `mobileverificationcode`, `emailverificationcode`, `pass_code`, `registrationOn`, `updateOn`, `token`, `devicetype`, `devicetoken`, `deviceid`, `social_id`, `status`, `latitude`, `longitude`, `colorFlag`, `ReferCode`, `ReferralCode`, `wallet`, `ReferWallet`, `ValidityCount`) VALUES
(1, 'Mukesh Patidar', '', '', '8770508141', 'mukesh@ebabu.co', '', 0, 1, 1, '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '762a5fc225bc7b9ebfe8de7ed139253909c283fdb2e1fe6072481ba6350ac020', '', '2018-06-30 12:10:19', '2018-06-30 12:12:58', '', 'android', '', '312e85104597bb94', '', 1, '', '', 'Green', 'MKD98494', '', '', '', 0),
(2, 'Mukee', '', '', '9926020798', 'mukesh@ebabu.co', '', 0, 0, 1, '7c4a8d09ca3762af61e59520943dc26494f8941b', '', 'fab4dca7a535a6a93d341f4142b8f0acdb3d95ae59b6a998885404c0db51c730', '', '2018-06-30 12:13:57', '2018-06-30 12:14:08', '5e492a4e2d43a4125d684a48cd1f79fb3a2d533e1530341048163', 'android', 'coKmUV4qb84:APA91bFdXD-R2ZM4IV9Fx4vbYI9szblPRekNnVheAeIKCMLgoaRobCqN8YEH5-AsEoI5Sdb-FwuOaBUDpRqNYrgzu91-wlfh2_biTIptJBSd81oh0LKWQE6qgy1KTTNNwWlO4Tn6tax8', '312e85104597bb94', '', 1, '', '', 'Green', 'MKD86729', '', '', '', 0),
(3, 'Vardhman', '', '', '9267960066', 'zanservices.delhi@gmail.com', '', 0, 0, 1, 'b16d676cf10360a101edb4273d158aa1029c4f64', '', '0c5164df1da564085c680aa519d59d3af25d7f21084cdaf97953f1179ba8bc38', '', '2018-07-02 11:16:01', '2018-07-02 11:16:08', '', 'android', '', 'ae15f9792808c9af', '', 0, '', '', 'Green', 'MKD26593', '', '', '', 0),
(4, 'Admin', '', '', NULL, 'admin@gmail.com', '', 0, 1, 0, '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', '', 1, '', '', 'Green', '', '', '', '', 0),
(5, 'Jjn', '', '', '1235469870', 'aks@gmail.com', '', 0, 0, 1, '968651bab195717f1a9c996b1d3dbb255999f265', '25f9b3ab7e7d3634a79eab88d59a0b4592d42384900dfc10ecc481af5b8dfd3d', '270300bb27713454cb7a5a668fc0ea4fd4583ef8e6bcfe41ab5b72160ab6f65a', '', '2018-07-02 20:55:24', '2018-07-02 20:54:45', '', '', '', '', '', 0, '', '', 'Green', 'MKD49431', '', '', '', 0),
(6, 'Jjkkm', '', '', '8851132189', 'vardhanjain_harsh@yahoo.co.in', '', 0, 0, 1, '7c4a8d09ca3762af61e59520943dc26494f8941b', '', 'f696599622f83a0fdc1cca82d0a75bca6a5c6843c95f82c1b5f1ae1a5cff3375', '', '2018-07-02 20:56:06', '2018-07-02 20:56:09', '8dedf4a9d71aa2bec4d6666ad73d750b49163fb91530545169764', 'android', 'dEegyL393p8:APA91bHBm4UXlHVPt_X-J9yQTdB4Th4r4-sPhgCJmtY6LeeUvlX9BZb9PH17bi7Yz78vHuj1k03cyeGDINrZH7n1MBWFXulFixSYvfr0e3r6gS2ZUPUEGkujItlN2twaYQscARhJct4blkY6lqsbELnfM6mcdKQIcA', 'c7085d31f6ae9520', '', 0, '', '', 'Green', 'MKD24320', '', '', '', 0),
(7, 'Saurabh', '', '', '8251024168', 'advsaurabhnayak@gmail.com', '', 0, 0, 1, 'e28986ae058832d9bda78295760b17a1c465102e', '', 'fb0ba27c853df787ad73c783517413133b7b4024fad79eaec5657346d94503e3', '', '2018-07-04 10:05:21', '2018-07-04 10:05:27', 'd4ee70de3e1f7a0496cb52b238de8176a9fd6bd51530678927457', 'android', 'emxjzDTFnB8:APA91bHN7ga1xT3iNgQGFjzULk_cTNlPZivlmC5-A_CzRzhy97Ok008bf00EkzV6S6vc3f55NyHz_h_j911uZLJYqN2Oehqlf-1SxnF7TMEirixoEynLiq5pymYXbU8u1oXPSSF4zsK1xgV3QoBHS09Rd5HLg4GtjA', 'ae15f9792808c9af', '', 1, '', '', 'Green', 'MKD20465', '', '2637', '', 30),
(8, 'Mukul Jain', '', '', '8882122432', 'mkljain@gmail.com', '', 0, 0, 1, 'ecd69913866a2ab09753d4633f5c0ae3da5d4e0a', '', 'a73fce05f5250bedf4cba89bcf6cbd4c77b132b9dc92394e50cac8ebe7f0216a', '', '2018-07-04 17:32:48', '2018-07-04 17:33:15', '76b00dcb65e90200bdaad7557a274c5c4416f5011530705795783', 'android', 'dxCLM4dIAqk:APA91bH6jhNDr1oMSi5dGupeaZUZFd9ksN3Dn-wC-RGGz_BFBbHrEAbyEN2A3-1Rydu7QPvb6tOUYFG6iBT5er4e5f5CRKiGvTSqYe3AzO-V-9E1PQcNuzoKee0km7gV_cxkNV_h0jesTIZdp5BYx8Bz5YPYRs-Pkw', '26405fb79e49c273', '', 1, '', '', 'Green', 'MKD19761', '', '', '', 0),
(9, 'Y', '', '', '9754743271', 'yatindra.mohite@ebabu.co', '', 0, 0, 1, '7c4a8d09ca3762af61e59520943dc26494f8941b', '', 'aa535fe2f6389aefe79d7cfbd8d4604522a4864e0021aed11b3ff615bca5f6e3', '', '2018-07-13 15:48:21', '2018-07-13 15:48:41', '78677e5991a4bb961909d38e3a72ec5071993e851531477121116', 'android', 'cl4cavom3DU:APA91bHSYTQZAMDemxMTPRHRRaojNDZhz7Gq3awTh956hz3CCxmB8NueGh9724D5sSuRuVbvjWXQ-5bwynX5i-wjjQX7GgkVlmgpDne-Kd3fBqDNiIhfx4PwVwzugWr2GLstYSTFAIyoKl4DbwZrcQeP_ZADN9R_Cw', 'e405f3d5edf87085', '', 1, '', '', 'Green', 'MKD68423', '', '', '', 0),
(10, 'Rupali', '', '', '7879124245', 'rupalipareek.3008@gmail.com', '', 0, 0, 1, 'aa85a68258ceda07c4e691a7dfa598a2f10f0d2b', '', '3a6726717ee04c45ef4a9132a8826cab067790da073bb9fb764e2f2497c8f275', '', '2018-07-17 13:13:14', '2018-07-18 12:14:13', '339d70c7148546dd85374f9cef324aa01531896253654', 'android', 'ejUTCD8REig:APA91bHDtTKfiW25A2CNMSMWR-H78s4pWgExPdL-7_4EYsi1jnGEHguI2yW6Br0BrVwOE3wNh7aMjN8qp2GcDuUq4rGglsyG0nTdAhgld_nPrOp9s0W7Dp268m8kbqO5vrBb0HqEeISuW-AuLgdpa5vQcSGAPTgVIg', 'e9e2db3006646576', '', 1, '', '', 'Green', 'MKD92001', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `userAddress`
--

CREATE TABLE `userAddress` (
  `Id` int(11) NOT NULL,
  `Address` varchar(256) NOT NULL,
  `HouseNo` varchar(50) NOT NULL,
  `LandMark` varchar(100) NOT NULL,
  `AddressTitle` varchar(50) NOT NULL DEFAULT 'Home',
  `CityState` varchar(50) NOT NULL,
  `LocalityId` int(11) NOT NULL,
  `Latitude` varchar(30) NOT NULL,
  `Longitude` varchar(30) NOT NULL,
  `UserId` int(11) NOT NULL,
  `AddedOn` datetime NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  `StoreId` int(11) NOT NULL,
  `IsPrimaryAddress` bit(1) NOT NULL,
  `State` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userAddress`
--

INSERT INTO `userAddress` (`Id`, `Address`, `HouseNo`, `LandMark`, `AddressTitle`, `CityState`, `LocalityId`, `Latitude`, `Longitude`, `UserId`, `AddedOn`, `UpdatedOn`, `StoreId`, `IsPrimaryAddress`, `State`) VALUES
(1, 'Yu', '', '', 'Home', '2', 1, '23.192391', '75.794292', 6, '2018-07-02 21:02:47', '2018-07-02 21:02:47', 3, b'0', 1),
(2, '616 shekar centre', '', '', 'Home', '1', 1, '22.7180683', '75.8823077', 9, '2018-07-13 15:54:09', '2018-07-13 15:54:09', 2, b'0', 1),
(3, '203 tulsi tower', '', '', 'Home', '1', 1, '22.7252579', '75.8832161', 10, '2018-07-17 13:15:36', '2018-07-17 13:15:36', 2, b'0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `UserFavourite`
--

CREATE TABLE `UserFavourite` (
  `Id` int(11) NOT NULL,
  `StoreId` int(11) NOT NULL,
  `DieticianId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `AddedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `UserPaymentAccount`
--

CREATE TABLE `UserPaymentAccount` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `BankName` varchar(50) NOT NULL,
  `Status` int(11) NOT NULL,
  `CardNo` varchar(50) NOT NULL,
  `CCV` int(11) NOT NULL,
  `AuthCode` varchar(256) NOT NULL,
  `PinCode` varchar(30) NOT NULL,
  `LinkedOn` datetime NOT NULL,
  `Sequence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `UserReferral`
--

CREATE TABLE `UserReferral` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `ReferCode` varchar(20) NOT NULL,
  `CreatedOn` int(11) NOT NULL,
  `Message` varchar(300) DEFAULT NULL,
  `ExpireOn` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `UserReferralTransaction`
--

CREATE TABLE `UserReferralTransaction` (
  `HistoryId` int(10) NOT NULL,
  `UserId` int(10) NOT NULL,
  `Type` varchar(8) NOT NULL,
  `Amount` varchar(20) NOT NULL,
  `Msg` varchar(255) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `UserValidityPlan`
--

CREATE TABLE `UserValidityPlan` (
  `Id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `ValidityPlanId` int(10) NOT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `ValidityDays` int(5) NOT NULL,
  `CreateOn` datetime NOT NULL,
  `ValidityAmount` varchar(20) NOT NULL,
  `TransactionDetail` varchar(100) NOT NULL,
  `PlanType` int(1) NOT NULL COMMENT '1=regular,2=trial'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserValidityPlan`
--

INSERT INTO `UserValidityPlan` (`Id`, `UserId`, `ValidityPlanId`, `StartDate`, `EndDate`, `ValidityDays`, `CreateOn`, `ValidityAmount`, `TransactionDetail`, `PlanType`) VALUES
(1, 7, 0, '2018-07-04 00:00:00', '2018-08-03 00:00:00', 30, '2018-07-04 10:13:04', '2637.00', 'pay_AV1a6Zf5sbVgnX', 1);

-- --------------------------------------------------------

--
-- Table structure for table `WalletTransaction`
--

CREATE TABLE `WalletTransaction` (
  `HistoryId` int(10) NOT NULL,
  `UserId` int(10) NOT NULL,
  `Type` varchar(8) NOT NULL,
  `Msg` varchar(250) NOT NULL,
  `Amount` varchar(20) NOT NULL,
  `CreateOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `WalletTransaction`
--

INSERT INTO `WalletTransaction` (`HistoryId`, `UserId`, `Type`, `Msg`, `Amount`, `CreateOn`) VALUES
(1, 7, 'Credit', 'Amount added to wallet for buy validity plan of amount 2637.00', '2637.00', '2018-07-04 10:13:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AdminOffer`
--
ALTER TABLE `AdminOffer`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `ProductId` (`ProductId`),
  ADD KEY `StoreId` (`StoreId`),
  ADD KEY `OrgId` (`OrgId`),
  ADD KEY `ProductCategoryId` (`ProductCategoryId`),
  ADD KEY `ProductSubCategoryId` (`ProductSubCategoryId`);

--
-- Indexes for table `AdminSetting`
--
ALTER TABLE `AdminSetting`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `AppVersion`
--
ALTER TABLE `AppVersion`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Area`
--
ALTER TABLE `Area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `City`
--
ALTER TABLE `City`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ContactUs`
--
ALTER TABLE `ContactUs`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `FAQ`
--
ALTER TABLE `FAQ`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Feedback`
--
ALTER TABLE `Feedback`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `StoreId` (`StoreId`),
  ADD KEY `UserId` (`UserId`),
  ADD KEY `ProductId` (`ProductId`),
  ADD KEY `OrderId` (`OrderId`);

--
-- Indexes for table `LocalArea`
--
ALTER TABLE `LocalArea`
  ADD PRIMARY KEY (`LocalAreaId`);

--
-- Indexes for table `MkdValidityPlan`
--
ALTER TABLE `MkdValidityPlan`
  ADD PRIMARY KEY (`ValidityId`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`noti_id`);

--
-- Indexes for table `OrderItems`
--
ALTER TABLE `OrderItems`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Page_content`
--
ALTER TABLE `Page_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PauseResume`
--
ALTER TABLE `PauseResume`
  ADD PRIMARY KEY (`PRId`);

--
-- Indexes for table `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Product_Store_StoreId` (`StoreId`),
  ADD KEY `FK_Product_User_UserID` (`UserId`),
  ADD KEY `CategoryId` (`CategoryId`);

--
-- Indexes for table `ProductCategory`
--
ALTER TABLE `ProductCategory`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `ProductImages`
--
ALTER TABLE `ProductImages`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_ProductImages_Product_Id` (`ProductId`);

--
-- Indexes for table `Promotions`
--
ALTER TABLE `Promotions`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `ReferralByDeviceId`
--
ALTER TABLE `ReferralByDeviceId`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `State`
--
ALTER TABLE `State`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `SubscriptionPlan`
--
ALTER TABLE `SubscriptionPlan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Tax`
--
ALTER TABLE `Tax`
  ADD PRIMARY KEY (`TaxId`);

--
-- Indexes for table `TiffenProductSchedule`
--
ALTER TABLE `TiffenProductSchedule`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `TiffenTiming`
--
ALTER TABLE `TiffenTiming`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `TiffinStartEndStatus`
--
ALTER TABLE `TiffinStartEndStatus`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `TiffinUserWeeklyMeal`
--
ALTER TABLE `TiffinUserWeeklyMeal`
  ADD PRIMARY KEY (`WeeklyId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userAddress`
--
ALTER TABLE `userAddress`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `UserFavourite`
--
ALTER TABLE `UserFavourite`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `UserId` (`UserId`),
  ADD KEY `StoreId` (`StoreId`),
  ADD KEY `DieticianId` (`DieticianId`);

--
-- Indexes for table `UserPaymentAccount`
--
ALTER TABLE `UserPaymentAccount`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `UserReferral`
--
ALTER TABLE `UserReferral`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `UserReferralTransaction`
--
ALTER TABLE `UserReferralTransaction`
  ADD PRIMARY KEY (`HistoryId`);

--
-- Indexes for table `UserValidityPlan`
--
ALTER TABLE `UserValidityPlan`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `WalletTransaction`
--
ALTER TABLE `WalletTransaction`
  ADD PRIMARY KEY (`HistoryId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `AdminOffer`
--
ALTER TABLE `AdminOffer`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AdminSetting`
--
ALTER TABLE `AdminSetting`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `AppVersion`
--
ALTER TABLE `AppVersion`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Area`
--
ALTER TABLE `Area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `City`
--
ALTER TABLE `City`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ContactUs`
--
ALTER TABLE `ContactUs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `FAQ`
--
ALTER TABLE `FAQ`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Feedback`
--
ALTER TABLE `Feedback`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `LocalArea`
--
ALTER TABLE `LocalArea`
  MODIFY `LocalAreaId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `MkdValidityPlan`
--
ALTER TABLE `MkdValidityPlan`
  MODIFY `ValidityId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `noti_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `OrderItems`
--
ALTER TABLE `OrderItems`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `PauseResume`
--
ALTER TABLE `PauseResume`
  MODIFY `PRId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `Product`
--
ALTER TABLE `Product`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `ProductCategory`
--
ALTER TABLE `ProductCategory`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ProductImages`
--
ALTER TABLE `ProductImages`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Promotions`
--
ALTER TABLE `Promotions`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ReferralByDeviceId`
--
ALTER TABLE `ReferralByDeviceId`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `State`
--
ALTER TABLE `State`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `SubscriptionPlan`
--
ALTER TABLE `SubscriptionPlan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `Tax`
--
ALTER TABLE `Tax`
  MODIFY `TaxId` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `TiffenProductSchedule`
--
ALTER TABLE `TiffenProductSchedule`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `TiffenTiming`
--
ALTER TABLE `TiffenTiming`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `TiffinStartEndStatus`
--
ALTER TABLE `TiffinStartEndStatus`
  MODIFY `Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `TiffinUserWeeklyMeal`
--
ALTER TABLE `TiffinUserWeeklyMeal`
  MODIFY `WeeklyId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `userAddress`
--
ALTER TABLE `userAddress`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `UserFavourite`
--
ALTER TABLE `UserFavourite`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `UserPaymentAccount`
--
ALTER TABLE `UserPaymentAccount`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `UserReferral`
--
ALTER TABLE `UserReferral`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `UserReferralTransaction`
--
ALTER TABLE `UserReferralTransaction`
  MODIFY `HistoryId` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `UserValidityPlan`
--
ALTER TABLE `UserValidityPlan`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `WalletTransaction`
--
ALTER TABLE `WalletTransaction`
  MODIFY `HistoryId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `AdminOffer`
--
ALTER TABLE `AdminOffer`
  ADD CONSTRAINT `AdminOffer_ibfk_1` FOREIGN KEY (`ProductId`) REFERENCES `Product` (`Id`),
  ADD CONSTRAINT `AdminOffer_ibfk_2` FOREIGN KEY (`StoreId`) REFERENCES `Store` (`Id`),
  ADD CONSTRAINT `AdminOffer_ibfk_3` FOREIGN KEY (`OrgId`) REFERENCES `Organization` (`Id`),
  ADD CONSTRAINT `AdminOffer_ibfk_4` FOREIGN KEY (`ProductCategoryId`) REFERENCES `ProductCategory` (`Id`),
  ADD CONSTRAINT `AdminOffer_ibfk_5` FOREIGN KEY (`ProductSubCategoryId`) REFERENCES `ProductSubCategory` (`Id`);

--
-- Constraints for table `Feedback`
--
ALTER TABLE `Feedback`
  ADD CONSTRAINT `Feedback_ibfk_1` FOREIGN KEY (`StoreId`) REFERENCES `Store` (`Id`),
  ADD CONSTRAINT `Feedback_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `Feedback_ibfk_3` FOREIGN KEY (`ProductId`) REFERENCES `Product` (`Id`),
  ADD CONSTRAINT `Feedback_ibfk_4` FOREIGN KEY (`OrderId`) REFERENCES `OrderData` (`Id`);

--
-- Constraints for table `ProductImages`
--
ALTER TABLE `ProductImages`
  ADD CONSTRAINT `FK_ProductImages_Product_Id` FOREIGN KEY (`ProductId`) REFERENCES `Product` (`Id`);

--
-- Constraints for table `userAddress`
--
ALTER TABLE `userAddress`
  ADD CONSTRAINT `userAddress_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`);

--
-- Constraints for table `UserFavourite`
--
ALTER TABLE `UserFavourite`
  ADD CONSTRAINT `UserFavourite_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `UserFavourite_ibfk_2` FOREIGN KEY (`StoreId`) REFERENCES `Store` (`Id`);

--
-- Constraints for table `UserPaymentAccount`
--
ALTER TABLE `UserPaymentAccount`
  ADD CONSTRAINT `UserPaymentAccount_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
