-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 28, 2017 at 10:33 AM
-- Server version: 5.5.57-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `barbar`
--

-- --------------------------------------------------------

--
-- Table structure for table `barbar_user`
--

CREATE TABLE IF NOT EXISTS `barbar_user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(40) NOT NULL,
  `user_email` varchar(300) NOT NULL,
  `user_mobile_num` varchar(20) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_location` text NOT NULL,
  `user_lat` varchar(15) NOT NULL,
  `user_lng` varchar(15) NOT NULL,
  `user_type` int(1) NOT NULL COMMENT '1=customer,2=barbar',
  `user_device_type` varchar(10) NOT NULL,
  `user_device_id` varchar(100) NOT NULL,
  `user_device_token` text NOT NULL,
  `user_token` varchar(50) NOT NULL,
  `email_status` int(1) NOT NULL COMMENT '1=active',
  `admin_status` int(1) NOT NULL DEFAULT '1' COMMENT '1=active',
  `mobile_status` int(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `email_code` varchar(50) NOT NULL,
  `mobile_code` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `barbar_user`
--

INSERT INTO `barbar_user` (`user_id`, `user_name`, `user_email`, `user_mobile_num`, `user_password`, `user_location`, `user_lat`, `user_lng`, `user_type`, `user_device_type`, `user_device_id`, `user_device_token`, `user_token`, `email_status`, `admin_status`, `mobile_status`, `create_date`, `update_date`, `email_code`, `mobile_code`) VALUES
(1, '', 'yatindra.mohite@ebabu.co', '919754743271', '7c4a8d09ca3762af61e59520943dc2', '616,shekhar centeral', '22.754585', '75.545211', 0, '', '', '', '', 0, 1, 0, '2017-10-26 01:51:16', '2017-10-26 01:57:31', 'Qk1TLzV6TzBERkRCNnJzalpwSGo3QT09', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
