<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurant extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}
		/*$response = $this->common_model->check_auth($this->session->userdata('admin_id'));
	    if($response == 1001)
	    {
	      redirect(base_url().'Logout');
	    }  */  
	}

	public function detail()
	{
		$data['restaurant_data'] = $this->common_model->getData('Store',array('Status'=>'1'),'id','DESC');

    	$this->load->view('admin/restaurant/restaurant_list',$data);
	}
	
	public function add_restaurant()
    { 	
		if($this->input->server('REQUEST_METHOD') === 'POST')
    	{ 
	          $restaurant_data = array(
	          'Name' =>$this->input->post('restaurant_Name'),
	          'Address' =>$this->input->post('address'),
	          'OrgId'=>$this->session->userdata('admin_id'),
	          'status'=>1,
	          'Orgadmin_id'=>$this->session->userdata('admin_id'),
	          'City'=>$this->input->post('city'),
	          'State'=>$this->input->post('state'),
	          'Phone'=>$this->input->post('mobileno'),
	          'Email'=>$this->input->post('emailid'),
	          'Latitude'=>'',
	          'Longitude'=> '',
	          'AddedOn'=> date('Y-m-d H:i:s')

	          );

	        $insert_id = $this->common_model->common_insert('Store',$restaurant_data);

	        if($insert_id)
	        {
	            $this->session->set_flashdata('success', 'Restaurant Added Successfully.');
	            redirect(base_url().'restaurant/add_restaurant');
	        }
    	}
    	$this->load->view('admin/restaurant/add_restaurant');
	}

	public function edit($restaurant_id = false)
	{
		$data['restaurant_data'] = $this->common_model->common_getRow("Store",array('Id'=>$restaurant_id));

		if($this->input->server('REQUEST_METHOD') === 'POST')
    	{ 
	          $restaurant_data = array(
	          'Name' =>$this->input->post('restaurant_Name'),
	          'Address' =>$this->input->post('address'),
	          'City'=>$this->input->post('city'),
	          'State'=>$this->input->post('state'),
	          'Phone'=>$this->input->post('mobileno'),
	          'Email'=>$this->input->post('emailid'),
	          'Latitude'=>'',
	          'Longitude'=> '',
	          );

	       $update = $this->common_model->updateData("Store",$restaurant_data,array('Id'=>$restaurant_id));	

	        if($update != false)
	        {
	            $this->session->set_flashdata('success', 'Restaurant Updates Successfully.');
	            redirect(base_url().'restaurant/detail');
	        }
    	}

    	$this->load->view('admin/restaurant/edit_restaurant',$data);
	}

	public function check_mobile()
	{
		$mobile_no = $this->input->post('mobile_no'); 
		$query = $this->db->query("SELECT `mobileno` FROM `user` WHERE `mobileno` = '".$mobile_no."'");
		if ($query->num_rows() > 0){
			echo "10000";exit;
		}
	}

	public function randno($tot=false)
	{
	    if($tot=='')
	    {
	        $tot=8;    
	    }
       return $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $tot);    
    }


}
