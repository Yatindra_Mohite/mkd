<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Barber extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		}

      $response = $this->common_model->check_auth($this->session->userdata('admin_id'));
      if($response == 1001)
      {
        redirect(base_url().'Logout');
      }    
    }
    
    public function add_barber()
    {  
        if($this->input->server('REQUEST_METHOD') === 'POST')
        { 
            if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
            { 
                $date = date("ymdhis");
                $config['upload_path'] = 'uploads/barber_image/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $subFileName = explode('.',$_FILES['image']['name']);
                $ExtFileName = end($subFileName);
                $config['file_name'] = md5($date.$_FILES['image']['name']).'.'.$ExtFileName;
                          
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
              
                if($this->upload->do_upload('image'))
                { 
                  $upload_data = $this->upload->data();
                  $image = $upload_data['file_name'];
    
                  ini_set("memory_limit", "-1");
                    
                  $config['image_library']  = 'gd2';
                  $config['source_image']   = 'uploads/barber_image/'.$image;
                  $config['create_thumb']   = TRUE;
                  $config['maintain_ratio'] = TRUE;
                  $config['max_width']      = "100";
                  $config['max_height']     = "100";
                  $config['new_image'] = 'uploads/barber_image/'.$image;

                  $this->load->library('image_lib', $config);

                  $this->image_lib->initialize($config);

                  $newimage =  $this->image_lib->resize();
                  $this->image_lib->clear();
                  $x12 = explode('.', $image);//for extension
                  $thumb_img =  $x12[0].'_thumb.'.$x12[1];
                }
                else
                {   
                   $this->data['err']= $this->upload->display_errors();
                   $this->session->set_flashdata('error_pic', 'Please Select png,jpg,jpeg File Type.');
                   redirect('barber/add_barber');
                }
                //ImageJPEG(ImageCreateFromString(file_get_contents($image1)), 'uploads/admin_image/'.$image, 30);
            }
            else
            { 
                $thumb_img = '';
            }

            $address = $this->input->post('address');
            
            $string = str_replace(" ","+",$address);     

                    $chk_url='http://maps.google.com/maps/api/geocode/json?address='.$string.'&sensor=false';
                    $geocode = file_get_contents($chk_url);
                    $output = json_decode($geocode);
                        
                    if(!empty($output->results))
                    {
                       $lat = $output->results[0]->geometry->location->lat;
            
                       $lng = $output->results[0]->geometry->location->lng;
                    
                    }else if ($lat =='' && $lat ==0 && $lng =='' && $lng ==0){
                      $this->session->set_flashdata('invalid_address', 'this address is not valid');
                      redirect('barber/add_barber');
                    }
    
            $barber = array(
            'user_name' =>$this->input->post('b_name'),
            'user_email' =>$this->input->post('email'),
            'user_image'=>$thumb_img,
            'user_mobile_num'=>$this->input->post('contact_no'),
            'user_location'=>$this->input->post('address'),
            'admin_status'=>1,
            'user_lat'=>$lat,
            'user_lng'=>$lng,
            'user_type'=>'2',
            'email_status'=>'0',
            'mobile_status'=>'0',
            'create_date'=>date('Y-m-d h:i:s'),
            'update_date' =>date('Y-m-d h:i:s')
            );

            $insert_id = $this->common_model->common_insert('barber_user',$barber);
            if($insert_id)
            {
                     $email = $this->input->post('email');

                     $password =  $this->randno(8);
                    
                     $sha_password = md5($password);

                     $v_code = $this->common_model->randomuniqueCode();
                     $v_Code = $this->common_model->encryptor_ym('encrypt',$v_code);


                    $url = "<a href=".base_url()."barber_api/email_verification/".$this->common_model->encryptor_ym('encrypt',$insert_id)."/".$v_Code." target='_blank' title='' style='background: #d3a117; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";

                     
                   $data = array('username'=> $this->input->post('b_name'),
                                           'email'=>$email,
                                           'password'=>$password,
                                           'url'=> $url
                                         );

                      $message = $this->load->view('email_template/admin_template_verify.php',$data,TRUE);

                    /*$config = Array(        
                         'mailtype'  => 'html', 
                         'charset'   => 'utf-8'
                             );
                             $this->email->initialize($config);
                             $this->email->set_newline("\r\n");
                             $this->email->from('noreply@barber.com', 'Barber');
                             $this->email->to($email); 
                             $this->email->subject('Registration mail');
                             $this->email->set_mailtype('html');*/
                      $config = Array(
                          'protocol' => 'smtp',
                          'smtp_host' => 'mail.zohomail.com',
                          'smtp_port' => 465,
                          'smtp_user' => 'Barbers@Kwikuts.co.uk',
                          'smtp_pass' => 'Kwikuts2017',
                          'mailtype'  => 'html', 
                          'charset'   => 'utf-8',
                          'wordwrap'  => TRUE
                       );

                      $this->load->library('email', $config);

                      $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
                      $this->email->set_header('Content-type', 'text/html');

                      $this->email->set_newline("\r\n");
          
                      $this->email->from('Barbers@Kwikuts.co.uk', 'Kwikuts');
                      $this->email->to($email); 
                      $this->email->subject('Registration mail..!');       
                             
                      $this->email->message($message); 
    
                    if($this->email->send())
                    {
                        $update_info = $this->common_model->updateData('barber_user',array('user_password'=>$sha_password,'email_code'=>$v_Code),array('user_id'=>$insert_id));	
    
                        $this->session->set_flashdata('success', 'Barber registered successfully and password mail sent his email address for verification.');

                        redirect('barber/Add_barber');
                    }
                }
        } 
        $this->load->view('admin/barber/Add_barber');
    }

	public function active()
	{
		$data['user_data'] = $this->common_model->getData('barber_user',array('email_status'=>'1','mobile_status'=>'1','user_type'=>'2','admin_status'=>'1'),'user_id','DESC');
		
		$this->load->view('admin/barber/active_barber',$data);
	}

 /* public function ajzxbarber()
  { 
    $aColumns = array(
            'checkbox',
            'user_id',
            'user_image',
            'user_name',
            'user_email'
            );
    $sIndexColumn = "user_id";

      $results = $this->common_model->get_cd_list('barber_user',$aColumns,$sIndexColumn);
        echo json_encode($results);
  }
*/
	public function deactivated()
	{
		$data['user_data'] = $this->common_model->getData('barber_user',array('admin_status'=>'2'),'user_id','DESC');
		
		$this->load->view('admin/barber/deactivate_barber',$data);
	}
    //Deactivate action for Active user 
	public function deactivate_barber()
	{
		$user_id = $this->input->post('user_id');
        $delete = $this->db->query("UPDATE `barber_user` SET `admin_status` = 2 WHERE `user_id` IN($user_id)");

        if($delete)
        {
        	echo $user_id;exit;
        }	
	}

    //Active action for deactive user
	public function activate_barber()
	{
        $user_id = $this->input->post('user_id');
        $delete = $this->db->query("UPDATE `barber_user` SET `admin_status` = 1 WHERE `user_id` IN($user_id)");

        if($delete)
        {
        	echo $user_id;exit;
        }
	}

	public function delete()
	{
	   $user_id = $this->input->post('user_id');

	   $delete = $this->db->query("DELETE FROM `qalame_user` WHERE `user_id` IN($user_id)");
	   $delete = $this->db->query("DELETE FROM `qalame_follow_user` WHERE `following_id` IN($user_id)");

	   $delete = $this->db->query("DELETE FROM `qalame_follow_user` WHERE `follower_id` IN($user_id)");

	   $delete = $this->db->query("DELETE FROM `qalame_user_post` WHERE `user_id` IN($user_id)");

	   echo $user_id;exit;

    }
    
    public function randno($tot=false)
    {
        if($tot=='')
        {
            $tot=8;    
        }
        return $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $tot);    
    }

  public function check_email()
  {
      $email = $this->input->post('email'); 
      $mobileno = $this->input->post('mobileno');
      $query = $this->db->query("SELECT `user_email` FROM `barber_user` WHERE `user_email` = '".$email."' AND `user_type`='2'");
     
     if($query->num_rows() > 0){
      echo "10000";exit;
     }

    $query1 = $this->db->query("SELECT `user_mobile_num` FROM `barber_user` WHERE `user_mobile_num` = '".$mobileno."' AND `user_type`='2'");

     if ($query1->num_rows() > 0){
      echo "1001";exit;
     }
  }

  public function barber_gallery()
  {
    $user_id = $this->input->post('user_id');
    $gallery_data = $this->db->query("SELECT gallary_pic FROM barber_gallary WHERE user_id = $user_id ORDER BY gallary_id DESC")->result();
    
     foreach($gallery_data as $image)
     { ?>
          <img src="<?php echo base_url('uploads/gallary_image/'.$image->gallary_pic);?>" height="180px" width="180px" style="margin-top:0.9em;">&nbsp;&nbsp;

    <?php } exit;
  
  }

  public function unverified()
  {
     $data['user_data'] =  $this->db->query("SELECT user_id,create_date,user_name,user_email,user_mobile_num,user_image FROM barber_user WHERE user_type = '2' AND admin_status = '0' AND email_status = '1' ORDER BY user_id DESC")->result();

     $this->load->view('admin/barber/unverified_barber',$data);
  }

  //Verified Action
  public function verified()
  {
      $user_id = $this->input->post('user_id'); 

      $user_id_arr = explode(",",$user_id);

      for($i = 0;$i<count($user_id_arr);$i++)
      {
         $user_data =  $this->db->query("SELECT user_name,user_email FROM barber_user WHERE user_id = '".$user_id_arr[$i]."'")->row();

         $status_update = $this->common_model->updateData('barber_user',array('admin_status'=>1),array('user_id'=>$user_id_arr[$i]));

        if($status_update)    
        {
            $email = $user_data->user_email;
            $username = $user_data->user_name;
           
                    $data = array('username'=> $username );
                                        
                    $message = $this->load->view('email_template/admin_verifyappbarber_template.php',$data,TRUE);

                    $config = Array(        
                         'mailtype'  => 'html', 
                         'charset'   => 'utf-8'
                             );

                             $this->email->initialize($config);
                             $this->email->set_newline("\r\n");
                             $this->email->from('noreply@barber.com', 'Barber');
                             $this->email->to($email); 
                             $this->email->subject('Welcome to the Barber Team..!');
                             $this->email->set_mailtype('html');
                             
                        $this->email->message($message); 

                        $sendmail =   $this->email->send();

        }  
      }

      if($status_update)
      {
        echo $user_id;exit;
      } 

  }
	
  public function sale($barber_id = false)
  {
    $data = '';
    if(isset($_POST['submit']))
    {
       $year = $this->input->post('years');
    }
    else
    {
       $year = date('Y');
    }  

     $month = array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');

      for($i=0;$i<12;$i++)
      {
        $barber_sale_amount = $this->db->query("SELECT SUM(barber_customer_booking.booking_amount) AS amount,month FROM barber_customer_booking
 WHERE month = '".$month[$i]."' AND year = '".$year."' AND barber_id = ".$barber_id." GROUP BY month,year")->row();

        if(!empty($barber_sale_amount->amount))
        {  
          $arr[] = $barber_sale_amount->amount;
          
        }
        else
        {
          $arr[] = 0;
        } 
      }

    $data['yearss'] = $year;
    $data['total_amount'] = $arr;

   // print_r($data['total_amount']);exit;

    $data['amount'] = $this->db->query("SELECT SUM(barber_customer_booking.booking_amount) AS amount FROM barber_customer_booking  WHERE year = '".$year."' AND barber_id = ".$barber_id."")->result();

     $this->load->view('admin/barber/barber_sale',$data);

  }

  public function dummy()
  {
     $data['dummy_barber'] = $this->common_model->getData('barber_dummy',array(),'barber_id','DESC');
    
     $this->load->view('admin/barber/dummy_barber',$data);
  }

  public function add_dummy()
  {
     if($this->input->server('REQUEST_METHOD') === 'POST')
     { 
        if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
        { 
            $this->load->library('image_lib');

            $date = date("ymdhis");
            $config['upload_path'] = 'uploads/dummy_barber/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = 200; 
            $config['max_width'] = '200';
            $config['max_height'] = '150';

            $subFileName = explode('.',$_FILES['image']['name']);
            $ExtFileName = end($subFileName);
            $config['file_name'] = md5($date.$_FILES['image']['name']).'.'.$ExtFileName;
                      
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('image'))
            {
              $fileData = $this->upload->data();
              $uploadData['file_name'] = $fileData['file_name'];
              $image = $uploadData['file_name'];
            }
            else
            {
              $this->data['err']= $this->upload->display_errors();
              $this->session->set_flashdata('image_error', $this->data['err']);
              redirect('barber/add_dummy');
              exit;
            }
        }
        else
        { 
          $image = '';
        }

        $barber = array(
        'user_name' =>$this->input->post('b_name'),
        'user_email' =>$this->input->post('email'),
        'user_image'=>$image,
        'create_date' =>date('Y-m-d')
        );

        $insert_id = $this->common_model->common_insert('barber_dummy',$barber);

        if($insert_id)
        {
           $this->session->set_flashdata('success', 'Dummy Barber registered successfully.');
           redirect(base_url().'barber/dummy');
        }
  } 
     $this->load->view('admin/barber/add_dummy');
  }

  public function dummy_email()
  {
      $email = $this->input->post('email'); 
      $query = $this->db->query("SELECT `user_email` FROM `barber_dummy` WHERE `user_email` = '".$email."'");
     
     if($query->num_rows() > 0){
      echo "10000";exit;
     }
  }

}