<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
	}
	
	public function index()
	{ 
		$this->load->view('web/index');
	}

	public function terms_condition()
	{ 
		$this->load->view('web/terms-conditions');
	}

	public function supply_services()
	{
		$this->load->view('web/terms-conditions-supply-services');
	}

	public function privacy_policy()
	{
		$this->load->view('web/terms-conditions-supply-services');
	}

	public function join_us()
	{
		$this->load->view('web/join-us');
	}

	public function appointment()
	{
		$this->load->view('web/appointment');
	}
	
	public function faq()
	{
		$this->load->view('web/faq');
	}

	public function join_barber()
	{
		if(isset($_POST['submit']))
        { 
        	$v_code = $this->common_model->randomuniqueCode();
    		$v_Code = $this->common_model->encryptor_ym('encrypt',$v_code);

        	$arr = array(
        				 'user_name'=>$this->input->post('fname'),
        				 'user_email'=>$this->input->post('email'),
        				 'user_mobile_num'=>$this->input->post('contact_no'),
        				 'user_location'=>$this->input->post('address'),
        				 'email_code'=>$v_Code,
        				 'user_type'=>2,
        				 'email_status'=>0,
        				 'create_date'=>datetime,
        				 'update_date'=>datetime
        		       );

        	$insert = $this->common_model->common_insert('barber_user',$arr);

        	if(!empty($insert))
        	{
        		 $url = "<a href=".base_url()."barber_api/email_verification/".$this->common_model->encryptor_ym('encrypt',$insert)."/".$v_Code." target='_blank' title='' style='background: #d3a117; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";

		    		$data = array('email'=>$this->input->post('email'),
				                          'name'=>$this->input->post('fname'),
				                           'url'=>$url
				                    );

                    $message = $this->load->view('email_template/template_verify.php',$data,TRUE);
					
                        $config = Array(
                                  'protocol' => 'smtp',
                                  'smtp_host' => 'mail.zohomail.com',
                                  'smtp_port' => 465,
                                  'smtp_user' => 'Barbers@Kwikuts.co.uk',
                                  'smtp_pass' => 'Kwikuts2017',
                                  'mailtype'  => 'html', 
                                  'charset'   => 'utf-8',
                                  'wordwrap'  => TRUE
                                 );

                        /* $this->email->initialize($config);
                         $this->email->set_newline("\r\n");
                         $this->email->from('no-reply@kwikuts.com', 'kwikuts');
                         $this->email->to($this->input->post('email')); 
                         $this->email->subject('KwiKuts App: Verification Link');
                         $this->email->set_mailtype('html');
                             
                         $this->email->message($message); */

                        $this->load->library('email', $config);
                        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
                        $this->email->set_header('Content-type', 'text/html');

                        $this->email->set_newline("\r\n");
          
                        $this->email->from('Barbers@Kwikuts.co.uk', 'Kwikuts');
                        $this->email->to($this->input->post('email')); 
                        $this->email->subject('KwiKuts : Verification Link');

                        $this->email->message($message);

                    if($this->email->send())    
                    {
                    	$this->session->set_flashdata('success', 'You Have Registered Successfully and Verification Link Send to Your Email Address.');
	                    redirect(base_url().'join-us');
                    } 	
        	}
        	else{
        			$this->session->set_flashdata('failed', 'Something Went Wrong please try again later.');
	                redirect(base_url().'join-us');
        		}	

        } 
	}

    public function check_email()
    {
	  $email = $this->input->post('email'); 
	  $query=$this->db->query("SELECT `user_email` FROM `barber_user` WHERE `user_email` = '".$email."' AND `user_type`= 2");

	  if ($query->num_rows() > 0){
		echo "10000";
	  }
    }

    public function check_contact()
    {
      $contact_no = $this->input->post('contact_no'); 

	  $query1 = $this->db->query("SELECT `user_mobile_num` FROM `barber_user` WHERE `user_mobile_num` = '".$contact_no."' AND `user_type`= 2");

	   if ($query1->num_rows() > 0){
		echo "1001";
	  }
    }

    public function make_appointment()
    {
    	if(isset($_POST['submit']))
        { 
        	$service_arr = $this->input->post('service');

        	$services = implode(',',$service_arr);

            $booking_time = date("H:i", strtotime($this->input->post('booking_time')));

        	$arr = array(
        				 'user_name'=>$this->input->post('fname'),
        				 'user_email'=>$this->input->post('email'),
        				 'user_contact'=>$this->input->post('contact_no'),
        				 'user_address'=>$this->input->post('address'),
        				 'services_for'=>$services,
                         'booking_date'=>$this->input->post('booking_date'),
                         'booking_time'=>$booking_time,
        				 'admin_status'=>0,
        				 'create_date'=>datetime,
        				 'update_date'=>datetime
        		       );
           
        	$insert = $this->common_model->common_insert('barber_web_appointment',$arr);

        	if(!empty($insert))
        	{
        		$this->session->set_flashdata('success', 'Your Request Has been Submitted Successfully.');
                redirect(base_url().'appointment');
        	}
            else
            {
                $this->session->set_flashdata('failed', 'Something Went Wrong please try again later.');
                redirect(base_url().'appointment');
            }	

        }	
    }

    public function contactus()
    {
        if(isset($_POST['submit']))
        { 
            $arr = array(
                        'username'=>$this->input->post('name'),
                        'useremail'=>$this->input->post('email'),
                        'message'=>$this->input->post('message')
                       ); 

            $insert = $this->common_model->common_insert('barber_contactus',$arr);

            if(!empty($insert))
            {
                $this->session->set_flashdata('success', 'Your Message Has Been Send Successfully.');
                redirect(base_url());
            }
            else
            {
                $this->session->set_flashdata('failed', 'Something Went Wrong please try again later.');
                redirect(base_url());
            }   
        }
    }
}
