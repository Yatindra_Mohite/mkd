<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_user extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		if(!$userid = $this->session->userdata('admin_id')){
		redirect(base_url('login'));

		$militime = round(microtime(true) * 1000);
		define('militime', $militime);
	  }
	}
	
	public function index()
	{ 
       $data['Admin_user'] = $this->common_model->getData('barber_admin',array('role'=>2),'admin_id','DESC');

	   $this->load->view('admin/admin_user/detail',$data);
	}
	
public function add_admin()
{ 
	if($this->input->server('REQUEST_METHOD') === 'POST')
	{ 
		if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
        { 
            $this->load->library('image_lib');

            $date = date("ymdhis");
            $config['upload_path'] = 'uploads/admin_image1/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = 200; 
            $config['max_width'] = '200';
            $config['max_height'] = '150';

            $subFileName = explode('.',$_FILES['image']['name']);
            $ExtFileName = end($subFileName);
            $config['file_name'] = md5($date.$_FILES['image']['name']).'.'.$ExtFileName;
                      
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('image'))
            {
              $fileData = $this->upload->data();
              $uploadData['file_name'] = $fileData['file_name'];
              $image = $uploadData['file_name'];
            }
            else
            {
              $this->data['err']= $this->upload->display_errors();
              $this->session->set_flashdata('image_error', $this->data['err']);
              redirect('admin_user/add_admin');
              exit;
            }
        }
        else
        { 
            $image = '';
        }

		$administrator = array(
		'name' =>$this->input->post('name'),
		'email' =>$this->input->post('email'),
		'image'=>$image,
		'role'=> '2',
		'status'=>0,
		'create_date' =>date('Y-m-d'),
         'update_date'=>date('Y-m-d')
		);

        $insert_id = $this->common_model->common_insert('barber_admin',$administrator);
    		if($insert_id)
    		{
    			$email = $this->input->post('email');

    			$password =  $this->randno(8);

                $sha_password = md5($password);
         
	            $config = Array(        
                         'mailtype'  => 'html', 
                         'charset'   => 'utf-8'
                             );

             $this->load->library('email', $config);

             $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
             $this->email->set_header('Content-type', 'text/html');

             $this->email->set_newline("\r\n");
          
			 $this->email->from('noreply@barber.com', 'Kartavya');
			 $this->email->to($email); 
			 $this->email->subject('Admin user Registration');
		
		     $data = array('name'=>$this->input->post('name'),
					  'email'=>$email,
					  'password'=>$password
					);

			 $this->email->initialize($config);
	         $this->email->set_newline("\r\n");
	         $this->email->from('noreply@barber.com', 'Barber');
	         $this->email->to($email); 
	         $this->email->subject('Admin Registration');
	         $this->email->set_mailtype('html');

			 $message = $this->load->view('email_template/add_admin_template.php',$data,TRUE);
           
         	 $this->email->message($message); 

         		if($this->email->send())
         		{
         		    $update_info = $this->common_model->updateData('barber_admin',array('password'=>$sha_password),array('admin_id'=>$insert_id));	

         		     $this->session->set_flashdata('success', 'Admin registered successfully and password sent his email address.');
			  		    redirect('admin_user/add_admin');
         		}
    		}
			
	} 

    $this->load->view('admin/admin_user/add');
}

public function check_email()
{
      $email = $this->input->post('email'); 
      $query = $this->db->query("SELECT `email` FROM `barber_admin` WHERE `email` = '".$email."' AND `role`='2'");
     
     if($query->num_rows() > 0){
      echo "10000";exit;
     }
}

public function randno($tot=false)
{
    if($tot=='')
    {
        $tot=8;    
    }
    return $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $tot);    
}

public function change_status()
{  
    $admin_id = $this->input->post('id');
    $status = $this->input->post('status');
    $update = $this->common_model->updateData('barber_admin',array('status'=>$status),array('admin_id'=>$admin_id));

    if($update)
    {
       echo "1000"; 
    }
}


}
