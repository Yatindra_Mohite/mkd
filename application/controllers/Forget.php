 <?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Forget extends CI_Controller {
public function __construct()
{
	parent::__construct();
	
	date_default_timezone_set('Asia/Kolkata');
	$militime =round(microtime(true) * 1000);
	$datetime =date('Y-m-d h:i:s');
	define('militime', $militime);
	define('datetime', $datetime);

	/*cache control*/
	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	$this->output->set_header('Pragma: no-cache');
		
}

public function forgetpassword()
{ 
	if($this->input->server('REQUEST_METHOD') === 'POST')
	{
		$email = $this->input->post('email1');

		$query = $this->db->query('SELECT * FROM `barber_admin` where email="'.$email.'"')->row();
		if(empty($query))
		{ 
		   $this->session->set_flashdata('email_err','1000');
		   redirect('login');
		}
		else{
				$admin_id = $query->admin_id;
				$admin_name = $query->name;
				//$base_url = urlencode(base_url().'forget/changepassword/');
				$code =  $this->common_model->randomuniqueCode();
				$encryted_code = $this->common_model->encryptor_ym('encrypt', $code);
				$type = $this->common_model->encryptor_ym('encrypt', 1);
				
				//$url = base_url().'forget/changepassword/?8f14e45fceea167a5a36dedd4bea2543='.base64_encode($admin_id).'&type='.$type.'&code='.$encryted_code;

				//$url = "<a href=".base_url()."forget/changepassword/?8f14e45fceea167a5a36dedd4bea2543=".base64_encode($admin_id)."&type=".$type."&code=".$encryted_code." target='_blank' style='background: #222; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";

				$url = "<a href=".base_url()."forget/changepassword?8f14e45fceea167a5a36dedd4bea2543=".base64_encode($admin_id)."&type=".$type."&code=".$encryted_code." target='_blank' style='background: #388e3c; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";


						 $config = Array(
				                  'protocol' => 'smtp',
				                  'smtp_host' => 'mail.zohomail.com',
				                  'smtp_port' => 465,
				                  'smtp_user' => 'Barbers@Kwikuts.co.uk',
				                  'smtp_pass' => 'Kwikuts2017',
				                  'mailtype'  => 'html', 
				                  'charset'   => 'utf-8',
				                  'wordwrap'  => TRUE
             					 );

			            $this->load->library('email', $config);

			            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
			            $this->email->set_header('Content-type', 'text/html');

			            $this->email->set_newline("\r\n");
          
						$this->email->from('Barbers@Kwikuts.co.uk', 'Kwikuts');
						$this->email->to($email); 
						$this->email->subject('Forget Password..!');

						$data = array('name'=>'Admin',
									  'url'=>$url
									);

						$message = $this->load->view('email_template/template_forgot_pass',$data,TRUE);
						$this->email->message($message);
						if($this->email->send())
						{  
	 	   				   $this->common_model->updateData('barber_admin',array('code'=>$code),array('admin_id'=>$admin_id));

		        	       $this->session->set_flashdata('email_sent','2000');
						   redirect(base_url().'login');
		        	    }
		        	    else
		        	    { 
		        	    	 show_error($this->email->print_debugger());
		        	    }	

			}	
	}
}

public function changepassword()
{ 
	if(isset($_POST['submit']))
	{ 
	    $this->load->library('form_validation');
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[15]');
		$this->form_validation->set_rules('confrm_password', 'Confirm Password', 'trim|required|matches[new_password]');

		if($this->form_validation->run() == TRUE)
		{ 
			$admin_id = base64_decode($this->input->get('8f14e45fceea167a5a36dedd4bea2543'));
		    $type = $this->input->get('type');
			$ENC_code = 	$this->input->get('code');
		 	$e_code =  $this->common_model->encryptor_ym('decrypt', $ENC_code);

	   		$type = $this->common_model->encryptor_ym('decrypt', $type);

	   		if($type==1)
			{
		 	    $check_encrypted_code = $this->common_model->common_getRow('barber_admin',array('admin_id'=>$admin_id));

		 	    if($check_encrypted_code->code == $e_code)
		 	    { 
		 	        $new_password = $this->input->post('confrm_password');

					$new_arr = array('password'=>md5($new_password),'code'=>'');

		 	        $update = $this->common_model->updateData('barber_admin',$new_arr,array('admin_id'=>$admin_id));
		 	        if($update)
		 	        { 
		 	        	$this->session->set_flashdata("success",'Password has been successfully changed.');	
						redirect('forget/changepassword');
		 	        }	
		 	    }else
		 	    {
		 	    	$message = $this->load->view('email_template/verification_failed.php','',TRUE);
					print_r($message);exit;
		 	    }	
		 	}
		 	elseif($type==2)
			{
				$check_encrypted_code1 = $this->common_model->common_getRow('barber_user',array('user_id'=>$admin_id));

		 	    if($check_encrypted_code1->pass_code == $e_code)
		 	    {
			 	    $new_password = $this->input->post('confrm_password');

					$new_arr = array('user_password'=>sha1($new_password),'pass_code'=>'');

			 	    $update = $this->common_model->updateData('barber_user',$new_arr,array('user_id'=>$admin_id));
			 	    if($update)
			 	    {
			 	       $this->session->set_flashdata("success",'Password has been successfully changed.');	
					   redirect('forget/changepassword');
			 	    }
			 	}else
			 	{
			 	    $message = $this->load->view('email_template/verification_failed.php','',TRUE);
					print_r($message);exit;
			 	}
		 	}    
		}
			
	}	

  	$this->load->view('email_template/change_password');
}

public function checkstatus()
{
  $check_encrypted_code = $this->common_model->common_getRow('users',array());

  $check_encrypted_code = $this->common_model->common_getRow('qalame_admin',array());


  if($check_encrypted_code->code == '')
  {
     echo '1000'; exit;
  }	
}

}





