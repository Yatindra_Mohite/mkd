<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dummy extends MY_Controller {
function __construct() {
		parent::__construct();
		$militime =round(microtime(true) * 1000);
		date_default_timezone_set('Asia/Kolkata');
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
			
		/*if($this->check_authentication() != 'success')
        die;*/
	}
	function registration()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	if($json_array->user_email!='' && $json_array->user_password!='' && $json_array->user_mobile_num!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('barber_user',array('user_email'=>$json_array->user_email,'user_type'=>1));
	    		if(!empty($seleuser))
	    		{
    				if($seleuser->mobile_status ==1)
	    			{	
						if($seleuser->email_status ==1)
		    			{	
							if($seleuser->admin_status==1)
    						{
								$final_output['status'] = 'failed';
		    					$final_output['message'] = 'Email address and mobile number already registered.';
			    			}else
				    		{
				    			$final_output['status'] = '1000';
			    				$final_output['message'] = admin_status;
				    		}
		    			}else
		    			{
		    				$final_output['status'] = 'failed';
	    					$final_output['message'] = "Email address already registered! please login.";
		    			}
	    			}else
	    			{
	    				$selectmob = $this->common_model->common_getRow("barber_user",array('user_mobile_num'=>$json_array->user_mobile_num,'mobile_status'=>1,'user_type'=>1,'user_id !='=>$seleuser->user_id));
	    				if(!empty($selectmob))
	    				{
							$final_output['status'] = 'failed';
	    					$final_output['message'] = 'Mobile number has been already registered! please try another mobile number.';
	                    }else
	                    {
							//$otp = $this->common_model->randomuniqueCode();
	                    	$otp = '1234';
	                    	$update = $this->common_model->updateData("barber_user",array('user_mobile_num'=>$json_array->user_mobile_num,'mobile_code'=>hash('sha256', $otp),'update_date'=>datetime),array('user_id'=>$seleuser->user_id));
	                   		$final_output['status'] = 'success';
	    					$final_output['message'] = 'Successfully registered.';
	    					$final_output['user_id'] = $seleuser->user_id; 
	                    }
	    			}
		    	}else
	    		{
    				$v_code = $this->common_model->randomuniqueCode();
    				$v_Code = $this->common_model->encryptor_ym('encrypt',$v_code);
	    			//$otp = $this->common_model->randomuniqueCode();
		            $otp = '1234';
	    			$selectmob = $this->common_model->common_getRow("barber_user",array('user_mobile_num'=>$json_array->user_mobile_num,'mobile_status'=>1,'user_type'=>1));
    				if(!empty($selectmob))
    				{
						$final_output['status'] = 'failed';
    					$final_output['message'] = 'Mobile number has been already registered! please try another mobile number.';
                    }else
                    {
						$insert = $this->common_model->common_insert("barber_user",array('user_name'=>$json_array->user_name,'user_email'=>$json_array->user_email,'user_mobile_num'=>$json_array->user_mobile_num,'user_type'=>1,'admin_status'=>1,'email_status'=>1,'email_code'=>$v_Code,'mobile_code'=>hash('sha256', $otp),'user_password'=>sha1($json_array->user_password)
						,'create_date'=>datetime,'user_device_id'=>$json_array->user_device_id,'user_device_type'=>$json_array->user_device_type,'user_device_token'=>$json_array->user_device_token));
		    			if(!empty($insert) && $insert != false)
		    			{
							//Send verification mail
		    				$url = "<a href=".base_url()."customer_api/email_verification/".$this->common_model->encryptor_ym('encrypt',$insert)."/".$v_Code." target='_blank' title='' style='background: #d3a117; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";
		    				$data = array('email'=>$json_array->user_email,
				                          'name'=>$json_array->user_name,
				                           'url'=>$url
				                          );

	                      $message = $this->load->view('email_template/template_verify.php',$data,TRUE);
						
							 /* $subject = 'KwiKuts App: Verification Link';
					    	$email_from = 'no-reply@kwikuts.com';
					      	$email = $json_array->user_email;
					      	$headers  = 'MIME-Version: 1.0' . "\r\n";
					        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					        $headers .= 'From: '.$email_from. '\r\n';            // Mail it
					   		@mail($email, $subject,$message,$headers);	*/

					   		$config = Array(
				                  'protocol' => 'smtp',
				                  'smtp_host' => 'mail.zohomail.com',
				                  'smtp_port' => 465,
				                  'smtp_user' => 'Barbers@Kwikuts.co.uk',
				                  'smtp_pass' => 'Kwikuts2017',
				                  'mailtype'  => 'html', 
				                  'charset'   => 'utf-8',
				                  'wordwrap'  => TRUE
             					 );

					            $this->load->library('email', $config);

					            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
					            $this->email->set_header('Content-type', 'text/html');

					            $this->email->set_newline("\r\n");
		          
								$this->email->from('Barbers@Kwikuts.co.uk', 'Kwikuts');
								$this->email->to($json_array->user_email); 
								$this->email->subject('KwiKuts App: Verification Link..!');

								$this->email->message($message);

								$this->email->send();

	                       	$final_output['status'] = 'success';
		    				$final_output['message'] = 'Successfully registered.';
		    				$final_output['user_id'] = $insert; 
	        			}else
		    			{
		    				$final_output['status'] = 'failed'; 
						 	$final_output['message'] = some_error;
		    			}
		    		}
		    	}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = request_params;
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end signup (Y)
	function email_verification()
	{
		$id = $this->uri->segment(3);
		$code = $this->uri->segment(4);
		$id =$this->common_model->encryptor_ym('decrypt',$id);
		
		$select = $this->db->get_where("barber_user ",array('user_id'=>$id))->row();
		if(!empty($select))
		{
				$data = array('email'=>$select->user_email
                             );
          	if($select->email_code == $code && !empty($select->email_code))
			{
				//$this->db->cache_delete('user', 'verified'); //delete cache file
				$update = $this->db->update("barber_user",array('email_code'=>'','email_status'=>1),array('user_id'=>$id));	
				
				$message = $this->load->view('email_template/successfully_verify.php',$data,TRUE);
				print_r($message);exit;
			}else
			{
				$message = $this->load->view('email_template/verification_failed.php',$data,TRUE);
				print_r($message);exit;
			}
		}else
		{
			$message = $this->load->view('email_template/verification_failed.php','',TRUE);
			print_r($message);exit;
		}
	}
	//end email verification (Y) 10-11-17

	function otp_verification()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array->mobile_otp) && !empty($json_array->user_id))
	    {
	    	$checkotp = $this->common_model->common_getRow("barber_user",array('user_id'=>$json_array->user_id,'user_type'=>1));
	    	if(!empty($checkotp))
	    	{	
				if(hash('sha256', $json_array->mobile_otp) == $checkotp->mobile_code)
				{
					if($checkotp->social_id != '')
					{
						$token = bin2hex(openssl_random_pseudo_bytes(16));
        				$token = $token.militime;

        				$updateotp = $this->common_model->updateData("barber_user",array('mobile_code'=>'','mobile_status'=>1,'update_date'=>date('Y-m-d H:i:s'),'user_token'=>$token),array('user_id'=>$json_array->user_id));
					}
					else
					{
						$updateotp = $this->common_model->updateData("barber_user",array('mobile_code'=>'','mobile_status'=>1,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$json_array->user_id));		
						
						$token = $checkotp->user_token;				
					}	

					if($updateotp!=false)
					{
						$image = '';
						if(!empty($checkotp->user_image))
						{
							if (filter_var($checkotp->user_image, FILTER_VALIDATE_URL)) {
								$image = $checkotp->user_image;
							}else
							{
								$image = base_url().'uploads/user_image/'.$checkotp->user_image;
							}
						}


						$object =  array('user_id'=>$checkotp->user_id,
									'user_name'=>$checkotp->user_name,
									'user_image'=>$image,
									'user_email'=>$checkotp->user_email,
									'user_mobile_num'=>$checkotp->user_mobile_num,
									'user_device_type'=>$checkotp->user_device_type,
									'user_device_id'=>$checkotp->user_device_id,
									'user_device_token'=>$checkotp->user_device_token,
									'user_token'=>$token
									);

						$final_output['status'] = 'success';
						$final_output['message'] = 'OTP has been verified successfully.';
						$final_output['data'] = $object;
					}else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = some_error;
					}
				}else
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = "Otp does not match.";
				}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = 'Account does not exists.';		
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = some_error;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	// end otp verification (Y) 10-11-17

	function Resend_otp()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array->user_mobile_num))
	    {
	    	$checkotp = $this->common_model->common_getRow("barber_user",array('user_mobile_num'=>$json_array->user_mobile_num,'user_type'=>1));
	    	if(!empty($checkotp))
	    	{	
					//$otp = $this->common_model->randomuniqueCode();
	            $otp = '1234';
				$updateotp = $this->common_model->updateData("barber_user",array('mobile_code'=>hash('sha256', $otp),'mobile_status'=>0,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$checkotp->user_id));
				if($updateotp!=false)
				{
					$final_output['status'] = 'success';
					$final_output['message'] = 'OTP sent successfully.';
					$final_output['user_id'] = $checkotp->user_id;
				}else
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = some_error;
				}
			}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = 'Account does not exists';		
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	// end otp verification (Y) 10-11-17

	function login()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    { 
	    	if($json_array->user_email!='' && $json_array->user_password!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('barber_user',array('user_email'=>$json_array->user_email,'user_password'=>sha1($json_array->user_password),'user_type'=>1));
	    		if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{
		    			if($seleuser->mobile_status==1)
		    			{	
							if($seleuser->email_status==1)
							{
								$token = bin2hex(openssl_random_pseudo_bytes(16));
        						$token = $token.militime;
	    						$update = $this->common_model->updateData('barber_user',array('user_device_type'=>$json_array->user_device_type,'user_device_id'=>$json_array->user_device_id,'user_device_token'=>$json_array->user_device_token,'user_token'=>$token,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$seleuser->user_id));
	    						
	    						$updatedevicetoken = $this->common_model->updateData('barber_user',array('user_token'=>'','user_device_token'=>''),array('user_id !='=>$seleuser->user_id,'user_device_id'=>$json_array->user_device_id,'user_type'=>$seleuser->user_type));
	    						
	    						$image = '';
								if(!empty($seleuser->user_image))
								{
									if (filter_var($seleuser->user_image, FILTER_VALIDATE_URL)) {
										$image = $seleuser->user_image;
									}else
									{
										$image = base_url().'uploads/user_image/'.$seleuser->user_image;
									}
								}
								$object = array(
									'user_id'=>$seleuser->user_id,
									'user_name'=>$seleuser->user_name,
									'user_image'=>$image,
									'user_email'=>$seleuser->user_email,
									'user_mobile_num'=>$seleuser->user_mobile_num,
									'user_device_type'=>$json_array->user_device_type,
									'user_device_id'=>$json_array->user_device_id,
									'user_device_token'=>$json_array->user_device_token,
									'user_token'=>$token
									);
								$final_output['status'] = 'success';
	    						$final_output['message'] = 'Successfully Login';
	    						$final_output['data'] = $object;
							}else
							{
								$final_output['status'] = 'failed';
								$final_output['message'] = 'Please check your email if you have already verification email or click on resend button for email verification link.';
							}
						}else
		    			{
							$otp = '1234';
							$update = $this->common_model->updateData('barber_user',array('mobile_code'=>hash('sha256', $otp),'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$seleuser->user_id));

							$arr = (object)array('user_id'=>$seleuser->user_id,
										 'user_mobile_num'=>$seleuser->user_mobile_num	
										);

							$final_output['status'] = 'failed';
	    					$final_output['message'] = "Please verify your mobile";
	    					$final_output['data'] = $arr;
		    			}
	    			}else
	    			{
						$final_output['status'] = '1000';
	    				$final_output['message'] = admin_status;
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = 'Invalid login credentials';
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = request_params;
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end login (Y)
	
	function resend_verification_link()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
    		$seleuser = $this->common_model->common_getRow('barber_user',array('user_email'=>$json_array->user_email,'user_type'=>1));
    		if(!empty($seleuser))
    		{
    				$v_code = $this->common_model->randomuniqueCode();
    				$v_Code = $this->common_model->encryptor_ym('encrypt',$v_code);
    				$update = $this->common_model->updateData("barber_user",array('email_code'=>$v_Code),array('user_id'=>$seleuser->user_id));
    				$subject = 'KwiKuts App: Verification Link';
		    		$url = "<a href=".base_url()."customer_api/email_verification/".$this->common_model->encryptor_ym('encrypt',$seleuser->user_id)."/".$v_Code." target='_blank' title='' style='background: #d3a117; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";
                	$data = array('email'=>$json_array->user_email,
		                          'name'=>$seleuser->user_name,
		                           'url'=>$url
		                          );
                    $message = $this->load->view('email_template/template_verify.php',$data,TRUE);

                    $config = Array(
				                  'protocol' => 'smtp',
				                  'smtp_host' => 'mail.zohomail.com',
				                  'smtp_port' => 465,
				                  'smtp_user' => 'Barbers@Kwikuts.co.uk',
				                  'smtp_pass' => 'Kwikuts2017',
				                  'mailtype'  => 'html', 
				                  'charset'   => 'utf-8',
				                  'wordwrap'  => TRUE
             					 );

					            $this->load->library('email', $config);

					            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
					            $this->email->set_header('Content-type', 'text/html');

					            $this->email->set_newline("\r\n");
		          
								$this->email->from('Barbers@Kwikuts.co.uk', 'Kwikuts');
								$this->email->to($json_array->user_email); 
								$this->email->subject('KwiKuts App: Reset Password Link..!');

								$this->email->message($message);

							    $this->email->send();

    				$final_output['status'] = 'success';
    				$final_output['message'] = "Verification link has been sent to your registered email address.";
    		}else
    		{
    			$final_output['status'] = 'failed';
    			$final_output['message'] = 'Account does not exists';
	  		}
		}else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }		
		header("content-type: application/json");
	    echo json_encode($final_output);	exit;
	}
	//end resend verificatin mail (Y)

	function update_profile()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			if($aa['data']->admin_status==1)
			{	$data['user_name'] = $this->input->post('user_name');
				$image = '';
				if(!empty($_FILES['user_image']['name']) && isset($_FILES['user_image']))
				{
					$config = array();
					$config['upload_path']   = './uploads/user_image/';
					$config['allowed_types'] = 'jpg|jpeg|png';

					$subFileName = explode('.',$_FILES['user_image']['name']);
					$ExtFileName = end($subFileName);
				    $images = militime.$_FILES['user_image']['name'];
					//$images = md5(militime.$images).'.png';
					move_uploaded_file($_FILES["user_image"]["tmp_name"],"uploads/user_image/".$images);
					$data['user_image'] = $images;
					$image = base_url().'uploads/user_image/'.$images;
				}else
				{
					$getdata = $this->common_model->common_getRow("barber_user",array('user_id'=>$aa['data']->user_id));	
					if($getdata->user_image!=''){
						if (filter_var($getdata->user_image, FILTER_VALIDATE_URL)) {
							$image = $getdata->user_image;
						}else
						{
							$image = base_url().'uploads/user_image/'.$getdata->user_image;
						}
					}else{
					 	$image = '';
					}
				}
		
				$data['update_date'] = date('Y-m-d H:i:s');
				$insertt = $this->common_model->updateData("barber_user",$data,array('user_id'=>$aa['data']->user_id));
				if($this->db->affected_rows())
				{
					$newdata = $this->db->select('user_name')->get_where("barber_user",array('user_id'=>$aa['data']->user_id))->row();	
					$final_output['status'] = 'success'; 
					$final_output['message'] = 'Profile successfully updated';
					$final_output['data'] = array('user_image'=>$image,'user_name'=>$newdata->user_name);
				}else
				{
					$final_output['status'] = 'failed'; 
					$final_output['message'] = some_error;
				}
			}else
			{
				$final_output['status'] = '1000';
	    		$final_output['message'] = admin_error;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end update profile (Y)
	
	function forgot_password()
	{
		$json = file_get_contents('php://input');
	    $json_array = json_decode($json);
	    $final_output = array();
	    if(!empty($json_array))
	    {
	    	if($json_array->user_email!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('barber_user',array('user_email'=>$json_array->user_email,'user_type'=>1));
	    		if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{
		    			if($seleuser->email_status==1)
		    			{
	    					$code =  $this->common_model->randomuniqueCode();
							$encryted_code = $this->common_model->encryptor_ym('encrypt', $code);
							$type = $this->common_model->encryptor_ym('encrypt', 2);
	    					
	    					$url = "<a href=".base_url()."forget/changepassword?8f14e45fceea167a5a36dedd4bea2543=".base64_encode($seleuser->user_id)."&type=".$type."&code=".$encryted_code." target='_blank' style='background: #388e3c; color: #fff;padding: 11px 47px 11px 47px; border: none;font-weight: 700; border-radius: 7px; margin-top: 0px;text-decoration: none;'>";

	    					$config = Array(        
		            	     'mailtype'  => 'html', 
		            	     'charset'   => 'utf-8'
	   			     	    );

	           				/*$this->email->initialize($config);
	           				$this->email->set_newline("\r\n");
							$this->email->from('Barber@base3.engineerbabu.com', 'Barber');
							$this->email->to($json_array->user_email); 
							$this->email->subject('KwiKuts App: Reset Password Link');
							$this->email->set_mailtype('html');*/
							 $config = Array(
				                  'protocol' => 'smtp',
				                  'smtp_host' => 'mail.zohomail.com',
				                  'smtp_port' => 465,
				                  'smtp_user' => 'Barbers@Kwikuts.co.uk',
				                  'smtp_pass' => 'Kwikuts2017',
				                  'mailtype'  => 'html', 
				                  'charset'   => 'utf-8',
				                  'wordwrap'  => TRUE
             					 );

					            $this->load->library('email', $config);

					            $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
					            $this->email->set_header('Content-type', 'text/html');

					            $this->email->set_newline("\r\n");
		          
								$this->email->from('Barbers@Kwikuts.co.uk', 'Kwikuts');
								$this->email->to($json_array->user_email); 
								$this->email->subject('KwiKuts App: Reset Password Link..!');

							$data = array('email'=>$seleuser->user_email,
                         			'url'=>$url,
                         			'name'=>$seleuser->user_name		
                         				);

							$message = $this->load->view('email_template/template_forgot_pass.php',$data,TRUE);

							$this->email->message($message);

							$this->email->send();

			           		$update = $this->common_model->updateData("barber_user",array("pass_code"=>$code),array('user_id'=>$seleuser->user_id));
			           		
			           		$final_output['status'] = 'success';
    						$final_output['message'] = "Reset password link has been sent to your registered email address.";
		    			}else
		    			{
		    				$final_output['status'] = 'failed';
	    					$final_output['message'] = 'Email address is not verified';
		    			}
	    			}else
	    			{
						$final_output['status'] = '1000';
	    				$final_output['message'] = admin_error;
	    			}
	    		}else
	    		{
	    			$final_output['status'] = 'failed';
	    			$final_output['message'] = 'Email address is not registered.';
	    		}
	    	}else
	    	{
	    		$final_output['status'] = 'failed';
	    		$final_output['message'] = request_params;
	    	}
	    }else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end forgot password (Y)
	function change_password()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
				if($aa['data']->admin_status ==1){
				    $final_output = array();
				    if(!empty($json_array->old_password) && !empty($json_array->new_password))
				    {
				    	$selepas = $this->db->select('user_password')->get_where("barber_user",array('user_id'=>$aa['data']->user_id,'user_password'=>sha1($json_array->old_password)))->row();
				    	if(!empty($selepas))
				    	{
					    	$updatepass= $this->common_model->updateData("barber_user",array('user_password'=>sha1($json_array->new_password),'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$aa['data']->user_id));
					    	if($updatepass==true)
					    	{	
					    		$final_output['status'] = 'success';
		    					$final_output['message'] = "Password has been successfully changed";
					    	}else
					    	{
					    		$final_output['status'] = 'failed';
		    					$final_output['message'] = some_error;
					    	}
				    	}else
				    	{
				    		$final_output['status'] = 'failed';
							$final_output['message'] = "Current password does not match";
				    	}
				    }else
				    {
						$final_output['status'] = 'failed';
						$final_output['message'] = "All parameters are required";
				    }
			}else{
				$final_output['status'] = 'failed';
				$final_output['message'] = admin_status;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	//end Chagne Password (Y)

	function Get_barber_list_by_filter()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
				if($aa['data']->admin_status ==1){
				$final_output = $data = array();
				$available = false;
				$lat = $json_array->lat;
				$lng = $json_array->lng;
				$distance = $json_array->distance;
				$user_id = $aa['data']->user_id;
				$day = $json_array->day;
				$time = $json_array->time;

				$distance = '32';
				
				$time = date("H:i", strtotime($time));

				/*$create_at = $json_array->create_date;

				if($create_at==0)
				{
					$create_at1= "";
				}else
				{
					$create_at1 = "AND barber_user.create_date < '$create_at'";
				}*/
			    if(!empty($json_array->page_no))
	            {
	              $page_no = $json_array->page_no;
	            }
	           	else
	            { 
	              $page_no = 1;  
	            } 

	             $per_set = 4;
	             $from  = ($page_no-1)*$per_set;

				$barberlist	= $this->db->query("SELECT barber_user.create_date,barber_user.user_id,barber_user.user_name,barber_user.user_image,barber_user.about_me,barber_user.user_type,barber_user.user_mobile_num,barber_user.user_location,barber_user.mobile_status,barber_user.admin_status,barber_user.email_status,barber_user.user_lat,barber_user.user_lng,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(`user_lat`) ) 
                                  * cos( radians(`user_lng`) - radians('$lng')) + sin(radians('$lat'))* sin( radians(`user_lat`))))) AS distance,barber_availability.availability,barber_availability.always,barber_availability.".$day." FROM barber_user LEFT JOIN barber_availability ON barber_user.user_id=barber_availability.barber_id HAVING  distance <= '$distance' AND barber_user.mobile_status = 1 AND barber_user.admin_status = 1 AND barber_user.email_status = 1 AND barber_user.user_type = 2 order by distance ASC LIMIT $from,$per_set")->result();
					
				if(!empty($barberlist))
				{

					foreach ($barberlist as $key )
					{

						$image = '';
						if(!empty($key->user_image))
						{
							if (filter_var($key->user_image, FILTER_VALIDATE_URL)) {
								$image = $key->user_image;
							}else
							{
								$image = base_url().'uploads/barber_image/'.$key->user_image;
							}
						}
						
						$revarr = array(); 
						$reviesel = $this->common_model->getDataField('user_name,user_image,rating,review,barber_rating_review.update_date','barber_rating_review',array('barber_id'=>$key->user_id),'rating_id','DESC',array('barber_user'=>'barber_rating_review.user_id=barber_user.user_id'),'2');

						if(!empty($reviesel))
						{
							foreach ($reviesel as $value) {

								$image1 = '';
								if(filter_var($value->user_image, FILTER_VALIDATE_URL))
                          		{
								   $image1 = $value->user_image;
								}
								else
								{
									$image1 = base_url().'uploads/user_image/'.$value->user_image;
								}


								$revarr[] = array(
											'user_name'=>$value->user_name,
											'user_image'=>$image1,
											'rating'=>$value->rating,
											'review'=>$value->review,
											'review_date'=>date('Y-m-d',strtotime($value->update_date))
											);
							}
						}

						$selepas = $this->common_model->common_getRow('barber_favourite_list',array('barber_id'=>$key->user_id,'user_id'=>$user_id));
						if(!empty($selepas))
						{
							$isfave = 1;
						}else
						{
							$isfave = 0;
						}

						$averagerat = 0;
						$averat = $this->db->query("SELECT AVG(rating) as averat FROM barber_rating_review WHERE 'barber_id'= '".$key->user_id."'")->row();

						if(!empty($averat->averat))
						{
							$averagerat = $averat->averat;
						}

						$galimag = array(); 
						$gallarypic = $this->common_model->getDataField('gallary_pic','barber_gallary',array('user_id'=>$key->user_id),'gallary_id','ASC');
						if(!empty($gallarypic))
						{
							foreach ($gallarypic as $gallarypicval) {
								
								/*$galimag[] = array(
											'user_name'=>base_url().'uploads/gallary_image/'.$gallarypicval->gallary_pic,
											);*/
								$galimag[] = base_url().'uploads/gallary_image/'.$gallarypicval->gallary_pic;
							}
						}

						
						if($key->always == 1 OR $key->always == null)
						{
							$available = true;
							$Availability = (object)array(); 

							$data[] = array(
								'barber_id'=>$key->user_id,
								'user_name'=>$key->user_name,
								'user_image'=>$image,
								'user_mobile_num'=>$key->user_mobile_num,
								'user_location'=>$key->user_location,
								'user_lat'=>$key->user_lat,
								'user_lng'=>$key->user_lng,
								'about_me'=>$key->about_me,
								'create_date'=>date('Y-m-d h:i:s',strtotime($key->create_date)),
								'favourite_status'=>$isfave,
								'avg_rating'=>$averagerat,
								'distance'=>$key->distance,
								'availability'=>$Availability,
								'rating_review'=>$revarr,
								'gallary_pic'=>$galimag,
								'always'=>1
								);	
						}
						else if($key->always == 0)
						{
							$available = true;
							$Availability = (object)array(); 
							$from_to = $time_arr = array(); 

							    $arr_search = array('mon'=>0,'tue'=>1,'wed'=>2,'thu'=>3,'fri'=>4,'sat'=>5,'sun'=>6);

							    if(array_key_exists($day,$arr_search))
							    {
							    	$d_day = $arr_search[$day];
							    }	
							   // echo $d_day
							 
								$var = json_decode($key->$day);

								for($i=0;$i<count($var);$i++)
								{ 
									$from = date("H:i", strtotime($var[$i]->from));
									$to = date("H:i", strtotime($var[$i]->to));
									if(($time >= $from) && ($time <= $to))
									{	
										$from_to[] = array('from'=>$var[$i]->from,
														 'to'=>	$var[$i]->to
														);
										
									}
								} 
								   if(!empty($from_to))
								   {
								   	 $time_arr[] = array('day'=>$d_day,'timearr'=>$from_to);
								   	 $Availability = array('timings'=>$time_arr);
								   }
 
							

							$data[] = array(
								'barber_id'=>$key->user_id,
								'user_name'=>$key->user_name,
								'user_image'=>$image,
								'user_mobile_num'=>$key->user_mobile_num,
								'user_location'=>$key->user_location,
								'user_lat'=>$key->user_lat,
								'user_lng'=>$key->user_lng,
								'about_me'=>$key->about_me,
								'create_date'=>date('Y-m-d h:i:s',strtotime($key->create_date)),
								'favourite_status'=>$isfave,
								'avg_rating'=>$averagerat,
								'distance'=>$key->distance,
								'availability'=>$Availability,
								'rating_review'=>$revarr,
								'gallary_pic'=>$galimag,
								'always'=>0
								);
						  
						}	
					}

					if($available = true)
					{
						$final_output['status'] = 'success';
						$final_output['message'] = "Barber List";
						$final_output['data'] = $data;
					}
					else
					{
						$final_output['status'] = 'failed';
						$final_output['message'] = "Barber Not Found";
						unset($final_output['data']);
					}	
					
				}
				else
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = "Barber Not Found";
					unset($final_output['data']);
				}	
				
			}else{
				$final_output['status'] = 'failed';
				$final_output['message'] = admin_status;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}

	function Get_barber_list_by_distance()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
				if($aa['data']->admin_status ==1){
				$final_output = $data = array();
				$lat = $json_array->lat;
				$lng = $json_array->lng;
				$distance = $json_array->distance;
				$user_id = $aa['data']->user_id;
				//$create_at = $json_array->create_date;

				// if($create_at==0)
				// {
				// 	$create_at1= "";
				// }else
				// {
				// 	$create_at1 = "AND barber_user.create_date < '$create_at'";
				// }

				$barberlist	= $this->db->query("SELECT barber_user.create_date,barber_user.user_id,barber_user.user_name,barber_user.user_image,barber_user.about_me,barber_user.user_type,barber_user.user_mobile_num,barber_user.user_location,barber_user.mobile_status,barber_user.admin_status,barber_user.email_status,barber_user.user_lat,barber_user.user_lng,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(`user_lat`) ) 
                                  * cos( radians(`user_lng`) - radians('$lng')) + sin(radians('$lat'))* sin( radians(`user_lat`))))) AS distance,barber_availability.availability FROM barber_user LEFT JOIN barber_availability ON barber_user.user_id=barber_availability.barber_id HAVING  distance <= '32' AND barber_user.mobile_status = 1 AND barber_user.admin_status = 1 AND barber_user.email_status = 1 AND barber_user.user_type = 2 order by distance DESC")->result();
				if(!empty($barberlist))
				{
					foreach ($barberlist as $key ) {
						
						$image = '';
						if(!empty($key->user_image))
						{
							if (filter_var($key->user_image, FILTER_VALIDATE_URL)) {
								$image = $key->user_image;
							}else
							{
								$image = base_url().'uploads/barber_image/'.$key->user_image;
							}
						}

						$Availability = (object)array(); 
						if(!empty($key->availability))
						{
							$Availability = json_decode($key->availability);
						}
						$revarr = array(); 
						$reviesel = $this->common_model->getDataField('user_name,user_image,rating,review,barber_rating_review.update_date','barber_rating_review',array('barber_id'=>$key->user_id),'rating_id','DESC',array('barber_user'=>'barber_rating_review.user_id=barber_user.user_id'),'');
						if(!empty($reviesel))
						{
							foreach ($reviesel as $value) {

								$image1 = '';
								if(filter_var($value->user_image, FILTER_VALIDATE_URL))
                          		{
								   $image1 = $value->user_image;
								}
								else
								{
									$image1 = base_url().'uploads/user_image/'.$value->user_image;
								}


								$revarr[] = array(
											'user_name'=>$value->user_name,
											'user_image'=>$image1,
											'rating'=>$value->rating,
											'review'=>$value->review,
											'review_date'=>date('Y-m-d',strtotime($value->update_date))
											);
							}
						}

						$selepas = $this->common_model->common_getRow('barber_favourite_list',array('barber_id'=>$key->user_id,'user_id'=>$user_id));
						if(!empty($selepas))
						{
							$isfave = 1;
						}else
						{
							$isfave = 0;
						}

						$averagerat = 0;
						$averat = $this->db->query("SELECT AVG(rating) as averat FROM barber_rating_review WHERE 'barber_id'= '".$key->user_id."'")->row();

						if(!empty($averat->averat))
						{
							$averagerat = $averat->averat;
						}
							

						$galimag = array(); 
						$gallarypic = $this->common_model->getDataField('gallary_pic','barber_gallary',array('user_id'=>$key->user_id),'gallary_id','ASC');
						if(!empty($gallarypic))
						{
							foreach ($gallarypic as $gallarypicval) {
								
								$galimag[] =	base_url().'uploads/gallary_image/'.$gallarypicval->gallary_pic;
										
							}
						}	
						$data[] = array(
								'barber_id'=>$key->user_id,
								'user_name'=>$key->user_name,
								'user_image'=>$image,
								'user_mobile_num'=>$key->user_mobile_num,
								'user_location'=>$key->user_location,
								'user_lat'=>$key->user_lat,
								'user_lng'=>$key->user_lng,
								'about_me'=>$key->about_me,
								'create_date'=>date('Y-m-d h:i:s',strtotime($key->create_date)),
								'favourite_status'=>$isfave,
								'avg_rating'=>$averagerat,
								'distance'=>$key->distance,
								'availability'=>$Availability,
								'rating_review'=>$revarr,
								'gallary_pic'=>$galimag
								);	
					}
					$final_output['status'] = 'success';
					$final_output['message'] = "Barber List";
					$final_output['data'] = $data;
				}
				else
				{
					$final_output['status'] = 'failed';
					$final_output['message'] = "Barber Not Found";
					unset($final_output['data']);
				}	
				
			}else{
				$final_output['status'] = 'failed';
				$final_output['message'] = admin_status;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}
	
	function Favourite()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if($aa['data']->admin_status ==1){
				    $final_output = array();
				    $user_id = $aa['data']->user_id;
				    $barber_id= $json_array->barber_id;
				    $status= $json_array->status;	//1=fav, 0=unfav
				    $statuss = '';
				    if(!empty($json_array->barber_id))
				    {
				    	$favouritedata = $this->common_model->common_getRow('barber_favourite_list',array('user_id'=>$user_id,'barber_id'=>$barber_id));
				 		if(!empty($favouritedata))
				 		{
				 			if($status==0)
				 			{
				 				$deletee = $this->common_model->deleteData('barber_favourite_list',array('user_id'=>$user_id,'barber_id'=>$barber_id));
				 			}else
				 			{
				 				$statuss = 'false';
				 			}
				 		}else
				 		{
				 			if($status==0)
				 			{
				 				$statuss = 'false';
				 			}else
				 			{
				 				$deletee = $this->common_model->common_insert('barber_favourite_list',array('user_id'=>$user_id,'barber_id'=>$barber_id,'create_date'=>date('Y-m-d H:i:s')));
				 			}
				 		}
				 		if($statuss=='false')
				 		{
								if($status==0){ $msg= 'Already removed from favourite list'; }else{ $msg= 'Already Added in favourite list'; }
				 				
				 				$final_output['status'] = 'failed';
								$final_output['message'] = $msg;
				 		}else
				 		{
				 			if($deletee==true)
				 			{
				 				if($status==0){ $msg= 'Successfully removed from favourite list'; }else{ $msg= 'Successfully Added in favourite list';}
				 				
				 				$final_output['status'] = 'success';
								$final_output['message'] = $msg;
				 			}else
				 			{
				 				$final_output['status'] = 'failed';
								$final_output['message'] = "Something went wrong! please try again later.";
				 			}
				 		}
				    }else
				    {
						$final_output['status'] = 'failed';
						$final_output['message'] = "All parameters are required";
				    }
			}else{
				$final_output['status'] = 'failed';
				$final_output['message'] = admin_status;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}

	function Favourite_list()
	{
		$aa = $this->check_authentication();
		if($aa['status']=='true')
		{
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);
			if($aa['data']->admin_status ==1){
				    $final_output = $data = array();
				    $user_id = $aa['data']->user_id;
				    $lat = $json_array->lat;
				    $lng = $json_array->lng;
				    $statuss = '';

				    $create_at = $json_array->create_date;

					if($create_at==0)
					{
						$create_at1= "";
					}else
					{
						$create_at1 = "AND barber_user.create_date < '$create_at'";
					}


				$barberlist	= $this->db->query("SELECT barber_user.create_date,barber_user.user_id,barber_user.user_name,barber_user.user_image,barber_user.about_me,barber_user.user_type,barber_user.user_mobile_num,barber_user.user_location,barber_user.mobile_status,barber_user.admin_status,barber_user.email_status,barber_user.user_lat,barber_user.user_lng,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(`user_lat`) ) 
                                  * cos( radians(`user_lng`) - radians('$lng')) + sin(radians('$lat'))* sin( radians(`user_lat`))))) AS distance,barber_favourite_list.create_date as favdate,barber_availability.availability,barber_availability.always FROM barber_user INNER JOIN barber_favourite_list ON barber_user.user_id=barber_favourite_list.barber_id LEFT JOIN barber_availability ON barber_user.user_id=barber_availability.barber_id WHERE barber_user.admin_status = 1 AND barber_user.user_type = 2 AND barber_favourite_list.user_id = '$user_id' ".$create_at1." order by distance DESC")->result();


				if(!empty($barberlist))
				{
					foreach ($barberlist as $key ) {
						
						$image = '';
						if(!empty($key->user_image))
						{
							if (filter_var($key->user_image, FILTER_VALIDATE_URL)) {
								$image = $key->user_image;
							}else
							{
								$image = base_url().'uploads/barber_image/'.$key->user_image;
							}
						}

						$selepas = $this->common_model->common_getRow('barber_favourite_list',array('barber_id'=>$key->user_id,'user_id'=>$user_id));
						if(!empty($selepas))
						{
							$isfave = 1;
						}else
						{
							$isfave = 0;
						}

						$revarr = array(); 
						$reviesel = $this->common_model->getDataField('user_name,user_image,rating,review,barber_rating_review.update_date','barber_rating_review',array('barber_id'=>$key->user_id),'rating_id','DESC',array('barber_user'=>'barber_rating_review.user_id=barber_user.user_id'),'2');
						if(!empty($reviesel))
						{
							foreach ($reviesel as $value) {

								$image1 = '';
								if(filter_var($value->user_image, FILTER_VALIDATE_URL))
                          		{
								   $image1 = $value->user_image;
								}
								else
								{
									$image1 = base_url().'uploads/user_image/'.$value->user_image;
								}	

								$revarr[] = array(
											'user_name'=>$value->user_name,
											'user_image'=>$image1,
											'rating'=>$value->rating,
											'review'=>$value->review,
											'review_date'=>date('Y-m-d',strtotime($value->update_date))
											);
							}
						}
						$averagerat = 0;
						$averat = $this->db->query("SELECT AVG(rating) as averat FROM barber_rating_review WHERE 'barber_id'= '".$key->user_id."'")->row();
						if(!empty($averat->averat) && $averat->averat != null)
						{
							$averagerat = $averat->averat;
						}

						$galimag = array(); 
						$gallarypic = $this->common_model->getDataField('gallary_pic','barber_gallary',array('user_id'=>$key->user_id),'gallary_id','ASC');
						if(!empty($gallarypic))
						{
							foreach ($gallarypic as $gallarypicval) {

							$galimag[] = base_url().'uploads/gallary_image/'.$gallarypicval->gallary_pic;
											
											
							}
						}	

						if($key->always == 1 OR $key->always == null)
						{
							$available = true;
							$Availability = (object)array(); 

							

							$data[] = array(
								'barber_id'=>$key->user_id,
								'user_name'=>$key->user_name,
								'user_image'=>$image,
								'user_mobile_num'=>$key->user_mobile_num,
								'user_location'=>$key->user_location,
								'user_lat'=>$key->user_lat,
								'user_lng'=>$key->user_lng,
								'about_me'=>$key->about_me,
								'create_date'=>date('Y-m-d h:i:s',strtotime($key->create_date)),
								'favourite_status'=>$isfave,
								'avg_rating'=>$averagerat,
								'distance'=>$key->distance,
								'availability'=>$Availability,
								'rating_review'=>$revarr,
								'gallary_pic'=>$galimag,
								'always'=>1
								);	
						}
						else if($key->always == 0)
						{
							$available = true;
							$Availability = (object)array(); 

							if(!empty($key->availability) && $key->availability != null)
							{
								$Availability = json_decode($key->availability);
							}
							/*$from_to = $time_arr = array(); 

							    $arr_search = array('mon'=>0,'tue'=>1,'wed'=>2,'thu'=>3,'fri'=>4,'sat'=>5,'sun'=>6);

							    if(array_key_exists($day,$arr_search))
							    {
							    	$d_day = $arr_search[$day];
							    }	
							 
								$var = json_decode($key->$day);

								for($i=0;$i<count($var);$i++)
								{ 
									$from = date("H:i", strtotime($var[$i]->from));
									$to = date("H:i", strtotime($var[$i]->to));
									if(($time >= $from) && ($time <= $to))
									{	
										$from_to[] = array('from'=>$var[$i]->from,
														 'to'=>	$var[$i]->to
														);
										
									}
								} 
								   if(!empty($from_to))
								   {
								   	 $time_arr[] = array('day'=>$d_day,'timearr'=>$from_to);
								   	 $Availability = array('timings'=>$time_arr);
								   }*/

							$data[] = array(
								'barber_id'=>$key->user_id,
								'user_name'=>$key->user_name,
								'user_image'=>$image,
								'user_mobile_num'=>$key->user_mobile_num,
								'user_location'=>$key->user_location,
								'user_lat'=>$key->user_lat,
								'user_lng'=>$key->user_lng,
								'about_me'=>$key->about_me,
								'create_date'=>date('Y-m-d h:i:s',strtotime($key->create_date)),
								'favourite_status'=>$isfave,
								'avg_rating'=>$averagerat,
								'distance'=>$key->distance,
								'availability'=>$Availability,
								'rating_review'=>$revarr,
								'gallary_pic'=>$galimag,
								'always'=>0
								);
						  
						}

					}
				}
	 			if($available = true)
	 			{
	 				$final_output['status'] = 'success';
					$final_output['message'] = "Favourite list";
					$final_output['data'] = $data;
	 			}else
	 			{
	 				$final_output['status'] = 'failed';
					$final_output['message'] = "Empty favourite list";
	 			}
			}else{
				$final_output['status'] = 'failed';
				$final_output['message'] = admin_status;
			}
		}else
		{
			$final_output = $aa;
		}
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
	}

	  //Socail login for Customer
    function customer_social_login()
    {
    	$json = file_get_contents('php://input');
	    $json_array = json_decode($json);

	    $final_output = array();

	    if(!empty($json_array))
	    {
	    	if($json_array->user_email!='')
	    	{
	    		$seleuser = $this->common_model->common_getRow('barber_user',array('user_email'=>$json_array->user_email,'user_type'=>1));

				if(!empty($seleuser))
	    		{
	    			if($seleuser->admin_status==1)
	    			{

	    				if($seleuser->mobile_status==1)
	    				{

	    					$token = bin2hex(openssl_random_pseudo_bytes(16));
        					$token = $token.militime;

	    					$update = $this->common_model->updateData('barber_user',array('user_device_type'=>$json_array->user_device_type,'user_device_id'=>$json_array->user_device_id,'user_device_token'=>$json_array->user_device_token,'user_token'=>$token,'social_id'=>$json_array->social_id,'user_name'=>$json_array->user_name,'user_image'=>$json_array->user_image,'admin_status'=>1,'email_status'=>1,'update_date'=>date('Y-m-d H:i:s')),array('user_id'=>$seleuser->user_id));
	    						
	    					$updatedevicetoken = $this->common_model->updateData('barber_user',array('user_token'=>'','user_device_token'=>''),array('user_id !='=>$seleuser->user_id,'user_device_id'=>$json_array->user_device_id,'user_type'=>$seleuser->user_type));

	    					if(!empty($seleuser->user_mobile_num))
	    					{
	    						$user_mobile_num = $seleuser->user_mobile_num;
	    					}
	    					else
	    					{
	    						$user_mobile_num = '';
	    					}	

	    					$object = array(
									'user_id'=>$seleuser->user_id,
									'user_name'=>$seleuser->user_name,
									'user_image'=>$json_array->user_image,
									'user_email'=>$seleuser->user_email,
									'user_mobile_num'=>$user_mobile_num,
									'user_device_type'=>$json_array->user_device_type,
									'user_device_id'=>$json_array->user_device_id,
									'user_device_token'=>$json_array->user_device_token,
									'user_token'=>$token
									
									);
								$final_output['status'] = 'success';
	    						$final_output['message'] = 'Successfully Login';
	    						$final_output['data'] = $object;
	    				}
	    				else
	    				{
	    					$final_output['status'] = 'failed';
	    				    $final_output['message'] = 'Verify OTP.';
	    				    $final_output['user_id'] = $seleuser->user_id; 
	    				}		

	    			}
	    			else
	    			{
	    				$final_output['status'] = '1000';
	    				$final_output['message'] = admin_status;
	    			}	
	    		}
	    		else
	    		{
	    			$token = bin2hex(openssl_random_pseudo_bytes(16));
        			$token = $token.militime;

	    			$insert = $this->common_model->common_insert("barber_user",array('user_name'=>$json_array->user_name,'user_email'=>$json_array->user_email,'user_type'=>1,'create_date'=>datetime,'user_device_id'=>$json_array->user_device_id,'user_device_type'=>$json_array->user_device_type,'user_device_token'=>$json_array->user_device_token,'social_id'=>$json_array->social_id,'user_image'=>$json_array->user_image,'admin_status'=>1,'email_status'=>1));

	    			if(!empty($insert))
		    		{
		    			$object = array(
									'user_id'=>$insert,
									'user_name'=>$json_array->user_name,
									'user_image'=>$json_array->user_image,
									'user_email'=>$json_array->user_email,
									'user_device_type'=>$json_array->user_device_type,
									'user_device_id'=>$json_array->user_device_id,
									'user_device_token'=>$json_array->user_device_token,
									'user_token'=>$token
									
									);

		    			    $final_output['status'] = 'success';
		    				$final_output['message'] = 'Verify OTP.';
		    				$final_output['user_id'] = $insert; 
	        		}
	        		else
		    		{
		    				$final_output['status'] = 'failed'; 
						 	$final_output['message'] = some_error;
		    		}
	    		}	
	    	}

	    }
	    else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }

    //for first time user when he comes from social platfrom.
    function mobile_resistration()
    {
    	$json = file_get_contents('php://input');
	    $json_array = json_decode($json);

	    $final_output = array();

	    if(!empty($json_array))
	    {
            $selectmob = $this->common_model->common_getRow("barber_user",array('user_mobile_num'=>$json_array->user_mobile_num,'mobile_status'=>1,'user_type'=>1,'user_id !='=>$json_array->user_id));	

            if(!empty($selectmob))
            {
            	$final_output['status'] = 'failed'; 
			    $final_output['message'] = 'Mobile number has been already registered! please try another mobile number.';
            }
            else
            {
			    $otp = '1234';
            	$updatemobile = $this->common_model->updateData('barber_user',array('user_mobile_num'=>$json_array->user_mobile_num,'mobile_code'=>hash('sha256', $otp),'update_date'=>datetime),array('user_id'=>$json_array->user_id,'user_type'=>'1'));

            	if($updatemobile == true)
            	{
					$final_output['status'] = 'success';
					$final_output['message'] = 'Successfully';
					$final_output['user_id'] = $json_array->user_id; 
            	}
            	else
            	{
            		$final_output['status'] = 'failed'; 
			        $final_output['message'] = some_error;
            	}	
            }	
	    }
	    else
	    {
	    	$final_output['status'] = 'failed';
	    	$final_output['message'] = request_params;
	    }
	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }

    function Rating_and_Review_list()
    {
    	$aa = $this->check_authentication();
		if($aa['status']=='true')
		{ 
			$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = $rating_arr = array();
				$barber_id = $json_array->barber_id;
				$create_at = $json_array->create_at;

				if(!empty($barber_id))
				{
					if($create_at==0)
					{
						$create_at1= "WHERE barber_id = $barber_id";
					}else
					{
						$create_at1= "WHERE create_at < $create_at AND barber_id = $barber_id";
					}

					$query = $this->db->query('SELECT * FROM barber_rating_review '.$create_at1.' ORDER BY rating_id DESC LIMIT 10');
 
					$ratinglist = $query->result();

					if(!empty($ratinglist))
					{
                          foreach($ratinglist as $rating)
                          {
                          	$userdata = $this->db->query('SELECT user_name,user_image FROM barber_user WHERE user_id = '.$rating->user_id.'')->row();

                          	if(!empty($userdata)) 
                          	{
                               $user_name = $userdata->user_name;

                                if (filter_var($userdata->user_image, FILTER_VALIDATE_URL))
                          		{
								   $image = $userdata->user_image;
								}
								else
								{
									$image = base_url().'uploads/user_image/'.$userdata->user_image;
								}

                            } else { $user_name = '';$image = ''; }	

                          	$rating_arr[] = array(
                          				'user_name'=>$user_name,
                          				'user_image'=>$image,
                          				'rating'=>$rating->rating,
                          				'review'=>$rating->review,
                          				'review_date'=>date('Y-m-d',strtotime($rating->update_date)),
                          				'create_at'=>$rating->create_at
                          		    );
                           }
                            $final_output['status'] = 'success';
							$final_output['message'] = "Rating & Review List";
							$final_output['data'] = $rating_arr;

					}
					else
					{
                            $final_output['status'] = 'failed';
							$final_output['message'] = "Rating & Review Not Found.";
							unset($final_output['data']);
					}	
				}
				else
				{
					$final_output['status'] = 'failed';
	    	        $final_output['message'] = request_params;
				}	
		    }
		    else
		    {
		    	$final_output['status'] = 'failed';
				$final_output['message'] = admin_status;
		    }	
		}
		else
		{
			$final_output = $aa;
		}	
		header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }

    function add_rating_review()
    {
       	$aa = $this->check_authentication();
	  	if($aa['status']=='true')
	  	{	
	    	$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		   	if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();
			    if(!empty($json_array))
			    {
			    	if($json_array->rating != '' && $json_array->booking_id != '' && $json_array->barber_id != '')	
			    	{
			    		$booking_id = $json_array->booking_id;
			    		$user_id = $aa['data']->user_id;
			    		$barber_id = $json_array->barber_id;
			    		$rating = $json_array->rating;
			    		$review = $json_array->review;

			    		$checkexisting = $this->common_model->common_getRow("barber_rating_review",array('user_id'=>$user_id,'barber_id'=>$barber_id,'booking_id'=>$booking_id));

			    		if(!empty($checkexisting))
			    		{
			    			$final_output['status'] = 'failed';
			    			$final_output['message'] = 'Already Exist.';

			    		}
			    		else
			    		{
			    			$insert_rating = array('user_id'=>$user_id,
			    								   'booking_id'=>$booking_id,
			    								   'barber_id'=>$barber_id,
			    									'rating'=>$rating,
			    									'review'=>$review,
			    									'create_date'=>datetime,
			    									'update_date'=>datetime,
			    									'create_at'=>militime
			    								  );

			    			$insert = $this->common_model->common_insert("barber_rating_review",$insert_rating);

			    			if(!empty($insert))
			    			{
			    				$final_output['status'] = 'success';
			    				$final_output['message'] = 'Rating & Review Added Successfully.';
			    			}
			    			else
			    			{
			    				$final_output['status'] = 'failed'; 
			        			$final_output['message'] = some_error;
			    			}	
			    		}	
			    	}
			    	else
			    	{
                        $final_output['status'] = 'failed';
			    		$final_output['message'] = request_params;
			    	}	
			    }
			    else
			    {
			    	$final_output['status'] = 'failed';
			    	$final_output['message'] = request_params;
			    }
			} 
			else
			{
				$final_output['status'] = 'failed';
				$final_output['message'] = admin_status;
			}   	
	  	} 
	  	else
		{
			$final_output = $aa;
		} 

	    header("content-type: application/json");
	    echo json_encode($final_output); exit;
    }

    //Customer Prebooking 
    function customer_booking()                                                               
    {
    	$aa = $this->check_authentication();
	  	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();
			    if(!empty($json_array))
			    {
			    	if($json_array->booking_time != ''  && $json_array->payment_id != '' && $json_array->barber_id != '')	
			    	{
			    		$user_id = $aa['data']->user_id;
			    		$barber_id = $json_array->barber_id;
			    		$service_id = $json_array->service_array;
			    		$booking_time = $json_array->booking_time;
			    		$booking_day = $json_array->booking_day;
			    		$booking_date = $json_array->booking_date;
			    		$booking_amount = $json_array->booking_amount;
			    		$payment_id = $json_array->payment_id;

			    	    $current_date = date('Y-m-d');
			    		$date = strtotime(date('Y-m-d'));  
					    $newDate = date('Y-m-d',strtotime('+14 days',$date));
					
						if($booking_date >= $current_date AND $booking_date <= $newDate)
						{ 
						   //$checkexisting = $this->common_model->common_getRow("barber_customer_booking",array('barber_id'=>$barber_id,'booking_date'=>$booking_date,'booking_time'=>$booking_time));
							$checkbarber = $this->db->query("SELECT * FROM barber_customer_booking WHERE barber_id = '$barber_id' AND booking_date = '$booking_date' AND booking_day = '$booking_day'")->result();

							if(!empty($checkbarber))
							{
								foreach($checkbarber as $key)
								{
									 $checkexisting = $this->db->query("SELECT * FROM barber_customer_booking WHERE barber_id = '$barber_id' AND booking_date = '$booking_date' AND '$booking_time' BETWEEN '$key->start_booking_time' AND '$key->end_booking_time' AND booking_status = 1")->row();

									if(!empty($checkexisting))
									{
										$final_output['status'] = 'failed';
				    					$final_output['message'] = 'Barber Already Booked';
				    					header("content-type: application/json");
				    					echo json_encode($final_output); exit;
									}

									$checkinwebappointment = $this->db->query("SELECT * FROM barber_web_appointment WHERE dummy_barber_id = '$barber_id' AND booking_date = '$booking_date' AND '$booking_time' BETWEEN '$key->start_booking_time' AND '$key->end_booking_time' AND admin_status = 1")->row();

									if(!empty($checkinwebappointment))
									{
										$final_output['status'] = 'failed';
				    					$final_output['message'] = 'Barber Already Booked.';
				    					header("content-type: application/json");
				    					echo json_encode($final_output); exit;
									}	
								}	
							}	

				    		$checkbarberavaibility = $this->db->query("SELECT always,".$booking_day." FROM barber_availability WHERE barber_id = ".$barber_id."")->row();


				    		$available = false;
				    		if(!empty($checkbarberavaibility))
				    		{
				    			if($checkbarberavaibility->always == 1)
				    			{
				    				$available = true;
				    			}
				    			else if($checkbarberavaibility->always == 0)
				    			{
				    				$avaibility = json_decode($checkbarberavaibility->$booking_day);

				    				$booking_time_twenty_four = date("H:i", strtotime($booking_time));

				    				for($i=0;$i<count($avaibility);$i++)
			    					{	 
				    					$from = date("H:i", strtotime($avaibility[$i]->from));
										$to = date("H:i", strtotime($avaibility[$i]->to));

										if(($booking_time_twenty_four >= $from) && ($booking_time_twenty_four <= $to))
										{	
											$available = true;
											break;
										}
										
			    					}
				    			}	

				    		}	
				    		if($available == true)
				    		{ 
				    			$booking_code = $this->randno(6);

				    			 $arr = array(
				    			 	          'user_id'=>$user_id,
				    			 			  'barber_id'=>$barber_id,
				    			 			  'service_id'=>json_encode($service_id),
				    			 			  'booking_time'=>$booking_time,
				    			 			  'booking_day' =>$booking_day,
				    			 			  'booking_date'=>$booking_date,
				    			 			  'booking_amount'=>$booking_amount,
				    			 			  'payment_id'=>$payment_id,
				    			 			  'booking_code'=>$booking_code,
				    			 			  'month'=>date('M'),
				    			 			  'year'=>date('Y'),
				    			 			  'booking_status_datetime'=>datetime,
				    			 			  'create_date'=>datetime,
				    			 			  'update_date'=>datetime
				    			 			);

				    			    $insert = $this->common_model->common_insert("barber_customer_booking",$arr);
					    			if($insert)
					    			{
					    				$user_devicetoken = $this->db->query("SELECT user_device_token,user_device_type FROM barber_user WHERE user_id  = '$barber_id'")->row();

					    				if(!empty($user_devicetoken->user_device_token) && ($user_devicetoken->user_device_type == 'android'))
		            					{
		            					  $title = 'Customer Booking Request';	
		            					  $msg = $aa['data']->user_name." sent booking request";
		            		 			  $message = array("title"=>$title,"type"=>1,"message" =>$msg,"currenttime"=>militime);

		                     			  $this->common_model->sendPushNotification($user_devicetoken->user_device_token,$message);

		                     			   $insertnoti = $this->common_model->common_insert('barber_notification',array('sender_id'=>$user_id,'reciever_id'=>$barber_id,'type'=>'1','title'=>$title,'message'=>$msg,'create_at'=>militime));
		            					}
		            					else if(!empty($user_devicetoken->user_device_token) && ($user_devicetoken->user_device_type == 'android'))
		            					{
		            						//ios notification

		            					}	
					    					
					    				$final_output['status'] = 'success';
					    		        $final_output['message'] = 'Successfully Booked.';	
					    			}
					    			else
				    				{
				    					$final_output['status'] = 'failed';
				    		        	$final_output['message'] = some_error;
				    				}	
				    			}
				    			else
				    			{
				    				$final_output['status'] = 'failed';
				    		        $final_output['message'] = 'Barber Not available';
				    			}	
				    		
						}	
						else
						{ 
							$final_output['status'] = 'failed';
				    		$final_output['message'] = 'Pre-booking Available only for 14 days.';
						}

			    	}
			    	else
			    	{
			    		$final_output['status'] = 'failed';
			    		$final_output['message'] = request_params;
			    	}	
						
			    }
			    else
			    {
			    	$final_output['status'] = 'failed';
			    	$final_output['message'] = request_params;
			    }	
		    }
		    else
		    {
		       $final_output['status'] = 'failed';
			   $final_output['message'] = admin_status;
		    }	
	  	}
	  	else
		{
			$final_output = $aa;
		} 

	    header("content-type: application/json");
	    echo json_encode($final_output); exit;	
    }

    function check_favourite_avaibility()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();

		    	if($json_array->booking_time != '' && $json_array->barber_id != '')	
			    {
			    	$user_id = $aa['data']->user_id;
			    	$booking_day = $json_array->booking_day;
			    	$booking_time = $json_array->booking_time;

		    		$time = date("H:i", strtotime($booking_time));
		    		
		    	   $result = $this->db->query("SELECT ".$booking_day." FROM barber_availability WHERE `barber_id` = ".$json_array->barber_id." ")->result();

			    	if(!empty($result))
			    	{
			    		$from_to = array();
			    		$var = json_decode($result[0]->$booking_day);

			    		for($i=0;$i<count($var);$i++)
						{ 

							$from = date("H:i", strtotime($var[$i]->from));
							$to = date("H:i", strtotime($var[$i]->to));
							if(($time >= $from) && ($time <= $to))
							{	
								$from_to[] = array('from'=>$var[$i]->from,
												 'to'=>	$var[$i]->to
												);
							}
						}

						if(!empty($from_to))
						{
							$final_output['status'] = 'success';
				    	    $final_output['message'] = 'Available';
						}
						else
						{
							$final_output['status'] = 'failed';
				    		$final_output['message'] = 'Not Available';
						}	
		    		}
			    	else
			    	{
	                    $final_output['status'] = 'failed';
				    	$final_output['message'] = 'Not Available';
			    	}	

			    }
			    else
			    {
			    	$final_output['status'] = 'failed';
			    	$final_output['message'] = request_params;
			    }	
		    }
		    else
		    {
		       $final_output['status'] = 'failed';
			   $final_output['message'] = admin_status;
		    }	
	  	}
	  	else
		{
			$final_output = $aa;
		} 

	    header("content-type: application/json");
	    echo json_encode($final_output); exit;	
    }

    //Load more booking list
    function booking_list()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();

		    	if($json_array->status != '')	
			    {
			    	$user_id = $aa['data']->user_id;

			    	$create = '';
			        if($json_array->create_date !=0 && $json_array->create_date !='')
			        {
			           $create = "AND create_date < '$json_array->create_date'";
			        }

			        if($json_array->user_type == 1)
          			{
             			 $userid = "user_id = '$user_id'";
			        }else
			        {
			              $userid = "barber_id = '$user_id'";
			        }

			        if($json_array->status == 0)
			        {
			        	$customer_booking = $this->db->query("SELECT * FROM barber_customer_booking WHERE ".$userid." AND booking_status = 0 ".$create." ORDER BY create_date DESC LIMIT 5")->result();
			        }	
			    	else if($json_array->status == 1)
			    	{
			    		$customer_booking = $this->db->query("SELECT * FROM barber_customer_booking WHERE ".$userid." AND booking_status = 1 ".$create." ORDER BY create_date DESC LIMIT 5")->result();
			    	}	
			    	else if($json_array->status == 2)
			    	{
			    		$customer_booking = $this->db->query("SELECT * FROM barber_customer_booking WHERE ".$userid." AND booking_status IN(2,3) ".$create." ORDER BY create_date DESC LIMIT 5")->result();
			    	}	
			    		if(!empty($customer_booking))
			    		{
			    			foreach($customer_booking as $booking)
			    			{
					    	    if($json_array->user_type ==1)
					    	    {
					    	    	$userids = $booking->barber_id;
					    	    	
			                    }
			                    else
			                    {
			                        $userids = $booking->user_id;
			                    }

			                  	$user_detail = $this->db->query("SELECT user_name,user_image,user_email,user_mobile_num FROM barber_user WHERE user_id = '$userids'")->row();

                  				$username = $useremail = $usermobileno = $image =  '';
                  				if(!empty($user_detail))
                  				{
                  					$username = $user_detail->user_name;
                  					$useremail = $user_detail->user_email;
                  					$usermobileno = $user_detail->user_mobile_num;

                  					if($json_array->user_type==1)
		    			  			{
		    			  				$image = base_url().'uploads/barber_image/'.$user_detail->user_image;
		    			  				
		    			  				
		    			  				$booking_code = $booking->booking_code;
		    			  			}
		    			  			else
		    			  			{
		    			  				$booking_code = '';
		    			  				
		    			  				if (filter_var($user_detail->user_image, FILTER_VALIDATE_URL))
                          				{
								   			$image = $user_detail->user_image;
										}
										else
										{
											$image = base_url().'uploads/user_image/'.$user_detail->user_image;
										}

		    			  			}	
                  				}	
								$servicenames = '';
                  				$servicename = $this->db->query("SELECT GROUP_CONCAT(service_name) as service_names FROM barber_services WHERE service_id IN (".$booking->service_id.")")->result();
                  				if(!empty($servicename) && $servicename != 'null')
                  				{ 
                  					$servicenames = $servicename[0]->service_names;
                  				}
                  				$rating_review = $this->db->query("SELECT rating,review,update_date FROM barber_rating_review WHERE booking_id = '$booking->booking_id'")->row();

		    			  		$rating = 0;
		    			  		$review = $reviewdate = '';
		    			  		if(!empty($rating_review))
		    			  		{
		    			  			$rating = $rating_review->rating;
		    			  			$review = $rating_review->review;
		    			  			$reviewdate = date('Y-m-d',strtotime($rating_review->update_date));
		    			  		}	

		    			  		$arr[] = array(
		    			  				'booking_id'=>$booking->booking_id,
		    			  				'user_id'=>$booking->user_id,
		    			  				'user_name'=>$username,
		    			  				'image'=>$image,
		    			  				'user_email'=>$useremail,
		    			  				'user_mobile'=>$usermobileno,
		    			  				'amount'=>$booking->booking_amount,
		    			  				'date_of_request'=>substr($booking->booking_status_datetime, 0,10),
		    			  				'date_of_appointment'=>$booking->booking_date,
		    			  				'booking_status'=>$booking->booking_status,
		    			  				'rating'=>$rating,
					                    'review'=>$review,
					                    'booking_code' =>$booking_code,
					                    'service_name'=>$servicenames,
					                    'review_date'=>$reviewdate,
					                    'create_date'=>$booking->create_date
		    			  			);
			    			}
			    			if(!empty($arr))	
			    			{
			    				$final_output['status'] = 'success';
			    				$final_output['message'] = $arr;
			    			}
			    			else
			    			{
			    				$final_output['status'] = 'failed';
			    				$final_output['message'] = 'Data Not Found';
			    			}	
			    		}
			    		else
			    		{
			    			$final_output['status'] = 'failed';
			    			$final_output['message'] = 'Data Not Found';
			    		}	
			    }
			    else
			    {
			    	$final_output['status'] = 'failed';
			    	$final_output['message'] = request_params;
			    }	
		    }
		    else
		    {
		       $final_output['status'] = 'failed';
			   $final_output['message'] = admin_status;	
		    }	
	  	}
	  	else
	  	{
	  		$final_output = $aa;
	  	}

	  	header("content-type: application/json");
	    echo json_encode($final_output); exit;	
    }

    //pull to refresh booking list
    function booking_list_pull_to_refresh()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();

		    	if($json_array->status != '')	
			    {
			    	$user_id = $aa['data']->user_id;

			    	$create = '';
			        if($json_array->create_date !=0 && $json_array->create_date !='')
			        {
			           $create = "AND create_date > '$json_array->create_date'";
			        }

			        if($json_array->user_type == 1)
          			{
             			 $userid = "user_id = '$user_id'";
			        }else
			        {
			              $userid = "barber_id = '$user_id'";
			        }

			        if($json_array->status == 0)
			        {
			        	$customer_booking = $this->db->query("SELECT * FROM barber_customer_booking WHERE ".$userid." AND booking_status = 0 ".$create." ORDER BY create_date DESC LIMIT 5")->result();
			        }	
			    	else if($json_array->status == 1)
			    	{
			    		$customer_booking = $this->db->query("SELECT * FROM barber_customer_booking WHERE ".$userid." AND booking_status = 1 ".$create." ORDER BY create_date DESC LIMIT 5")->result();
			    	}	
			    	else if($json_array->status == 2)
			    	{
			    		$customer_booking = $this->db->query("SELECT * FROM barber_customer_booking WHERE ".$userid." AND booking_status IN(2,3) ".$create." ORDER BY create_date DESC LIMIT 5")->result();
			    	}	
			    		if(!empty($customer_booking))
			    		{
			    			foreach($customer_booking as $booking)
			    			{
					    	    if($json_array->user_type ==1)
					    	    {
					    	    	$userids = $booking->barber_id;
					    	    	
			                    }
			                    else
			                    {
			                        $userids = $booking->user_id;
			                    }

			                  	$user_detail = $this->db->query("SELECT user_name,user_image,user_email,user_mobile_num FROM barber_user WHERE user_id = '$userids'")->row();

                  				$username = $useremail = $usermobileno = $image =  '';
                  				if(!empty($user_detail))
                  				{
                  					$username = $user_detail->user_name;
                  					$useremail = $user_detail->user_email;
                  					$usermobileno = $user_detail->user_mobile_num;

                  					if($json_array->user_type==1)
		    			  			{
		    			  				$image = base_url().'uploads/barber_image/'.$user_detail->user_image;
		    			  				
		    			  				$booking_code = $booking->booking_code;
		    			  			}
		    			  			else
		    			  			{
		    			  				$booking_code = '';
		    			  				if (filter_var($user_detail->user_image, FILTER_VALIDATE_URL))
                          				{
								   			$image = $user_detail->user_image;
										}
										else
										{
											$image = base_url().'uploads/user_image/'.$user_detail->user_image;
										}

		    			  			}	
                  				}	
								$servicenames = '';
                  				$servicename = $this->db->query("SELECT GROUP_CONCAT(service_name) as service_names FROM barber_services WHERE service_id IN (".$booking->service_id.")")->result();
                  				if(!empty($servicename) && $servicename != 'null')
                  				{ 
                  					$servicenames = $servicename[0]->service_names;
                  				}
                  				$rating_review = $this->db->query("SELECT rating,review,update_date FROM barber_rating_review WHERE booking_id = '$booking->booking_id'")->row();

		    			  		$rating = 0;
		    			  		$review = $reviewdate = '';
		    			  		if(!empty($rating_review))
		    			  		{
		    			  			$rating = $rating_review->rating;
		    			  			$review = $rating_review->review;
		    			  			$reviewdate = date('Y-m-d',strtotime($rating_review->update_date));
		    			  		}	

		    			  		$arr[] = array(
		    			  				'booking_id'=>$booking->booking_id,
		    			  				'user_id'=>$booking->user_id,
		    			  				'user_name'=>$username,
		    			  				'image'=>$image,
		    			  				'user_email'=>$useremail,
		    			  				'user_mobile'=>$usermobileno,
		    			  				'amount'=>$booking->booking_amount,
		    			  				'date_of_request'=>substr($booking->booking_status_datetime, 0,10),
		    			  				'date_of_appointment'=>$booking->booking_date,
		    			  				'booking_status'=>$booking->booking_status,
		    			  				'rating'=>$rating,
					                    'review'=>$review,
					                    'booking_code' =>$booking_code,
					                    'service_name'=>$servicenames,
					                    'review_date'=>$reviewdate,
					                    'create_date'=>$booking->create_date
		    			  			);
			    			}
			    			if(!empty($arr))	
			    			{
			    				$final_output['status'] = 'success';
			    				$final_output['message'] = $arr;
			    			}
			    			else
			    			{
			    				$final_output['status'] = 'failed';
			    				$final_output['message'] = 'Data Not Found';
			    			}	
			    		}
			    		else
			    		{
			    			$final_output['status'] = 'failed';
			    			$final_output['message'] = 'Data Not Found';
			    		}	
			    }
			    else
			    {
			    	$final_output['status'] = 'failed';
			    	$final_output['message'] = request_params;
			    }	
		    }
		    else
		    {
		       $final_output['status'] = 'failed';
			   $final_output['message'] = admin_status;	
		    }	
	  	}
	  	else
	  	{
	  		$final_output = $aa;
	  	}

	  	header("content-type: application/json");
	    echo json_encode($final_output); exit;	
    }


    function booking_cancel()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();

		    	if($json_array->booking_id != '')	
			    {
                   	$user_id = $aa['data']->user_id;

			    	$cancel_booking = $this->common_model->updateData("barber_customer_booking",array('booking_status'=>3,'booking_status_datetime'=>date('Y-m-d h:i:s'),'create_date'=>date('Y-m-d h:i:s'),'reason_of_cancel'=>$json_array->reason),array('booking_id'=>$json_array->booking_id));      

			    	$get_barber_data = $this->db->query("SELECT barber_id FROM barber_customer_booking WHERE booking_id  = '$json_array->booking_id'")->row();



			    	$user_devicetoken = $this->db->query("SELECT user_device_token,user_device_type FROM barber_user WHERE user_id  = '".$get_barber_data->barber_id."'")->row();

				    	if(!empty($user_devicetoken->user_device_token) && ($user_devicetoken->user_device_type == 'android'))
	            		{
    					  $title = 'Customer Booking Cancel';	
    					  $msg = $aa['data']->user_name." sent booking cancel request";
    		 			  $message = array("title"=>$title,"type"=>1,"message"=>$msg,"currenttime"=>militime);

             			  $this->common_model->sendPushNotification($user_devicetoken->user_device_token,$message);

             			  $insertnoti = $this->common_model->common_insert('barber_notification',array('sender_id'=>$user_id,'reciever_id'=>$get_barber_data->barber_id,'type'=>'1','title'=>$title,'message'=>$msg,'create_at'=>militime));
	            		}

			    		$final_output['status'] = 'success';
			    		$final_output['message'] = 'Booking Has Been Cancel Successfully.';
			    }
			    else
			    {
			    	$final_output['status'] = 'failed';
			        $final_output['message'] = request_params;
			    }	
			}
			else
		    {
			    $final_output['status'] = 'failed';
			    $final_output['message'] = admin_status;	
			}	
		}
		else
		{
		    $final_output = $aa;
		}		
		header("content-type: application/json");
		echo json_encode($final_output); exit;
	}

	/*function instant_booking()
	{
	  $aa = $this->check_authentication();
	  if($aa['status']=='true')
	  {
	  	$json = file_get_contents('php://input');
		$json_array = json_decode($json);

		if($aa['data']->admin_status ==1)
		{
			
		}
		else
		{
			$final_output['status'] = 'failed';
			$final_output['message'] = admin_status;
		}	
	  }
	  else
	  {
		$final_output = $aa;
      }	
      header("content-type: application/json");
	  echo json_encode($final_output); exit;
	}	*/

	/*function check_version()
	{
		$json = file_get_contents('php://input');
		$json_array = json_decode($json);
		if($json_array->type=='android')
		{
			$check_version = $this->db->select('min_version')->get_where("app_version",array('min_version <=' => $json_array->version_code,'version_id'=>1))->row();
		}else
		{
			$check_version = $this->db->select('min_version')->get_where("app_version",array('min_version <=' => $json_array->version_code,'version_id'=>2))->row();
		}
		if(!empty($check_version))
		{
			$final_output['status'] = 'success';
	    	$final_output['message'] = 'successfully';
		}else
		{
			$final_output['status'] = 'failed';
	    	$final_output['message'] = constant("version_".$json_array->language."_1");
		}
		header("content-type: application/json");
	    echo json_encode($final_output);	
	}*/
	//end check app version (Y)	

	function get_services()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$final_output = array();
                $service_data = $this->common_model->getData('barber_services',array(),'service_name','DESC');

                if(!empty($service_data))
                {
                	foreach($service_data as $services)
                	{
                		$arr[] = array('service_id'=>$services->service_id,
                					   'service_name'=>$services->service_name,
                					   'service_price'=>$services->service_price
                			);
                	}

                	$final_output['status'] = 'success';
			        $final_output['message'] = 'Services List';
			        $final_output['data'] = $arr;
                }
                else
                {
                	$final_output['status'] = 'failed';
			        $final_output['message'] = 'Services Not Found.';
                }	
		    }	
		    else
		    {
		    	$final_output['status'] = 'failed';
			    $final_output['message'] = admin_status;	
		    }	
	  	}
	  	else
		{
		
			$final_output = $aa;
		}	
	  	header("content-type: application/json");
	    echo json_encode($final_output);exit;
    }

    function search_barber()
    {
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

   			if($aa['data']->admin_status ==1)
		    {
		    	$user_id = $aa['data']->user_id;
		    	$lat = $json_array->lat;
				$lng = $json_array->lng;
				$barbername = $json_array->barber_name;

				$create_at = $json_array->create_date;

				if($create_at==0)
				{
					$create_at1= "";
				}else
				{
					$create_at1 = "AND barber_user.create_date < '$create_at'";
				}

				$barberlist	= $this->db->query("SELECT barber_user.create_date,barber_user.user_id,barber_user.user_name,barber_user.user_image,barber_user.about_me,barber_user.user_type,barber_user.user_mobile_num,barber_user.user_location,barber_user.mobile_status,barber_user.admin_status,barber_user.email_status,barber_user.user_lat,barber_user.user_lng,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(`user_lat`) ) 
                                  * cos( radians(`user_lng`) - radians('$lng')) + sin(radians('$lat'))* sin( radians(`user_lat`))))) AS distance,barber_favourite_list.create_date as favdate,barber_availability.availability FROM barber_user INNER JOIN barber_favourite_list ON barber_user.user_id=barber_favourite_list.barber_id LEFT JOIN barber_availability ON barber_user.user_id=barber_availability.barber_id WHERE barber_user.admin_status = 1 AND barber_user.user_type = 2 AND barber_favourite_list.user_id = '$user_id' ".$create_at1." AND barber_user.user_name LIKE '%".$barbername."%' order by distance DESC LIMIT 2")->result();					

				//print_r($this->db->last_query());exit;
				if(!empty($barberlist))
				{
					foreach ($barberlist as $key ) {
						
						$image = '';
						if(!empty($key->user_image))
						{
							if (filter_var($key->user_image, FILTER_VALIDATE_URL)) {
								$image = $key->user_image;
							}else
							{
								$image = base_url().'uploads/barber_image/'.$key->user_image;
							}
						}

						$Availability =  (object)array(); 
						if(!empty($key->availability) && $key->availability != null)
						{
							$Availability = json_decode($key->availability);
						}

						$revarr = array(); 
						$reviesel = $this->common_model->getDataField('user_name,user_image,rating,review,barber_rating_review.update_date','barber_rating_review',array('barber_id'=>$key->user_id),'rating_id','DESC',array('barber_user'=>'barber_rating_review.user_id=barber_user.user_id'),'2');
						if(!empty($reviesel))
						{
							foreach ($reviesel as $value) {

								$image1 = '';
								if(filter_var($value->user_image, FILTER_VALIDATE_URL))
                          		{
								   $image1 = $value->user_image;
								}
								else
								{
									$image1 = base_url().'uploads/user_image/'.$value->user_image;
								}	

								$revarr[] = array(
											'user_name'=>$value->user_name,
											'user_image'=>$image1,
											'rating'=>$value->rating,
											'review'=>$value->review,
											'review_date'=>date('Y-m-d',strtotime($value->update_date))
											);
							}
						}

						$averagerat = 0;
						$averat = $this->db->query("SELECT AVG(rating) as averat FROM barber_rating_review WHERE 'barber_id'= '".$key->user_id."'")->row();
						if(!empty($averat->averat) && $averat->averat != null)
						{
							$averagerat = $averat->averat;
						}

						$galimag = array(); 
						$gallarypic = $this->common_model->getDataField('gallary_pic','barber_gallary',array('user_id'=>$key->user_id),'gallary_id','ASC');
						if(!empty($gallarypic))
						{
							foreach ($gallarypic as $gallarypicval) {

							     $galimag[] = base_url().'uploads/gallary_image/'.$gallarypicval->gallary_pic;
							}
						}	

						$data[] = array(
								'distance'=>$key->distance,
								'about_me'=>$key->about_me,
								'barber_id'=>$key->user_id,
								'user_name'=>$key->user_name,
								'user_image'=>$image,
								'user_mobile_num'=>$key->user_mobile_num,
								'user_location'=>$key->user_location,
								'user_lat'=>$key->user_lat,
								'user_lng'=>$key->user_lng,
								'create_date'=>date('Y-m-d h:i:s',strtotime($key->create_date)),
								'favourite_status'=>1,
								'avg_rating'=>$averagerat,
								'availability'=>$Availability,
								'rating_review'=>$revarr,
								'gallary_pic'=>$galimag
								);	
					}
				}	
				if(!empty($data))
	 			{
	 				$final_output['status'] = 'success';
					$final_output['message'] = "Favourite list";
					$final_output['data'] = $data;
	 			}else
	 			{
	 				$final_output['status'] = 'failed';
					$final_output['message'] = "Empty favourite list";
	 			}	
		    }
		    else
		    {
		    	$final_output['status'] = 'failed';
			    $final_output['message'] = admin_status;	
		    }	
	  	}
	    else
		{
			$final_output = $aa;
		}	
	  	header("content-type: application/json");
	    echo json_encode($final_output);exit;
    }


    function instant_booking()
    { 
    	$aa = $this->check_authentication();

    	if($aa['status']=='true')
	  	{
	  		$json = file_get_contents('php://input');
		    $json_array = json_decode($json);

		    if($aa['data']->admin_status ==1)
		    {
		    	$user_id = $aa['data']->user_id;
		    	$lat = $json_array->lat;
				$lng = $json_array->lng;
				$day = strtolower(date('D'));
				$current_time = date('H:i'); // 24 Hour Format
				$time = date('h:i A'); // 12 Hour Format
				$current_date = date('Y-m-d');

				$result = $this->check_next_barber($lat,$lng,$day,$current_time,$time,$current_date,$user_id);
		   		print_r($result);exit;
		    }
		    else
		    {
		    	$final_output['status'] = 'failed';
			    $final_output['message'] = admin_status;
		    }	
	  	}
	  	else
		{
			$final_output = $aa;
		}	

	  	header("content-type: application/json");
	    echo json_encode($final_output);exit;
    }

    function check_next_barber($lat='',$lng='',$day='',$currenttime='',$time='',$current_date='',$userid)
    {
		$aa = 1;
		$response = array();
		$barberlist	= $this->db->query("SELECT barber_user.user_id,barber_user.user_name,barber_user.user_type,barber_user.mobile_status,barber_user.admin_status,barber_user.email_status,barber_user.user_lat,barber_user.user_lng,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(`user_lat`) )  * cos( radians(`user_lng`) - radians('$lng')) + sin(radians('$lat'))* sin( radians(`user_lat`))))) AS distance,barber_availability.always,barber_customer_booking.start_booking_time,barber_customer_booking.end_booking_time,COALESCE(barber_customer_booking.booking_status,0) as bookstatus ,barber_availability.".$day." FROM barber_user LEFT JOIN barber_availability ON barber_user.user_id=barber_availability.barber_id LEFT JOIN barber_customer_booking ON (barber_user.user_id = barber_customer_booking.barber_id AND DATE(barber_customer_booking.booking_status_datetime) = CURDATE() AND barber_customer_booking.booking_status != 0) HAVING  distance <= '32' AND barber_user.mobile_status = 1 AND barber_user.admin_status = 1 AND barber_availability.always IS NOT NULL AND barber_user.email_status = 1 AND barber_user.user_type = 2 order by distance ASC")->result();
			foreach($barberlist as $barber)
			{
				$allocate_barber = $this->db->query("SELECT booking_id,allocate_barber FROM `barber_customer_booking` WHERE user_id = '$userid' AND DATE(booking_status_datetime) = CURDATE() AND FIND_IN_SET(".$barber->user_id.",allocate_barber)")->row();
				if(empty($allocate_barber))
				{
					if($barber->bookstatus == 1)
					{
						$checktime = $this->db->query("SELECT * FROM `barber_customer_booking` WHERE barber_id = '".$barber->user_id."' AND '$time' BETWEEN start_booking_time AND end_booking_time")->row();
						if(empty($checktime))
						{
							$barberid = $barber->user_id;
						}else
						{
							continue;
						}
					}else
					{
						$barberid = $barber->user_id;
					}

					$barber_avaibility =  $this->db->query("SELECT * FROM barber_web_appointment
			 					WHERE dummy_barber_id = '$barberid' AND booking_date = '".$current_date."' AND admin_status = 1 AND '$time' BETWEEN start_booking_time AND end_booking_time")->row();
					if(empty($barber_avaibility))
					{
						if(!isset($allocate_barber->booking_id))
						{
							$bookiid = 0;
							$allocate = '';
						}else
						{
							$bookiid = $allocate_barber->booking_id;
							$allocate = $allocate_barber->allocate_barber;
						}
						$barber->booking_id = $bookiid;
						$barber->allocate_barber = $allocate;
						$barber->lat = $lat;
						$barber->lng = $lng;
						$barber->day = $day;
						$barber->currenttime = $currenttime;
						$barber->time = $time;
						$barber->current_date = $current_date;
						$barber->userid = $userid;
						$response = $this->getbarber($barber);
						if(!empty($response))
						{
							return $response;
						}
						break;
					}
				}else
				{
					continue;
				}
			}

    }

    function getbarber($data)
    {
    		//$userid = $data->user_id;
    		//echo date("Y-m-d h:i:s");
    		
    	print_r($data);exit;
    	if($data->always == 1)
		{
			$barberdata = $data;
		}
	 	// else if($data->always == 0)
		// {  
		// 	$days = json_decode($data->$day);

		// 	for($i=0;$i<count($days);$i++)
		// 	{ 
		// 		$from = date("H:i", strtotime($days[$i]->from));
		// 		$to = date("H:i", strtotime($days[$i]->to));

		// 		if(($current_time >= $from) && ($current_time <= $to))
		// 		{	
		// 			$barberdata[] = array('barber_id'=>$data->user_id,'distance'=>$data->distance);
		// 		}
		// 	}
		// }	
		if(!empty($barberdata) )
		{
			if($barberdata->booking_id == 0)
			{
				$booking_code = $this->randno(6);
				
					$day = strtolower(date('D'));
					$current_time = date('H:i'); // 24 Hour Format
					$time = date('h:i A'); // 12 Hour Format
					$current_date = date('Y-m-d');

					$arr = array(
				 	          'user_id'=>$user_id,
				 			  'barber_id'=>$barberdata->user_id,
				 			  'service_id'=>'',
				 			  'booking_time'=>$time,
				 			  'booking_day' =>$day ,
				 			  'booking_date'=>$current_date,
				 			  'booking_amount'=>'',
				 			  'payment_id'=>'',
				 			  'booking_code'=>$booking_code,
				 			  'month'=>date('M'),
				 			  'year'=>date('Y'),
				 			  'booking_type'=>1,
				 			  'current_lat'=>'',
				 			  'current_long'=>'',
				 			  'allocate_barber'=>$barberdata->user_id,
				 			  'booking_status_datetime'=>datetime,
				 			  'create_date'=>datetime,
				 			  'update_date'=>datetime
		    			 	);

				$instant_booking = $this->common_model->common_insert('barber_customer_booking',$arr);
				$bookingid = $this->db->insert_id();
			}else
			{
				$new_allocate = $barberdata->allocate_barber.','.$barberdata->user_id;
				$instant_booking = $this->common_model->updateData("barber_customer_booking",array('allocate_barber'=>$new_allocate),array('booking_id'=>$barberdata->booking_id));
				$bookingid = $barberdata->booking_id;
			}
			
			if($instant_booking)
			{  
				$counter = 0;
	    		$curnt = strtotime(date("Y-m-d h:i:s"));
				$curnt_second = strtotime(date("Y-m-d h:i:s", strtotime("+50 seconds")));
				for ($i=$curnt; $i <= $curnt_second ; $i++) { 
					$i = strtotime(date("Y-m-d h:i:s"));
					$allocate_barber = $this->db->query("SELECT booking_status FROM `barber_customer_booking` WHERE booking_id = '$bookingid'")->row();
						$counter++;
					if(!empty($allocate_barber))
					{
						if($allocate_barber->booking_status != 0)
						{
							return $allocate_barber->booking_status;
						}
						break;	
					}
					if($i == $curnt_second)
					{
						$this->check_next_barber($barberdata->lat,$barberdata->lng,$barberdata->day,$barberdata->currenttime,$barberdata->time,$barberdata->current_date,$barberdata->userid);
					}
					sleep(1);
				}
				// $final_output['status'] = 'success';
				// $final_output['message'] = "Booking Successfully";
			}
		}


    }


  //   function check_avaibility_by_cron()
  //   { 
  //   	   $current_date = date('Y-m-d');
  //       $date = date('h:i:s');

		// $day = strtolower(date('D'));
		// $current_time = date('H:i'); // 24 Hour Format
		// $time = date('h:i A'); // 12 Hour Format

		// $date = date('Y-m-d h:i:s');

  //       $results = $this->db->query("SELECT `booking_status_datetime`,`barber_id`,`user_id`,`booking_id`,`current_lat`,`current_long`,`allocate_barber` FROM `barber_customer_booking` WHERE `booking_date` = '".$current_date."' AND `booking_status` = 0 AND `booking_type` = 1")->result();
      
  //       if(!empty($results))
  //       {
  //       	foreach($results as $result)
  //       	{
  //       		$a = $result->booking_status_datetime;
  //       		$booking_id = $result->booking_id;

  //       		$time_to_second = $this->db->query("SELECT TIME_TO_SEC(TIMEDIFF('$date','$a')) as time_in_second FROM barber_customer_booking  WHERE booking_id = '$booking_id'")->row();

  //       		if($time_to_second->time_in_second > 60)
  //       	    {
  //       	    	$barber_id = $result->allocate_barber;
  //       	    	$lat = $result->current_lat;
		// 			$lng = $result->current_long;


  //       	    	$barberlist	= $this->db->query("SELECT barber_user.user_id,barber_user.user_name,barber_user.user_type,barber_user.mobile_status,barber_user.admin_status,barber_user.email_status,barber_user.user_lat,barber_user.user_lng,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(`user_lat`) ) 
  //                                 * cos( radians(`user_lng`) - radians('$lng')) + sin(radians('$lat'))* sin( radians(`user_lat`))))) AS distance,barber_availability.always,barber_availability.".$day." FROM barber_user LEFT JOIN barber_availability ON barber_user.user_id=barber_availability.barber_id HAVING  distance <= '5000' AND barber_user.mobile_status = 1 AND barber_user.admin_status = 1 AND barber_availability.always IS NOT NULL AND barber_user.email_status = 1 AND barber_user.user_type = 2 order by distance ASC")->result();
        	    		

  //       	    		foreach($barberlist as $barber)
		// 				{
		// 					if($barber->always == 1)
		// 					{
		// 					     $barber_arr[] = array('barber_id'=>$barber->user_id,'distance'=>$barber->distance);
								
		// 					}

		// 				    else if($barber->always == 0)
		// 					{  
		// 						$days = json_decode($barber->$day);

		// 						for($i=0;$i<count($days);$i++)
		// 						{ 
		// 							$from = date("H:i", strtotime($days[$i]->from));
		// 							$to = date("H:i", strtotime($days[$i]->to));

		// 							if(($current_time >= $from) && ($current_time <= $to))
		// 							{	
		// 									$barber_arr[] = array('barber_id'=>$barber->user_id,'distance'=>$barber->distance);
		// 							}
		// 						}
		// 					}	
		// 				}
						
		// 			$explode_arr =  explode(',',$barber_id);
					
		// 	    	for($i=0;$i<count($barber_arr);$i++)
		// 	    	{
		// 	    		if(in_array($barber_arr[$i]['barber_id'], $explode_arr))
		// 	    		{
		// 	    			unset($barber_arr[$i]);
		// 	    		}	
		// 	    	}
		// 	    	//print_r($barber_arr);exit;

		// 	    	for($i=0;$i<count($barber_arr);$i++)
		// 			{
		// 				$check_avaibility =  $this->db->query("SELECT * FROM barber_customer_booking WHERE barber_id = ".$barber_arr[$i]['barber_id']." AND booking_time = '".$time."' AND booking_date = '".$current_date."' AND booking_status IN (0,1)")->row();
						
		// 				if(empty($check_avaibility))
		// 				{
		// 					$barber_avaibility =  $this->db->query("SELECT * FROM barber_web_appointment
		//  					WHERE dummy_barber_id = ".$barber_arr[$i]['barber_id']." AND booking_time = '".$time."' AND booking_date = '".$current_date."' AND admin_status IN (0,1)")->row();
 					
		//  					if(empty($barber_avaibility))
		//  					{
		//  						$barbers_available[] =  array('distance'=>$barber_arr[$i]['distance'],'barber_id'=>$barber_arr[$i]['barber_id'] );
		//  					}
		//  					else
		//  					{ 
		//  						continue;
		//  					}	
		// 				}

		// 			}   

  //       	    }
       		
  //       	}	

  //       	$barbers_available

  //       }	
  //   }

    function check_avaibility_by_cron()
    {
    	$current_date = date('Y-m-d');
  		$date = date('h:i:s');
		$day = strtolower(date('D'));
		$current_time = date('H:i'); // 24 Hour Format
		$time = date('h:i A'); // 12 Hour Format
		echo $date = date('Y-m-d h:i:s');exit;

		$results = $this->db->query("SELECT `booking_status_datetime`,`barber_id`,`user_id`,`booking_id`,`current_lat`,`current_long`,`allocate_barber` FROM `barber_customer_booking` WHERE `booking_date` = '".$current_date."' AND `booking_status` = 0 AND `booking_type` = 1")->result();

		//print_r($results);exit;

		 if(!empty($results))
         {
        	foreach($results as $result)
        	{
        		$a = $result->booking_status_datetime;
        		$booking_id = $result->booking_id;
        		$user_id = $result->user_id;

        		$time_to_second = $this->db->query("SELECT TIME_TO_SEC(TIMEDIFF('$date','$a')) as time_in_second FROM barber_customer_booking  WHERE booking_id = '$booking_id'")->row();

	        		if($time_to_second->time_in_second > 60)
	         	    {
	         	    	$barber_id = $result->allocate_barber;
	        	    	$lat = $result->current_lat;
			 			$lng = $result->current_long;

	        	    	$barberlist	= $this->db->query("SELECT barber_user.user_id,barber_user.user_name,barber_user.user_type,barber_user.mobile_status,barber_user.admin_status,barber_user.email_status,barber_user.user_lat,barber_user.user_lng,(( 3959 * acos( cos( radians('$lat') ) * cos( radians(`user_lat`) ) 
	                                  * cos( radians(`user_lng`) - radians('$lng')) + sin(radians('$lat'))* sin( radians(`user_lat`))))) AS distance,barber_availability.always,barber_availability.".$day." FROM barber_user LEFT JOIN barber_availability ON barber_user.user_id=barber_availability.barber_id HAVING  distance <= '5000' AND barber_user.mobile_status = 1 AND barber_user.admin_status = 1 AND barber_availability.always IS NOT NULL AND barber_user.email_status = 1 AND barber_user.user_type = 2 AND barber_user.user_id NOT IN($barber_id) order by distance ASC")->result();

	        	    		foreach($barberlist as $barber)
							{	
								if($barber->always == 1)
								{
							     	$barber_user_id = $barber->user_id;
								}
						   		else if($barber->always == 0)
								{  
								   $days = json_decode($barber->$day);

									for($i=0;$i<count($days);$i++)
									{ 
										$from = date("H:i", strtotime($days[$i]->from));
										$to = date("H:i", strtotime($days[$i]->to));

										if(($current_time >= $from) && ($current_time <= $to))
										{	
											$barber_user_id = $barber->user_id;
										}

										continue;
									}
								}	

								$check_avaibility =  $this->db->query("SELECT * FROM barber_customer_booking WHERE barber_id = ".$barber_user_id." AND booking_time = '".$time."' AND booking_date = '".$current_date."' AND booking_status IN (0,1)")->row();
						
								if(empty($check_avaibility))
								{
									$barber_avaibility =  $this->db->query("SELECT * FROM barber_web_appointment
				 					WHERE dummy_barber_id = ".$barber_user_id." AND booking_time = '".$time."' AND booking_date = '".$current_date."' AND admin_status IN (0,1)")->row();
		 					
				 					if(empty($barber_avaibility))
				 					{
				 						$arr = array(
				    			 			  'barber_id'=>$barber_user_id,
				    			 			  'booking_time'=>$time,
				    			 			  'allocate_barber'=>$barber_id.','.$barber_user_id,
				    			 			  'booking_status_datetime'=>datetime,
				    			 			  'create_date'=>datetime,
				    			 			  'update_date'=>datetime
				    			 			);

				 						$cancel_booking = $this->common_model->updateData("barber_customer_booking",$arr,array('booking_id'=>$booking_id));

				 						break;

				 					}
				 					else
				 					{ 
				 						continue;
				 					}	
								}

							}
	        	    	
	        	    } 		

         	}
        }
        die('not found');
       // exit();

    }

   /* function check_prebooking_by_cron()
    {

		$results = $this->db->query("SELECT `booking_time`,`booking_status_datetime`,`barber_id`,`user_id`,`booking_id` FROM `barber_customer_booking` WHERE `booking_status` = 0 AND `booking_type` = 0 AND DATE(`booking_status_datetime`) = CURDATE() ")->result();

		if(!empty($results))
		{
			foreach($results as $result)
			{
				$a = $result->booking_status_datetime;
				$booking_time = $result->booking_time;
				$current_date = date('Y-m-d h:i:s');
				$booking_id = $result->booking_id;

				$time_to_second = $this->db->query("SELECT TIME_TO_SEC(TIMEDIFF('$current_date','$a')) as time_in_second,booking_id FROM barber_customer_booking  WHERE booking_id = '$booking_id' AND `booking_status` = 0 AND `booking_type` = 0")->row();

				if($time_to_second->time_in_second > 3600)
				{
					
					$cancel_booking = $this->common_model->updateData("barber_customer_booking",array('booking_status'=>3),array('booking_id'=>$time_to_second->booking_id));
				}

					
			}	
		}

    }*/


	function check_authentication()
	{
	    $response = '';
	 	$headers = $_SERVER['HTTP_SECRET_KEY'];

		if(!empty($headers))
		{
			$check = $this->ChechAuth($headers);
			if($check['status']=="true")
			{
				$final_output['data'] = $check['data'];
				$final_output['status'] = "true";
			}else
			{
				$final_output['status'] ="false";
				$final_output['message'] = "Invalid Token";
			}   
		}else
		{
			$final_output['status'] ="false";
			$final_output['message'] = "Unauthorised access";
		}
	    return $final_output;	
	}
	
	function ChechAuth($token)
	{
		$auth = $this->common_model->getDataField('user_id,admin_status,user_name','barber_user',array('user_token'=>$token));
		if(!empty($auth))
		{
			$abc['status'] = "true";
			$abc['data'] = $auth[0];
			return $abc;
		}else
		{
			$abc['status'] = "false";
			return $abc;
		}
	}

   public function randno($tot=false)
   {
    if($tot=='')
    {
        $tot=8;    
    }
    return $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $tot);    
  }

	
	
}
?>