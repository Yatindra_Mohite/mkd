<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Services extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		if(!$userid = $this->session->userdata('admin_id')){
			redirect(base_url('login'));
		   }

		$response = $this->common_model->check_auth($this->session->userdata('admin_id'));
	    if($response == 1001)
	    {
	       redirect(base_url().'Logout');
	    }    
  }

	public function details()
	{
		$data['service_data'] = $this->common_model->getData('barber_services',array());

		$this->load->view('admin/service/service_list',$data);
	}

	public function update_price()
	{
		
		$service_id = $this->input->post('service_id');

		$price = $this->input->post('price');

		$update_price = $this->common_model->updateData('barber_services',array('service_price'=>$price),array('service_id'=>$service_id));

		if($update_price)
		{	
			$getdata = $this->common_model->common_getRow('barber_services',array('service_id'=>$service_id));
			echo json_encode($getdata);
		}
		
	}
}