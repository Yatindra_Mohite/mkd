<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Menu extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$militime=round(microtime(true) * 1000);
		define('militime', $militime);
		//if(!$userid = $this->session->userdata('admin_id')){
			//redirect(base_url('login'));
		//}

     // $response = $this->common_model->check_auth($this->session->userdata('admin_id'));
     // if($response == 1001)
      //{
       // redirect(base_url().'Logout');
    //  }    
  }

  // public function index()
  // {
  //   $data['products'] = $this->common_model->getData('Product',array(),'id','DESC');

  //   $this->load->view('admin/product/product',$data);
  // }

  public function index()
  { 
   /* $data['products'] = $this->db->query("SELECT cate.CategoryName,cate.Id,GROUP_CONCAT(Product.Name) as proname,GROUP_CONCAT(Product.Id) as proid FROM `Product` INNER JOIN ProductCategory as cate ON cate.Id = Product.CategoryId GROUP BY Product.CategoryId")->result();*/
  	$aa= '';
    $mainid = $this->input->post('maincategory');
    $weekid = $this->input->post('weekid');

    if(!empty($mainid) && ctype_digit($mainid))
  	{
  		//echo $mainid; exit;
  	}else
  	{
  		$aa = 1;
  		$mainid = 1;
  		$weekid = 1;
  	}
    if($mainid == 1)
  	{ 
    $colum = 'IsInBreakFast';
  	}
  	elseif ($mainid == 2) {
    $colum = 'IsInLunch';
  	}else
  	{
    $colum = 'IsInDinner';
  	}  
  		$this->db->join("Product","Product.Id = TiffenProductSchedule.ProductId","INNER");	
  	  $data['menudata'] = $this->db->select("Product.Name,TiffenProductSchedule.*")->get_where("TiffenProductSchedule",array('WeekDay'=>$weekid,$colum=>1))->result();
      
      if($aa==1)
      { 
      	$this->load->view('admin/menu/menu',$data);
        //print_r($data);
      }else
      {
      	if(!empty($data['menudata']))
        {
                $result['status'] = true;
                $result['data'] = $data;
                //echo json_encode($result);
                // $trdata[] = "<tr>
                //     <td><center> " $product->Name " </center></td>
                //     <td><center><a href='" base_url("product/edit/".$product->ProductId); "'><span class='label label-sm btn green'><i class='fa fa-pencil'></i></span></a></center></td>
                // </tr>";
        }
        else
        { 
             $result['status'] = false;
             echo json_encode($result);
        }
        
      }
  }


  public function manage_menu()
  { 
    $data['products'] = $this->db->query("SELECT cate.CategoryName,cate.Id,GROUP_CONCAT(Product.Name) as proname,GROUP_CONCAT(Product.Id) as proid FROM `Product` INNER JOIN ProductCategory as cate ON cate.Id = Product.CategoryId WHERE CategoryId != 6 GROUP BY Product.CategoryId")->result();
    if($this->input->server('REQUEST_METHOD') === 'POST')
    { 
        $prod_arr = $this->input->post('langOptgroup');
        $weekday = $this->input->post('week_name');
        $maincategory = $this->input->post('category');
       // $amount = $this->input->post('amount');
        $breakfst = $lunch = $dinner = 0;
        if($maincategory==1){ $breakfst = 1; }elseif($maincategory==2){ $lunch = 1;}else{ $dinner = 1;}
        $inserarr = array();
        
        if(!empty($prod_arr))
        {
        	$implod = implode(',',$prod_arr);
        	$selected = $this->db->query("SELECT ProductId FROM TiffenProductSchedule WHERE WeekDay = '$weekday' AND IsInBreakFast = '$breakfst' AND IsInLunch = '$lunch' AND IsInDinner = '$dinner' AND ProductId IN (".$implod.")")->result();
        	if(!empty($selected))
        	{
        		foreach ($selected as $value) {
        			$sel[] = $value->ProductId;
        		}
        		$kk = array_merge(array_diff($prod_arr, $sel), array_diff($sel, $prod_arr));
            	//$kk = array_diff($kk, $id);
        	}else
        	{
        		$kk = $prod_arr;
        	}
        	foreach ($kk as $key) {
              	$inserarr[] = "('','$weekday','".$key."',$breakfst,$lunch,$dinner,1,'".date('Y-m-d H:i:s')."')";
            }

            $arr_imp = implode(',', $inserarr);
            $insertdata = $this->db->query("INSERT INTO TiffenProductSchedule VALUES ".$arr_imp."");
            if($insertdata)
            {
              	$this->session->set_flashdata('weekd', $weekday);
                $this->session->set_flashdata('cate', $maincategory);
                $this->session->set_flashdata('success', 'Menu successfully Added.');
              	redirect(base_url().'menu/manage_menu');
            }
        }  
    }

    $this->load->view('admin/menu/manage_menu',$data);
  }

  public function subscription()
  { 
     //$data['subscription'] = $this->db->query("SELECT * FROM SubscriptionPlan WHERE Status = 1")->result();
    $data['category'] = $this->db->query("SELECT CategoryName,Id FROM `ProductCategory` ORDER BY Sequence ASC")->result();
     if($this->input->server('REQUEST_METHOD') === 'POST')
    { 
        $data1['Name'] = $this->input->post('name');
        $data1['Price'] = $this->input->post('price');
        $data1['ValidTillMonthCount'] = $this->input->post('month_count');
        $data1['MainCategory'] = $this->input->post('category');
        $prod_arr = $this->input->post('langOpt3');
        $qty = $this->input->post('qty');
        $inserarr = array();
        if(!empty($prod_arr))
        {
            $impprod = implode(',', $prod_arr);
            $impqty = implode(',', $qty);
            $data1['ProductCategory']= $impprod;
            $data1['ProductCategoryQty']= $impqty;
            $data1['CreateOn']= date('Y-m-d H:i:s');
            $data1['Status']= 1;
            //$inserarr = "('','$name','','$maincategory','$subsprice',$validity_count,'$impprod','".date('Y-m-d H:i:s')."',1)";
           	//$insertdata = $this->db->query("INSERT INTO TiffenProductSchedule VALUES ".$inserarr."");
           	$insertdata = $this->common_model->common_insert("SubscriptionPlan",$data1);
            if($insertdata)
            {
              $this->session->set_flashdata('success', 'Subscription successfully Added.');
              redirect(base_url().'menu/subscription');
            }
        }  
    }

     $this->load->view('admin/subscription/subscription',$data);
  }



  public function remove_product_image($image_id =false)
  {
  	  $delete = $this->common_model->deleteData('ProductImages',array('id'=>$image_id));

  	  if($delete)
  	  {
  	  	 echo '1000';exit;
  	  }	
  }

}