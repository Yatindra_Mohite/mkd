<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Version extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		if(!$admin_id = $this->session->userdata('admin_id')){
      redirect(base_url('admin'));
    }

		date_default_timezone_set('Asia/Kolkata');
		$militime =round(microtime(true) * 1000);
		$datetime =date('Y-m-d h:i:s');
		define('militime', $militime);
		define('datetime', $datetime);
	
	}

	public function customer()
	{ 
		 $data['update_version'] = $this->common_model->common_getRow('app_version',array('version_id'=>1));

     $this->load->view('admin/version/customer_version',$data);	
  }

  public function barber()
  { 
     $data['update_version'] = $this->common_model->common_getRow('app_version',array('version_id'=>2));

     $this->load->view('admin/version/barber_version',$data);  

  }

  public function update_c()
  {
      if(isset($_POST['submit1']))
      {
              $min_android_version = $this->input->post('min_android_version');
              $max_android_version = $this->input->post('max_android_version');
              
             $version_update = $this->common_model->updateData('app_version',array('android_min_version'=> $min_android_version,'android_max_version'=>$max_android_version),array('version_id'=>'1'));

          if($version_update)
          {
              /*if($this->input->post('check_value') == 1)
              {
                    $student_data = $this->common_model->getDataField('device_token,student_id','students',array());

                    $gcmRegIds = array();
                    $i = 0;
                    foreach($student_data as $user_device_token)
                    {
                        $i++;
                        $gcmRegIds[floor($i/1000)][] = $user_device_token->device_token;

                        $userid_arr[] = $user_device_token->student_id;
                    }
                        $title = 'App Update';
                        $update_message = 'A new version of <b>ATMC</b> is now on Google Play Store. Update soon to get the latest features and bug fixes.';

                        $pushMessage=array("title" =>$title,'message_id'=>$max_android_version,"message" =>$update_message,'image'=>'',"type"=>'2',"currenttime"=>militime);
                       
                      if(isset($gcmRegIds)) 
                      {  
                          $message = $pushMessage;
                          $pushStatus = array();
                      
                          foreach($gcmRegIds as $val){ $pushStatus[] = $this->common_model->sendNotification($val, $message);
                          }
                      }
              }  */
                  $this->session->set_flashdata('success1',"Version updated Successfully");

                  redirect('version/customer');
          } 
          else
          {
            $this->session->set_flashdata('failed',"Version is Not Updated");

            redirect(base_url().'version/customer');
          } 
            
      }
      else if(isset($_POST['submit2']))
      {
          $min_ios_version = $this->input->post('min_ios_version');
          $max_ios_version = $this->input->post('max_ios_version');
              
          $version_update = $this->common_model->updateData('app_version',array('ios_min_version'=> $min_ios_version,'ios_max_version'=>$max_ios_version),array('version_id'=>'1'));

          if($version_update)
          {
            $this->session->set_flashdata('success2',"Version updated Successfully");

            redirect(base_url().'version/customer');
          } 
          else
          {
            $this->session->set_flashdata('failed',"Version is Not Updated");

            redirect(base_url().'version/customer');
          }   
      
      }  
         redirect(base_url().'version/customer');
  }


  public function update_b()
  {
      if(isset($_POST['submit1']))
      {
              $min_android_version = $this->input->post('min_android_version');
              $max_android_version = $this->input->post('max_android_version');
              
             $version_update = $this->common_model->updateData('app_version',array('android_min_version'=> $min_android_version,'android_max_version'=>$max_android_version),array('version_id'=>'2'));

          if($version_update)
          {
              $this->session->set_flashdata('success1',"Version updated Successfully");

              redirect(base_url().'version/barber');
          } 
          else
          {
            $this->session->set_flashdata('failed',"Version is Not Updated");

            redirect(base_url().'version/barber');
          } 
            
      }
      else if(isset($_POST['submit2']))
      {
          $min_ios_version = $this->input->post('min_ios_version');
          $max_ios_version = $this->input->post('max_ios_version');
              
          $version_update = $this->common_model->updateData('app_version',array('ios_min_version'=> $min_ios_version,'ios_max_version'=>$max_ios_version),array('version_id'=>'2'));

          if($version_update)
          {
            $this->session->set_flashdata('success2',"Version updated Successfully");

            redirect(base_url().'version/barber');
          } 
          else
          {
            $this->session->set_flashdata('failed',"Version is Not Updated");

            redirect(base_url().'version/barber');
          }   
      
      }  
         redirect(base_url().'version/barber');
  }


 } 
