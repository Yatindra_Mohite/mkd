<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	class Common_model extends CI_Model {
	public function __construct()
    {
        parent::__construct();
        define('secret_key','sk_test_7f2wMJcAaiY99o8y2Sf7S2pI');
	}

	public function cancel_count()
	{
		$query = $this->db->query("SELECT COUNT(*) as cancel_count FROM user_orders WHERE order_status IN(2,3,6)")->result();

		return $query[0]->cancel_count;
	}
	
	public function common_insert($tbl_name = false, $data_array = false)
	{
		$ins_data = $this->db->insert($tbl_name, $data_array);
		if($ins_data){
			return $last_id = $this->db->insert_id();
		}
		else{
			return false;
		}
	}

	public function updateData($table,$data,$where_array)
	{ 
	    $this->db->where($where_array);
		if($this->db->update($table,$data)){
			//print_r($this->db->last_query()); exit;
			return true;
		}
		else{
			//print_r($this->db->last_query()); exit;
			return false;
		}
	}

	// Function for select data
	public function getData($table,$where='', $order_by = false, $order = false, $join_array = false, $limit = false)
	{
		//$this->db->select('*');
		$this->db->from($table);

		if(!empty($where))
		{
			$this->db->where($where);
		}
		
		if(!empty($order_by))
		{
			$this->db->order_by($order_by, $order); 	
		}



		if(!empty($join_array))
		{
			foreach ($join_array as $key => $value) {

				$this->db->join($key, $value); 	
			}
			
		}

		if(!empty($limit))
		{
			$this->db->limit($limit); 	
		}

		$result = $this->db->get();
		

		//print_r($this->db->last_query()); exit;
		return $result->result();
		//return $result;
	}

	// Function for select data
	public function getDataField($field = false, $table, $where='', $order_by = false, $order = false, $join_array = false, $limit = false, $join_type = false )
	{
		$this->db->select($field);

		$this->db->from($table);

		if(!empty($where))
		{
			$this->db->where($where);
		}
		
		if(!empty($order_by))
		{
			$this->db->order_by($order_by, $order); 	
		}



		if(!empty($join_array))
		{
			foreach ($join_array as $key => $value) {

				if(!empty($join_type))
					$this->db->join($key, $value, 'left');
				else
					$this->db->join($key, $value); 	
			}
			
		}

		if(!empty($limit))
		{
			$this->db->limit($limit); 	
		}

		$result = $this->db->get();
		

		//print_r($this->db->last_query()); exit;
		return $result->result();
		//return $result;
	}

	public function common_getRow($tbl_name = flase, $where = false, $join_array = false)
	{
		$this->db->select('*');
		$this->db->from($tbl_name);
		
		if(isset($where) && !empty($where))
		{
			$this->db->where($where);	
		}
		
		if(!empty($join_array))
		{
			foreach($join_array as $key=>$value){
				$this->db->join($key,$value);
			}	
		}
		
		$query = $this->db->get();
		
		$data_array = $query->row();
		//print_r($this->db->last_query()); exit;
		if($data_array)
		{
			return $data_array;
		}
		else{
			return false;
		}
	}
	public function deleteData($table,$where)
	{ 
		$this->db->where($where);
		if($this->db->delete($table))
		{
			return true;
		}
		else{
			return false;
		}
	}
	
	public function sqlcount($table = false,$where = false)
	{
		$this->db->select('*');	
		$this->db->from($table); 
		if(isset($where) && !empty($where))
		{
			$this->db->where($where);	
		}
		//$this->db->limit($limit, $start);       
		$query = $this->db->get();
		//print_r($this->db->last_query()); exit;
		return $query->num_rows(); 
	}
	
	
	public function id_encrypt($str = false) // Id Encryption
	{
		return (56*$str);
	}
	
	public function id_decrypt($str = false) // Id Decryption
	{
		return ($str/56);
	}
	
	public function id_encrypt_zub($str = false) // Id Encryption
	{
		$str=(56*$str)+111;
		return ($str);
	}
	
	public function id_decrypt_zub($str = false) // Id Decryption
	{
		$str=($str-111)/56;
		return ($str);
	}
	public function milliseconds() // Id Decryption
	{
		$str = round(microtime(true) * 1000);
		return ($str);
	}
	public function getAddress($lat, $lon)
	{
		$url  = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$lon."&sensor=false";
		$json = @file_get_contents($url);
		$data = json_decode($json);
		$status = $data->status;
		$address = '';
		if($status == "OK")
		{
			$address = $data->results[0]->formatted_address;
		}
		return $address;
	}
	public function randomuniqueCode() 
	{
	    $alphabet = "123456789";
	    $pass = array(); /*remember to declare $pass as an array*/
	    $alphaLength = strlen($alphabet) - 1; /*put the length -1 in cache*/
	    for ($i = 0; $i < 6; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); /*turn the array into a string*/
	}

	function sendPushNotification($registration_ids, $message)
	{
	  
	   $url = 'https://fcm.googleapis.com/fcm/send';
	   $fields = array(
	       'registration_ids' => array($registration_ids),
	       'data' => $message,
	   );
	   $headers = array(
	       'Authorization:key=' . GOOGLE_API_KEY,
	       'Content-Type: application/json'
	   );

	   $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url); 
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	   // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	    $result = curl_exec($ch);
	    curl_close($ch);
	 
	   //return $result;
	  //echo $result; exit;
	}

	function sendNotification($registration_ids, $message)
	{
	  // echo '<pre>';	
	  // print_r($registration_ids);exit;
	   $url = 'https://fcm.googleapis.com/fcm/send';
	   $fields = array(
	       'registration_ids' => $registration_ids,
	       'data' => $message
	   );

	   // echo '<pre>';
	   // print_r($fields);exit;
	   $headers = array(
	       'Authorization:key=' . GOOGLE_API_KEY,
	       'Content-Type: application/json'
	   );

	   $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url); 
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	   // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	    $result = curl_exec($ch);
	    curl_close($ch);
	 
	   //return $result;
	  //$result; 
	}

	//send notification to barber
	function iOSPushNotification($deviceToken=false,$message=false,$msg=false,$type=false,$user_id=false,$barber_id=false)//type = 1 = normal and 2 = App
	{ 

		  $passphrase = "";
		  $payload['aps'] = array(
		    'alert' => array(
		                  "title"=>$msg,
		                  "body"=>$message
		                  ),
		    'badge' => 1,
		    'type' => $type,
		    'sound' => 'default'
		  );  

		  $payload = json_encode($payload);
		  $apnsHost = 'gateway.sandbox.push.apple.com';  //'gateway.sandbox.push.apple.com';  
		  $apnsPort = 2195;
		 
		  $apnsCert = 'pem/customer/kwik_dev.pem';
		  //echo $apnsCert; exit;
		  
		  $streamContext = stream_context_create();
		  
		  stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
		  stream_context_set_option($streamContext, 'ssl', 'passphrase', $passphrase);
		  //stream_context_set_option($streamContext, 'ssl', 'cafile', 'entrust_2048_ca.cer');
		  $apns = stream_socket_client('ssl://' . $apnsHost . ':' .
		  $apnsPort,$error,$errorString,60,STREAM_CLIENT_CONNECT,$streamContext);

		  //foreach($deviceToken as $token)
		 // {
		     $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', $deviceToken) . chr(0) . chr(strlen($payload)) . $payload;

		      $result = fwrite($apns, $apnsMessage);
		    
		 // }	

		 
		  @socket_close($apns);
		  fclose($apns);
	}


	//send notification to Customer
	function iOSPushNotification1($deviceToken,$message,$msg,$type,$user_id,$barber_id)//type = 1 = normal and 2 = App
	{ 

		  $passphrase = "";
		  $payload['aps'] = array(
		    'alert' => array(
		                  "title"=>$msg,
		                  "body"=>$message
		                  ),
		    'message_id'=>$message_id,
		    'badge' => 1,
		    'type' => $type,
		    'sound' => 'default'
		  );  

		  $payload = json_encode($payload);
		  $apnsHost = 'gateway.sandbox.push.apple.com';  //'gateway.sandbox.push.apple.com';  
		  $apnsPort = 2195;
		 
		  $apnsCert = 'pem/barber/kwik_barber_dev.pem';
		  //echo $apnsCert; exit;
		  
		  $streamContext = stream_context_create();
		  
		  stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
		  stream_context_set_option($streamContext, 'ssl', 'passphrase', $passphrase);
		  //stream_context_set_option($streamContext, 'ssl', 'cafile', 'entrust_2048_ca.cer');
		  $apns = stream_socket_client('ssl://' . $apnsHost . ':' .
		  $apnsPort,$error,$errorString,60,STREAM_CLIENT_CONNECT,$streamContext);

		  //foreach($deviceToken as $token)
		 // {
		     $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', $deviceToken) . chr(0) . chr(strlen($payload)) . $payload;

		      $result = fwrite($apns, $apnsMessage);
		    
		 // }	

		 
		  @socket_close($apns);
		  fclose($apns);
	}


	function encryptor_ym($action, $string) {
	    $output = false;

	    $encrypt_method = "AES-256-CBC";
	    //pls set your unique hashing key
	    $secret_key = 'muni';
	    $secret_iv = 'muni123';

	    // hash
	    $key = hash('sha256', $secret_key);

	    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);

	    //do the encyption given text/string/number
	    if( $action == 'encrypt' ) {
	        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	        $output = base64_encode($output);
	    }
	    else if( $action == 'decrypt' ){
	    	//decrypt the given text/string/number
	        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	    }

	    return $output;
	}

	

	public function get_cd_list($table,$aColumns,$sIndexColumn) {
        /* Array of table columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
     	$this->cds = $table;

        /* $aColumns = array(
            'zip_code_id',
            'name',
            'zip_code',
            'offie_type',
            'delivery_status');*/

        /* Indexed column (used for fast and accurate table cardinality) */
        //$sIndexColumn = "zip_code_id";

        /* Total data set length */
        $sQuery = "SELECT COUNT('" . $sIndexColumn . "') AS row_count
            FROM $this->cds";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->row();
        $iTotal = $aResultTotal->row_count;

        /*
         * Paging
         */
        $sLimit = "";
        $iDisplayStart = $this->input->get_post('start', true);
        $iDisplayLength = $this->input->get_post('length', true);
        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart) . ", " .
                    intval($iDisplayLength);
        }

        $uri_string = $_SERVER['QUERY_STRING'];
        $uri_string = preg_replace("/\%5B/", '[', $uri_string);
        $uri_string = preg_replace("/\%5D/", ']', $uri_string);

        $get_param_array = explode("&", $uri_string);
        $arr = array();
        foreach ($get_param_array as $value) {
            $v = $value;
            $explode = explode("=", $v);
            $arr[$explode[0]] = $explode[1];
        }

        $index_of_columns = strpos($uri_string, "columns", 1);
        $index_of_start = strpos($uri_string, "start");
        $uri_columns = substr($uri_string, 7, ($index_of_start - $index_of_columns - 1));
        $columns_array = explode("&", $uri_columns);
        $arr_columns = array();
        foreach ($columns_array as $value) {
            $v = $value;
            $explode = explode("=", $v);
            if (count($explode) == 2) {
                $arr_columns[$explode[0]] = $explode[1];
            } else {
                $arr_columns[$explode[0]] = '';
            }
        }

        /*
         * Ordering
         */
        $sOrder = "ORDER BY ";
        $sOrderIndex = $arr['order[0][column]'];
        $sOrderDir = $arr['order[0][dir]'];
        $bSortable_ = $arr_columns['columns[' . $sOrderIndex . '][orderable]'];
        if ($bSortable_ == "true") {
            $sOrder .= $aColumns[$sOrderIndex] .
                    ($sOrderDir === 'asc' ? ' asc' : ' desc');
        }

        /*
         * Filtering
         */
        $sWhere = "";
        $sSearchVal = $arr['search[value]'];
        if (isset($sSearchVal) && $sSearchVal != '') {
            $sWhere = "WHERE (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . " LIKE '%" . $this->db->escape_like_str($sSearchVal) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        /* Individual column filtering */
        $sSearchReg = $arr['search[regex]'];
        for ($i = 0; $i < count($aColumns); $i++) {
            $bSearchable_ = $arr['columns[' . $i . '][searchable]'];
            if (isset($bSearchable_) && $bSearchable_ == "true" && $sSearchReg != 'false') {
                $search_val = $arr['columns[' . $i . '][search][value]'];
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . $this->db->escape_like_str($search_val) . "%' ";
            }
        }


        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
        FROM $this->cds
        $sWhere
        $sOrder
        $sLimit
        ";
        $rResult = $this->db->query($sQuery);

        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() AS length_count";
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->row();
        $iFilteredTotal = $aResultFilterTotal->length_count;

        /*
         * Output
         */
        $sEcho = $this->input->get_post('draw', true);
        $output = array(
            "draw" => intval($sEcho),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array()
        );

        foreach ($rResult->result_array() as $aRow) {
            $row = array();
            foreach ($aColumns as $col) {
                $row[] = $aRow[$col];
            }
          //  $row['link']="ssasasasasasasa";
            $output['data'][] = $row;
        }

        return $output;
    }

    public function check_auth($admin_id)
    {
    	$query = $this->db->query("SELECT `status` FROM `user` WHERE `id` ='$admin_id'")->row();	
    	//return $query->status;
    	if($query->status == '1')
    	{
    		return '100';
    	}
    	else
    	{
    		return '1001'; 
    	}	
    }

    public function user_payment($stripetoken=false,$email=false,$amount=false)
	{
		// $headers = $_SERVER;
		// if(!isset($headers['HTTP_SECRET_KEY']))
		// {
		// 	$userdetail = $this->ChechAuth($headers['HTTP_SECRET_KEY']);
		// 	if($userdetail['status']=='true')
		// 	{
				require_once("system/libraries/stripe/init.php");
				$output = array(); $id='';
				$stripe = array(
					  "secret_key"  => secret_key,
					);

					\Stripe\Stripe::setApiKey($stripe['secret_key']);

					$token  = $stripetoken;

					// \Stripe\Refund::create(array(
					//  "charge" => "ch_1BxvP7HW7Uv9yyDqqagx8w8E",
					// ));

					try {
						$customer = \Stripe\Customer::create(array(
						'email' => $email,
						'source'  => $token
						));

						$charge = \Stripe\Charge::create(array(
							'customer' => $customer->id,
							'amount'   => (int)$amount*100,
							'currency' => "gbp"

						));
						
						// Payment has succeeded, no exceptions were thrown or otherwise caught				
						$error = "Successfully";
						$result = "success";
						$id = $charge->id;

					} catch(Stripe_CardError $e) {			
						$error = $e->getMessage();
						$result = "failed";
					} catch (Stripe_InvalidRequestError $e) {
						$error = $e->getMessage();
						$result = "failed";		  
					} catch (Stripe_AuthenticationError $e) {
						$error = $e->getMessage();
						$result = "failed";
					} catch (Stripe_ApiConnectionError $e) {
						$error = $e->getMessage();
						$result = "failed";
					} catch (Stripe_Error $e) {
						$error = $e->getMessage();
						$result = "failed";
					} catch (Exception $e) {
						$error = $e->getMessage();
						if ($e->getMessage() == "zip_check_invalid") {
							$result = "failed";
						} else if ($e->getMessage() == "address_check_invalid") {
							$result = "failed";
						} else if ($e->getMessage() == "cvc_check_invalid") {
							$result = "failed";
						} else {
							$result = "failed";
						}		  
					}

					$output['status'] = $result;
					$output['message'] = $error;
					$output['id'] = $id;

					return $output;
		// 	}else
		// 	{
		// 		$output['status'] = "failed";
		// 		$output['message'] = 'Somthing went wrong! please try again.';
		// 	}
		// }else
		// {
		// 	$output['status'] = "failed";
		// 	$output['message'] = 'Somthing went wrong! please try again.';
		// }
		//header("content-type: application/json");
	   ///	echo json_encode($output);
	}

	function refund_amount($transaction_id = false)
	{
		require_once("system/libraries/stripe/init.php");
		$output = array(); $id='';
		$stripe = array(
			  "secret_key"  => secret_key,
			);

			\Stripe\Stripe::setApiKey($stripe['secret_key']);

			//$token  = $_POST['refund_token'];

			try {
				$response = \Stripe\Refund::create(array(
				 "charge" => $transaction_id,
				));

				$status = "success";
				$msg = "Successfully";
				$id = $response->id;
			} catch (Exception $e) {
					$error = $e->getMessage();
				$status = "failed";
				$msg = $error;
				$id = '';
			}
			$output['status'] = $status;
			$output['message'] = $msg;
			$output['id'] = $id;
			return $output;
	}



}

?>