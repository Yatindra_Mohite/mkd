<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Subscription</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
            <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                           
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Product</div>
                                    <div class="actions">
                                       <a title="Add Barber" href="<?php echo base_url('product/add_product');?>" type="button" id="" class="btn white pull-right">Add Product<i class="fa fa-plus" aria-hidden="true"></i></a> 
                                    </div>    
                                </div>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class=""><a href="#mon" data-toggle="tab">Mon</a></li>
                                <li class=""><a href="#tue" data-toggle="tab">Tue</a></li>
                                <li class=""><a href="#wed" data-toggle="tab">Wed</a></li>
                                <li class=""><a href="#thu" data-toggle="tab">Thus</a></li>
                                <li class=""><a href="#fri" data-toggle="tab">Fri</a></li>
                                <li class=""><a href="#sat" data-toggle="tab">Sat</a></li>
                                <li class=""><a href="#sun" data-toggle="tab">Sun</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
       
      <?php $this->load->view("admin/footer"); ?>
    </body>
</html>








     


