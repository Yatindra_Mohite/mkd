<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Peerbucket</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
</head>

<body style="margin:0;">

    <table cellspacing="0" cellpadding="0" width="600" align="center" style="font-family: 'Montserrat', sans-serif;">
        <tbody>
            <tr>
                <td align="center" style="padding:15px 0;background:#f7f7f7;">
                    <img src="https://i.imgur.com/si4YSmp.png" width="150px" alt="">
                    <span style="display:  block;margin-top:  20px;color:  #501426;font-weight:600;font-size:  24px;">Welcome to
                        <span style="color: #f26c4f;"> Foodtemple</span>
                    </span>
                </td>
            </tr>
            <tr>
                <td style="color:#333;line-height:28px;font-size:14px;padding:10px;text-align: center;">
                    <span style="display:block;margin-bottom:20px;font-size:20px;color: #f26c4f;margin-top:  20px;font-weight:  600;"> </span>
                  <!--   <span style="display:block;margin-bottom:20px;"> Thanks for signing at FoodTemple </span> -->
                  <span style="color: #f26c4f;font-size: 25px;font-weight: bold;text-align: center;margin-top: 7px;display:  block;margin-bottom:  10px;">CONGRATULATION</span>

                  <span style="font-size: 18px;font-weight: 600;color: #4f1526;margin-top: 7px;display:  block;text-align:  center;margin: 15px 0;">Your Email address has been verified successfully</span>
                    
                   <!--  <span style="display:block;margin-bottom:20px;">
                        <a href="#" title="Reset Password" style="text-decoration:  none;background: #f26c4f;color:  #fff;border-radius:  30px;
                        display: block;padding: 10px 40px;width: 120px;text-align:  center;margin:  auto;">Confirm</a>
                    </span> -->

                    <span style="display:block;margin-bottom:20px;"> Questions? Concerns? Suggestions? Contact support and a real person will get back to you in minutes:
                        <a href="mailto:support@Foodtemple.co" title="support@Foodtemple.co"> support@Foodtemple.co</a>
                    </span>
                </td>
            </tr>
            <tr>
                <td style="background:#501426;padding:15px 0;" align="center">
                    <a href="#" title="Privacy Policy" target="_blank" style="text-decoration:none;color:#fff;font-size:13px;margin:0 5px;">Privacy Policy</a>

                    <a href="#" title="Terms of Service" target="_blank" style="text-decoration:none;color:#fff;font-size:13px;margin:0 5px;">Terms of Service</a>

                    <a href="#" title="FAQ" target="_blank" style="text-decoration:none;color:#fff;font-size:13px;margin:0 5px;">FAQ</a>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>