<header>
        <div class="container">
            <div class="row">
                <div class="col m12 s12">
                    <div class="logo">
                        <a href="<?php echo base_url();?>" title="Home">
                            <img src="<?php echo base_url();?>theme/images/logo.png" alt="">
                        </a>
                    </div>
                    <nav class="nav right navigation">
                        <div class="nav-mobile">
                            <img src="<?php echo base_url();?>theme/images/menu-bar.png" alt="">
                        </div>
                        <ul>
                            <li>
                                <a href="<?php echo base_url();?>#welcome">About us</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>#services">Services</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>#gallery">Gallery</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>#testimonials">Testimonials</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>#how-we">How we work</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>#contactus">Contact us</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('join-us');?>" class="joinus-link waves-effect waves-light">Join Us</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('appointment');?>" class="joinus-link waves-effect waves-light">Book now</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>