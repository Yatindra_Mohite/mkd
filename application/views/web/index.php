<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <title>Kwikuts</title>
    <?php $this->load->view('web/include/head');?>
     <link href="<?php echo base_url();?>template/assets/global/css/parsley.css" rel="stylesheet">  
</head>
<body>
    <?php $this->load->view('web/include/header');?>
    <section class="banner">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="banner-text">
                        <h1 class="heading">Find your Barber or Stylist by using your phone</h1>
                        <h3 class="subhead">Book your next appoinment</h3>
                        <div class="download-btn">
                            <a href="#" title="App Store" class="waves-effect waves-light">
                                <i class="fa fa-apple"></i> App Store</a>

                            <a href="#" title="Play Store" class="waves-effect waves-light">
                                <i class="fa fa-play"></i> Play Store</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <span class="scroll-down">
            <a href="#welcome">
                <span></span>
            </a>
        </span>
    </section>

    <section class="welcome" id="welcome">
        <div class="container">
            <div class="row">
                <div class="col m4 s12">
                    <div class="welcome-image">
                        <img src="<?php echo base_url();?>theme/images/welcome.png" alt="">

                        <div class="app-slides">
                            <div class="owl-carousel owl-theme">

                                <div class="item">
                                    <img src="<?php echo base_url();?>theme/images/screen1.png" alt="">
                                </div>

                                <div class="item">
                                    <img src="<?php echo base_url();?>theme/images/screen2.png" alt="">
                                </div>

                                <div class="item">
                                    <img src="<?php echo base_url();?>theme/images/screen3.png" alt="">
                                </div>

                                <div class="item">
                                    <img src="<?php echo base_url();?>theme/images/screen4.png" alt="">
                                </div>

                                <div class="item">
                                    <img src="<?php echo base_url();?>theme/images/screen5.png" alt="">
                                </div>

                                <div class="item">
                                    <img src="<?php echo base_url();?>theme/images/screen6.png" alt="">
                                </div>

                                <div class="item">
                                    <img src="<?php echo base_url();?>theme/images/screen7.png" alt="">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col m1 s12"></div>

                <div class="col m7 s12">
                    <div class="welcome-text">
                        <h1> Welcome to Kwikuts</h1>
                        <p>“Kwikuts” is the UK’s first on demand barber app delivering barbers to your doorstep! Clients request
                            a barber through the smartphone app, website or over the phone. Once confirmed clients can track
                            barber location and ETA online or through the app. Kwikuts operates 7 days a week from 8 AM to
                            9 PM across London and neighbouring cities.</p>
                        <!-- <a href="#" title="Read more" class="btn theme-btn waves-effect waves-light">Read more</a> -->

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="our-services" id="services">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-heading">
                        <h1>Our Services</h1>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col m2 s12"></div>

                <div class="col m8 s12">
                    <div class="row">

                        <div class="col m4 s12">
                            <div class="box-border">
                                <div class="service-box box1">
                                    <span class="image image1"></span>
                                    <h4 class="name">Haircut</h4>
                                    <p class="price">price:</p>
                                    <h4 class="from">from
                                        <span>£25</span>
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="col m4 s12">
                            <div class="box-border">
                                <div class="service-box box2">
                                    <span class="image image2"></span>
                                    <h4 class="name">Beard Trim</h4>
                                    <p class="price">price:</p>
                                    <h4 class="from">from
                                        <span>£20</span>
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <div class="col m4 s12">
                            <div class="box-border">
                                <div class="service-box box3">
                                    <span class="image image3"></span>
                                    <h4 class="name">Haircut & Beard Trim</h4>
                                    <p class="price">price:</p>
                                    <h4 class="from">from
                                        <span>£30</span>
                                    </h4>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col m2 s12"></div>
            </div>
        </div>
    </section>


    <section class="our-gallery" id="gallery">
        <div class="container">
            <div class="row">
                <div class="col m12 s12">
                    <div class="page-heading">
                        <h1>Our Gallery</h1>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>

            <div class="row gallery">

                <div class="col m1 s12"></div>

                <div class="col m10 s12">
                    <div class="row">
                        <div class="col m3 s12">
                            <div class="gallery-image">
                                <a href="<?php echo base_url();?>theme/images/hair1.png">
                                    <img src="<?php echo base_url();?>theme/images/hair1.png" alt="" title="" />
                                </a>
                            </div>
                        </div>

                        <div class="col m3 s12">
                            <div class="gallery-image">
                                <a href="<?php echo base_url();?>theme/images/hair2.png">
                                    <img src="<?php echo base_url();?>theme/images/hair2.png" alt="" title="" />
                                </a>
                            </div>
                        </div>

                        <div class="col m3 s12">
                            <div class="gallery-image">
                                <a href="<?php echo base_url();?>theme/images/hair3.png">
                                    <img src="<?php echo base_url();?>theme/images/hair3.png" alt="" title="" />
                                </a>
                            </div>
                        </div>

                        <div class="col m3 s12">
                            <div class="gallery-image">
                                <a href="<?php echo base_url();?>theme/images/hair4.png">
                                    <img src="<?php echo base_url();?>theme/images/hair4.png" alt="" title="" />
                                </a>
                            </div>
                        </div>

                        <div class="col m3 s12">
                            <div class="gallery-image">
                                <a href="<?php echo base_url();?>theme/images/hair5.png">
                                    <img src="<?php echo base_url();?>theme/images/hair5.png" alt="" title="" />
                                </a>
                            </div>
                        </div>

                        <div class="col m3 s12">
                            <div class="gallery-image">
                                <a href="<?php echo base_url();?>theme/images/hair6.png">
                                    <img src="<?php echo base_url();?>theme/images/hair6.png" alt="" title="" />
                                </a>
                            </div>
                        </div>

                        <div class="col m3 s12">
                            <div class="gallery-image">
                                <a href="<?php echo base_url();?>theme/images/hair7.png">
                                    <img src="<?php echo base_url();?>theme/images/hair7.png" alt="" title="" />
                                </a>
                            </div>
                        </div>

                        <div class="col m3 s12">
                            <div class="gallery-image">
                                <a href="<?php echo base_url();?>theme/images/hair8.png">
                                    <img src="<?php echo base_url();?>theme/images/hair8.png" alt="" title="" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col m1 s12"></div>

            </div>
        </div>
    </section>

    <section class="testimonials" id="testimonials">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-heading">
                        <h1>Testimonials</h1>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>

                <div class="col-sm-12">
                    <span class="quote">
                        <img src="<?php echo base_url();?>theme/images/quote.png" alt="">
                    </span>
                    <div class=" owl-carousel ">

                        <!--item-->
                        <div class="item ">
                            <div class="test-text">
                                <h4>I cannot even express how awed Peter and I are with your work, Shawn. Words seem inadequate.
                                    Your presentation was exciting as you bought our ideas to life. I am deeply grateful
                                    you took on this job, I knew you could save our initial concepts, but I had no idea how
                                    you would expand the them and take them to a whole new level of creativity. You are a
                                    very talented artist – God bless!</h4>
                                <span class="user-image">
                                    <img src="<?php echo base_url();?>theme/images/user1.png " alt=" ">
                                </span>
                                <h3 class="name ">Jacob Godbout </h3>
                                <p class="date ">Dec 15,2017</p>
                            </div>
                        </div>
                        <!--end item-->

                        <!--item-->
                        <div class="item ">
                            <div class="test-text">
                                <h4>I cannot even express how awed Peter and I are with your work, Shawn. Words seem inadequate.
                                    Your presentation was exciting as you bought our ideas to life. I am deeply grateful
                                    you took on this job, I knew you could save our initial concepts, but I had no idea how
                                    you would expand the them and take them to a whole new level of creativity. You are a
                                    very talented artist – God bless!</h4>
                                <span class="user-image">
                                    <img src="<?php echo base_url();?>theme/images/user1.png " alt=" ">
                                </span>
                                <h3 class="name ">Jacob Godbout </h3>
                                <p class="date ">Dec 15,2017</p>
                            </div>
                        </div>
                        <!--end item-->

                        <!--item-->
                        <div class="item ">
                            <div class="test-text">
                                <h4>I cannot even express how awed Peter and I are with your work, Shawn. Words seem inadequate.
                                    Your presentation was exciting as you bought our ideas to life. I am deeply grateful
                                    you took on this job, I knew you could save our initial concepts, but I had no idea how
                                    you would expand the them and take them to a whole new level of creativity. You are a
                                    very talented artist – God bless!</h4>
                                <span class="user-image">
                                    <img src="<?php echo base_url();?>theme/images/user1.png " alt=" ">
                                </span>
                                <h3 class="name ">Jacob Godbout </h3>
                                <p class="date ">Dec 15,2017</p>
                            </div>
                        </div>
                        <!--end item-->

                        <!--item-->
                        <div class="item ">
                            <div class="test-text">
                                <h4>I cannot even express how awed Peter and I are with your work, Shawn. Words seem inadequate.
                                    Your presentation was exciting as you bought our ideas to life. I am deeply grateful
                                    you took on this job, I knew you could save our initial concepts, but I had no idea how
                                    you would expand the them and take them to a whole new level of creativity. You are a
                                    very talented artist – God bless!</h4>
                                <span class="user-image">
                                    <img src="<?php echo base_url();?>theme/images/user1.png " alt=" ">
                                </span>
                                <h3 class="name ">Jacob Godbout </h3>
                                <p class="date ">Dec 15,2017</p>
                            </div>
                        </div>
                        <!--end item-->

                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="how-it-work" id="how-we">
        <div class="container">
            <div class="row">
                <div class="col m12 s12">
                    <div class="page-heading">
                        <h1>How we work</h1>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col m4 s12">
                    <div class="how-box how-right">
                        <img src="<?php echo base_url();?>theme/images/mobile.png" alt="">
                        <p>Browse through the listed barbers and book your favorite barber within your location</p>
                    </div>
                </div>

                <div class="col m4 s12">
                    <div class="how-box how-right">
                        <img src="<?php echo base_url();?>theme/images/go.png" alt="">
                        <p>Get immediate confirmation from the barber. Track his location as he travels to you for the appointment.
                        </p>
                    </div>
                </div>

                <div class="col m4 s12">
                    <div class="how-box">
                        <img src="<?php echo base_url();?>theme/images/hair-cut-tool.png" alt="">
                        <p>It's time to enjoy the hair-cut! Pay directly through the app for your appointment.</p>
                    </div>
                </div>

            </div>
        </div>
    </section>


    <section class="video">
        <a href="https://www.youtube.com/watch?v=CuH3tJPiP-U" title="" class="bla-1 waves-effect waves-light">
            <i class="fa fa-play"></i>
        </a>
    </section>

    <section class="row contact-us" id="contactus">
        <div class="col m6 s12 padd-none">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d317715.71193282085!2d-0.38178515365288046!3d51.528735196636745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C+UK!5e0!3m2!1sen!2sin!4v1515389011424"
                width="100%" height="660" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="col m6 s12 padd-none">
            <div class="contact-form">
                <div class="page-heading">
                    <h1>Contact us</h1>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>

                <div class="info">
                    <ul>
                        <li>
                            <i class="fa fa-map-marker"></i>
                            <span>123, Street name, main road, Londonwide</span>
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i>
                            <span>admin@kwikuts.co.uk </span>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>
                            <span>0203 488 0879</span>
                        </li>
                    </ul>
                </div>

                <?php 
                if ($this->session->flashdata('success')) { 
                echo "<div class='alert alert-success' style='margin-left:21%;color:sienna;'>", $this->session->flashdata('success') ,"</div>";
                }else if($this->session->flashdata('failed')){
                echo "<div class='alert alert-danger' style='margin-left:21%;color:tomato;'>", $this->session->flashdata('failed') ,"</div>";
                } 
               ?>
                <div class="form">
                    <form action="<?php echo base_url('home/contactus');?>"  method="post" data-parsley-validate='' id="form11">
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="fa fa-user"></i>
                                <input id="name" name="name" type="text" class="validate" required>
                                <label for="name">Name</label>
                            </div>

                            <div class="input-field col s6">
                                <i class="fa fa-envelope"></i>
                                <input id="email" name="email" type="text" class="validate" data-parsley-type="email" required>
                                <label for="email">Email</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <textarea id="textarea" class="materialize-textarea" name="message" required></textarea>
                                <label for="textarea">Your Message</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn theme-btn waves-effect waves-light" type="submit" name="submit">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
   <?php $this->load->view('web/include/footer');?>
   <script src="<?php echo base_url()?>template/assets/global/js/parsley.min.js"></script>
    <script>
        $('.nav,.scroll-down').navScroll({
            mobileDropdown: true,
            mobileBreakpoint: 768,
            scrollSpy: true,
            onScrollStart: function () {
                // $scrollStatus.show();
                // $scrollStatus.text('Started scrolling');
            },
            onScrollEnd: function () {
                // $scrollStatus.text('Scrolling ended');
                setTimeout(function () {
                    // $scrollStatus.fadeOut(200);
                }, 1000);
            }
        });
    </script>

<script type="text/javascript">
  $('#form11').parsley();  
</script>

</body>

</html>