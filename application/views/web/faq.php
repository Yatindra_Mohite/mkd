<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <title>Kwikuts</title>

<?php $this->load->view('web/include/head');?>
</head>

<body>

   <?php $this->load->view('web/include/header');?>

    <section class="page-banner">
        <div class="container">
            <div class="row">
                <div class="col m12 s12">
                    <h1>FAQ</h1>
                </div>
            </div>
        </div>
    </section>

    <section class="page-text faq-section">
        <div class="container">
            <div class="row">
                <div class="col m12 s12">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li>
                            <div class="collapsible-header">Refund Policy:</div>
                            <div class="collapsible-body">
                                <p>Refunds will be issued on a case by case review, we have a 7 days policy for all refund requests
                                    to be submitted. Immediate</p>
                                <p> will be given under these circumstances:</p>
                                <ul class="collapse-ul">
                                    <li>The barber has been unprofessional when providing his/her service.</li>

                                    <li>If the barber is unable to make an appointment and Kwikuts is unable to provide an alternative
                                        barber. </li>
                                    <li> If an appointment is cancelled by the customer within 10 minutes, a full refund will
                                        be made. Any cancellations later than 10 minutes will incur a 50% cancellation fee.</li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header">
                                Lateness Policy:</div>
                            <div class="collapsible-body">
                                <ul class="collapse-ul left-padd-none">
                                    <li>If a barber has arrived on time to his/her appointment, however is waiting longer than
                                        10 minutes for response from the patient. The barber will hold the right to cancel
                                        their appointment and the customer will incur a 25% surcharge.</li>
                                    <li>In the event the barber is running late due to unforeseen circumstances, someone from
                                        the customer service team will update you of their ETA.</li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header">
                                My Barber was unprofessional:</div>
                            <div class="collapsible-body">
                                <ul class="collapse-ul left-padd-none">
                                    <li>If you find your barber has acted in an inappropriate manner or has not delivered the
                                        service as expected, you have the rights to raise a complaint with us by contacting
                                        the customer service team. Any serious allegations will result in the suspension
                                        of the barber until further notice. All complaints will follow our complaints procedure.</li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <div class="collapsible-header">
                                How can I pay:</div>
                            <div class="collapsible-body">
                                <p>We take the following methods of payments which are all available via our app:</p>
                                <ul class="collapse-ul">
                                    <li>Card payments (VISA, MASTERCARD, AMERICAN EXPRESS)</li>
                                    <li>Apple pay</li>
                                    <li>Android Pay</li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <div class="collapsible-header">
                                Promo Codes &amp; Credits:</div>
                            <div class="collapsible-body">
                                <p>Kwikuts will have special offers on for their valued customers, which will allow customers
                                    promotional codes to enter into the app.
                                    <br> Some examples are:</p>
                                <ul class="collapse-ul">
                                    <li>First Time Users</li>
                                    <li>Refer a friend</li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <div class="collapsible-header">
                                I have an unknown charge: </div>
                            <div class="collapsible-body">
                                <p>Any unknown charges should be raised to the customer service team, we will then investigate
                                    the charges and raise any technical glitches with our IT department. Any unexpected charges
                                    will be fully refunded.</p>
                            </div>
                        </li>

                        <li>
                            <div class="collapsible-header">
                                How long are the appointments?</div>
                            <div class="collapsible-body">
                                <p>All appointments are 30 minutes in length, if additional time is required, every 10 minutes
                                    will incur a £5.00 charge</p>
                            </div>
                        </li>

                        <li>
                            <div class="collapsible-header">
                                When and where can I request a visit?</div>
                            <div class="collapsible-body">
                                <p>We currently operate in London, covering all areas within the M25. However, if you do have
                                    a visit outside of our coverage zone, we may be able to deliver a barber to you. Please
                                    call the customer service team to enquire further.</p>
                                <p>We are open:</p>
                                <ul class="collapse-ul">
                                    <li>Monday- Sunday, 7 days a week</li>
                                    <li>8AM-9PM</li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
   <?php $this->load->view('web/include/footer');?>
</body>

</html>