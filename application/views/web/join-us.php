<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <title>Kwikuts</title>

    <link href="<?php echo base_url();?>theme/css/materialize.css" rel="stylesheet">
    <link href="<?php echo base_url();?>theme/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/css/simplelightbox.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/css/owl.carousel.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<?php echo base_url();?>template/assets/global/css/parsley.css" rel="stylesheet">  

    <link rel="icon" href="<?php echo base_url();?>theme/images/favicon.png">
</head>

<body class="form-page">
    <section class="joinus-page">
        <a href="javascript:window.history.go(-1);" class="back-home">
            <i class="fa fa-arrow-left"></i>
        </a>
        <div class="joinus-form barber-form">
            <h3 class="title">Register as a Barber</h3>
            <?php 
            if ($this->session->flashdata('success')) { 
            echo "<div class='alert alert-success' style='margin-left:38%;'>", $this->session->flashdata('success') ,"</div>";
            }else if($this->session->flashdata('failed')){
            echo "<div class='alert alert-danger' style='margin-left:38%;'>", $this->session->flashdata('failed') ,"</div>";
            } 
          ?>
            <form action="<?php echo base_url('home/join_barber');?>" method="post" data-parsley-validate='' id="form11">
                <div class="join-form">
                    <div class="input-field">
                        <i class="fa fa-user"></i>
                        <input type="text" name="fname" data-parsley-error-message="First Name is required" class="validate" required>
                        <label for="first_name">First Name</label>
                    </div>
                    <div class="input-field">
                        <i class="fa fa-envelope-o"></i>
                        <input type="text" name="email" id="email" data-parsley-type="email" data-parsley-error-message="Email is required" class="validate" required>
                        <label for="first_name">Email</label>
                        <div id="err" style="list-style-type: none;font-size: 0.9em; line-height: 1.9em;"></div>
                    </div>
                    <div class="input-field">
                        <i class="fa fa-phone"></i>
                        <input type="text" name="contact_no" id="contact_no" data-parsley-type="digits" data-parsley-error-message="Contact No. is required" class="validate" required>
                        <label for="first_name">Contact</label>
                        <div id="err1" style="list-style-type: none;font-size: 0.9em; line-height: 1.9em;"></div>
                    </div>
                    <div class="input-field">
                        <i class="fa fa-map-marker"></i>
                        <input type="text" name="address" data-parsley-error-message="Address is required" class="validate" required>
                        <label for="first_name">Address</label>
                    </div>
                    <button id="submit" type="submit" name="submit" class="btn waves-effect waves-light">Submit</button>
                </div>
            </form>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo base_url()?>template/assets/global/js/parsley.min.js"></script>
    <script src="<?php echo base_url();?>theme/js/materialize.min.js"></script>
    <script src="<?php echo base_url();?>theme/js/simple-lightbox.min.js"></script>
    <script src="<?php echo base_url();?>theme/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>theme/js/youtubepopup.js"></script>
    <script src="<?php echo base_url();?>theme/js/jquery.navScroll.min.js"></script>
    <script src="<?php echo base_url();?>theme/js/custom.js"></script>

<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script>
$(document).ready(function(){
    $("#submit").click(function(){ 
        var email  = $("#email").val();
        var contact_no  = $("#contact_no").val();
        var str = 0;
             $.ajax({
                    type:'POST',
                    url: '<?php echo base_url();?>home/check_email',
                    data: "email="+email,
                    async: false,
                    success: function(data){
                        if(data==10000)
                        {
                          str = 1;  
                          $("#err").html('Email already exists').css('color','red');
                        }
                        else if(data == 1001)
                        {
                           str = 2;  
                          $("#err1").html('Contact no. already exists').css('color','red');
                        }    
                        else
                        { 
                          $("#err").html('');
                          $("#err1").html('');
                        }
                    },
        
                });

              $.ajax({
                    type:'POST',
                    url: '<?php echo base_url();?>home/check_contact',
                    data: "contact_no="+contact_no,
                    async: false,
                    success: function(data){
                        if(data == 1001)
                        {
                           str = 2;  
                           $("#err1").html('Contact no. already exists').css('color','red');
                        }    
                        else
                        { 
                           $("#err1").html('');
                        }
                    },
        
                });

            if(str == 1 || str == 2)
            {
                return false;
            }
        });
});

</script> 

</body>

</html>