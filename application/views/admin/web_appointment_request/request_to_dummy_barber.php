<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Request to Dummy Babrer </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container"> 
             <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Request to Dummy Barber  
                                    </div>
                                    <div class="actions">
                                    </div>    
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>User Name</center></th>
                                                <th><center>Booking Datetime</center></th>
                                                <th><center>Assign</center></th>
                                                <th><center>Assigned Barber</center></th>
                                                <th><center>Status</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>User Name</center></th>
                                                <th><center>Booking Datetime</center></th>
                                                <th><center>Assign</center></th>
                                                <th><center>Assigned Barber</center></th>
                                                <th><center>Status</center></th>
                                            </tr>
                                        </tfoot>
                                         <tbody>
                                         <?php 
                                         if(!empty($Appointment_request))
                                         {   $i = 0;
                                            foreach($Appointment_request as $key)
                                            { $i++; ?>
                                             <tr id="xxx">
                                                <td><center><?php echo $i;?></center></td>   
                                                <td><center><?php
                                                  $username = $this->common_model->common_getRow('barber_user',array('user_id'=>$key->user_id,'user_type'=>1));
                                                  echo $username->user_name;?></center>
                                                </td>
                                                <td><center><?php echo $key->booking_date;?></center></td>
                                                <td><center><a class="btn green btn-outline sbold aaa" data-toggle="modal" href="<?php echo base_url('Appointment_request/available_barber/'.$key->booking_id.'/d');?>">Assign barber</a></center></td>  
                                                <td><center><?php 

                                                if($key->dummy_barber_id != 0 && $key->barber_id == 0)
                                                {
                                                	$barbername = $this->common_model->common_getRow('barber_dummy',array('barber_id'=>$key->dummy_barber_id));

                                                	echo $barbername->user_name.' - Dummy';
                                                }
                                                else if($key->dummy_barber_id != 0 && $key->barber_id != 0)
                                                {
                                                	$barbername = $this->common_model->common_getRow('barber_user',array('user_id'=>$key->barber_id,'user_type'=>2));

                                                	echo $barbername->user_name;
                                                }	
                                               ?></center></td> 
                                                <td><center><?php if($key->booking_status == 0){ echo '<a href="javascript:;" class="btn btn-xs purple">Pending</a>';} else if($key->booking_status == 1){ echo '<a href="javascript:;" class="btn btn-xs yellow">Confirmed</a>
';} else if($key->booking_status == 2) { echo '<a href="javascript:;" class="btn btn-xs green">Completed</a>';} else if($key->booking_status == 3) { echo '<a href="javascript:;" class="btn btn-xs red">Cancelled</a>';} ?></center>
												</td>

                                            </tr>
                                            <?php  
                                            } }
                                          else
                                          {?>
                                         <tr class="even pointer">
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ><center><?php echo "Record not found";?></center></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                         </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
           
        </div>
      <?php $this->load->view("admin/footer");?>
    </body>
<script type="text/javascript">
  function changestatus(id,status)
  {  
      var str = "id="+id+"&status="+status;

      alert(str);
     
      var r = confirm('Are you really want to change status?');
      if(r==true)
      {
          $.ajax({
            type:"POST",
             url:"<?php echo base_url('promocode/change_status')?>/",
             data:str,
             success:function(data)
             {   
                 if(data==1000)
                 {
                      location.reload();
                 }
             }
          });
      }
  }
</script>
</html>