<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Menu</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
        <?php $this->load->view("admin/head.php"); ?>
       
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
            <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                           
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Menu</div>
                                    <div class="actions">
                                       <a title="Add Barber" href="<?php echo base_url('menu/manage_menu');?>" type="button" id="" class="btn white pull-right">Manage Menu<i class="fa fa-plus" aria-hidden="true"></i></a> 
                                    </div>    
                                </div>
                            </div>
                             <ul class="nav nav-tabs" id="maincategory">
                                <li id="1" class=""><a href="#1" data-toggle="tab">BREAKFAST</a></li>
                                <li id="2" class=""><a href="#2" data-toggle="tab">LUNCH</a></li>
                                <li id="3" class=""><a href="#3" data-toggle="tab">DINNER</a></li>
                            </ul>
                            <ul class="nav nav-tabs" id="weekid">
                                <li id="1" class=""><a href="#mon" data-toggle="tab">Mon</a></li>
                                <li id="2" class=""><a href="#tue" data-toggle="tab">Tue</a></li>
                                <li id="3" class=""><a href="#wed" data-toggle="tab">Wed</a></li>
                                <li id="4" class=""><a href="#thu" data-toggle="tab">Thus</a></li>
                                <li id="5" class=""><a href="#fri" data-toggle="tab">Fri</a></li>
                                <li id="6" class=""><a href="#sat" data-toggle="tab">Sat</a></li>
                                <li id="7" class=""><a href="#sun" data-toggle="tab">Sun</a></li>
                            </ul>
                            <div class="portlet-body" >
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>Product Name </center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th><center>Product Name </center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </tfoot>

                                        <tbody id="menu_data">
                                        <?php 
                                        if(!empty($menudata))
                                        {
                                            foreach($menudata as $product)
                                            { ?>
                                            <tr>
                                                  <td><center><?php echo $product->Name;?></center></td>
                                                  <td><center><a href="<?php echo base_url('product/edit/'.$product->ProductId);?>"><span class="label label-sm btn green"><i class="fa fa-pencil"></i></span></a></center></td>
                                            </tr>
                                                <?php  
                                            } }
                                              else
                                              { ?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
       
      <?php $this->load->view("admin/footer"); ?>
    </body>
<script type="text/javascript">
    $("#weekid li").click(function(){ 
        var mainids = $("#maincategory .active").attr("id");
        var weekid = this.id;
        if(typeof(mainids)==='undefined'){ mainids = 1; }
        var data = 'maincategory='+mainids+'&weekid='+weekid;
        var base_url = '<?php echo base_url(); ?>';
        url ="menu";
        $.ajax({
        type:"POST",
        url:base_url + url,
        data:data,
        dataType:'json',
        //contentType: false,
        //cache: false,
        //processData: false,
        success: function(result){
            
            //console.log(result.status);
            if(result.status ==true){
                var len = JSON.stringify(result.data.menudata.length);
                var newdata = JSON.parse(JSON.stringify(result.data.menudata));
                var i=0;
                var str = "";

                for ( i;  i < len; i++) {
                    //alert(newdata[i].Name);

                    str = str+ '<tr><td><center>'+ newdata[i].Name +' </center></td><td><center><a href="'+base_url+'product/edit/'+newdata[i].ProductId +'><span class="label label-sm btn green"><i class="fa fa-pencil"></i></span></a></center></td></tr>';
                }


                $("#menu_data").empty();
                $("#menu_data").append(str);

            }else if(result.status ==false){ 
                $("#menu_data tbody").empty();
             $("#menu_data tbody").append('<tr class="even pointer"><td class="" ></td><td class="" ><center>Record not found</center></td><td class="" ></td></tr>');
              //return true;
            }
          }
        });

    });

</script>


</html>









     


