<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Product</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
            <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper" id="view">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Product</div>
                                    <div class="actions">
                                       <a title="Add Barber" href="<?php echo base_url('product/add_product');?>" type="button" id="" class="btn white pull-right">Add Product<i class="fa fa-plus" aria-hidden="true"></i></a> 
                                    </div> 
                                    <div class="actions">
                                        <select name="categoryname" id="categoryid" class="form-control" style="margin-left: -20%">
                                            <option value="">Select Category</option>
                                            <?php 
                                                $this->db->order_by('CategoryName','ASC');
                                                $getcategory = $this->db->select('Id,CategoryName')->get('ProductCategory')->result();
                                                if(!empty($getcategory))
                                                {
                                                    foreach ($getcategory as $key) { ?>
                                                    <option value="<?php echo $key->Id; ?>"><?php echo $key->CategoryName; ?></option>
                                            <?php   }
                                                }
                                            ?>
                                        </select>
                                        <input type="hidden" value="<?php echo $this->session->userdata('cid');?>" name="" id='hidecate'>   
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                    
                                        <thead>
                                            <tr>
                                                <th><center>Product Name </center></th>
                                                <!-- <th><center>Product Category</center></th> -->
                                                <th><center>Product Price</center></th>
                                                <th><center>Product Description</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </thead>
                                      
                                        <tbody id="yyy">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
               <div class="page-content-wrapper" style="display: none;" id="edit">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Edit Product</div>
                                            </div>
                                <div class="portlet-body form">
                                <?php 
                                   if($this->session->flashdata('success'))
                                   {
                                     echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
                                   }elseif($this->session->flashdata('failed'))
                                   {
                                     echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
                                   }
                                   ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Category<span class="required">*</span></label>
                        <div class="col-md-7">
                            <select class="form-control" name="category" id="category" data-parsley-required-message="Category selection is required" required>
                            <option value="">Select Category</option>
                           <?php  
                           $getcate = $this->db->get_where("ProductCategory",array(),'CategoryName','ASC')->result(); 
                            if(!empty($getcate)) 
                                    {
                                        foreach ($getcate as $key ) {  ?>
                                                <option value="<?php echo $key->Id; ?>" ><?php echo $key->CategoryName; ?></option>
                                        <?php }
                                    }
                           ?> 
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Name<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Product Name" id="product_name" name="product_name" class="form-control" data-parsley-required-message="Product Name is required" value ="" required/>
                            <input type="hidden" name="" id='prodid'>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Price<span class="required"> * </span></label>
                        <div class="col-md-7">
                             <div class="input-group">
                           <span class="input-group-addon">
                              £
                           </span>
                            <input type="number" id="price" name="price" min="1" step="any" placeholder="Product Price" class="form-control" data-parsley-required-message="Product Price" required  value=""/></div>
                             <div id="err1" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="required" required> * </span></label>
                        <div class="col-md-7">
                            <textarea rows="5" id="desc" cols="10" class="form-control" placeholder="Description" data-parsley-required-message="Description is required" name="description" required=""></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" value="Submit">
                            <a href="<?php echo base_url();?>product"><button type="button" class="btn default">Back</button>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>
            </div>
                </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- edit page end -->
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
       
      <?php $this->load->view("admin/footer"); ?>
    </body>
    <script type="text/javascript">
    function edit(col,col1,col2,col3,col4)
    {
        document.getElementById("view").style.display = "none";
        document.getElementById("sample_2").style.display = "none";
        document.getElementById("edit").style.display="block";
        $("#prodid").val(col);
        $("#product_name").val(col1);
        $("#price").val(col2);
        $("#desc").val(col3);
        $("#category").val(col4);
        $('#hidecate').val(col4);
    }

    $('#submit').click(function(){

        var id = $('#prodid').val();
        var name = $("#product_name").val();
        var price = $("#price").val();
        var desc = $("#desc").val();
        var cate = $("#category").val();
        var data = 'proid='+id+'&name='+name+'&price='+price+'&desc='+desc+'&cate='+cate;
        var base_url = '<?php echo base_url(); ?>';
        url ="product/edit";
        $.ajax({
        type:"POST",
        url:base_url + url,
        data:data,
        // dataType:'json',
        //contentType: false,
        //cache: false,
        //processData: false,
        success: function(result){
            //console.log(result);
            //reload();
            $('#hidecate').val(result);
           }
        });
    });

    $( document ).ready(function() {
        $('#categoryid').change( function(){
        var str = "";
            id = $('#categoryid').val();
        $.ajax({
            url:"<?php echo base_url();?>product/get_product_by_category",
            type:"POST",
            data:"id="+id,
            //dataType:'json',
            success:function(result)
            {
                var arr = JSON.parse(result);
                var lenth = arr.length;
                //console.log(arr);
                var i=0;
                for ( i;  i < lenth; i++) {
                    //alert(newdata[i].Name);
                    str = str+ '<tr id="xxx"><td><center>'+ arr[i].Name +'</center></td><td><center>₹ '+arr[i].Amount+'</center></td><td><center>'+arr[i].Description+'</center></td><td><center><a onclick="edit('+arr[i].Id+',\''+arr[i].Name+'\',\''+arr[i].Amount+'\',\''+arr[i].Description+'\',\''+arr[i].CategoryId+'\')"><span class="label label-sm btn green">edit</span></button></a></td></tr>';
                }
                if(str!='')
                {
                    $('#yyy').html(str);
                }
            }
        });
    });
});
</script>
</html>








     


