<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
        <div class="clearfix"> </div>
        <div class="page-container">
           <?php $this->load->view('admin/new_sidebar1'); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Product</div>
                                            </div>
        <div class="portlet-body form">
        <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('product/add_product');?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Category<span class="required">*</span></label>
                        <div class="col-md-7">
                            <select class="form-control" name="category" data-parsley-required-message="Category selection is required" required>
                            <option value="">Select Category</option>
                           <?php    if(!empty($catedata)) 
                                    {
                                        foreach ($catedata as $key ) { ?>
                                                <option value="<?php echo $key->Id; ?>"><?php echo $key->CategoryName; ?></option>
                                        <?php }
                                    }
                           ?> 
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Name<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Product Name" name="product_name[]" class="form-control" data-parsley-required-message="Product Name is required" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Product Price<span class="required"> * </span></label>
                        <div class="col-md-7">
                         <div class="input-group">
                           <div class="input-group-addon"> ₹ </div>
                            <input type="text"  name="price[]" placeholder="Product Price" id="mobile_no" class="form-control" data-parsley-type="digits" data-parsley-required-message="Product price is required" required /></div>
                             <!-- <div id="err1" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></div> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="required" required> * </span></label>
                        <div class="col-md-7">
                            <textarea rows="2" cols="10" class="form-control" placeholder="Description" name="description[]" ></textarea>
                        </div>
                    </div>
                    <div class="input_fields_container"></div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" value="Submit" >&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="javascript:history.go(-0)"><button type="button" class="btn default">Clear</button></a>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-sm add_more_button">Add More Product</button>
                        </div>
                    </div>
                </div>
            </form>

           
        </div>
            </div>
                </div>
                    </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php $this->load->view('admin/footer'); ?>
    </body>
     <script>
    $(document).ready(function() {
    var max_fields_limit      = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_button').click(function(e){ //click event on add more fields button having class add_more_button
        e.preventDefault();
        if(x < max_fields_limit){ //check conditions
            x++; //counter increment
            $('.input_fields_container').append('<div><div style="text-align:center">************************</div><div class="form-group"><label class="control-label col-md-3">Product Name<span class="required">*</span></label><div class="col-md-7"><input type="text" placeholder="Product Name" name="product_name[]" class="form-control" data-parsley-required-message="Product Name is required" required/></div></div><div class="form-group"><label class="control-label col-md-3">Product Price<span class="required"> * </span></label><div class="col-md-7"><div class="input-group"><div class="input-group-addon"> ₹ </div><input type="text"  name="price[]" placeholder="Product Price" id="mobile_no" class="form-control" data-parsley-type="digits" data-parsley-required-message="Product price is required" required /></div><!-- <div id="err1" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></div> --></div></div><div class="form-group"><label class="control-label col-md-3">Description<span class="required" required> * </span></label><div class="col-md-7"><textarea rows="2" cols="10" class="form-control" placeholder="Description" name="description[]" ></textarea></div></div><a href="#" class="remove_field" style="margin-left: 50%;"><i class="fa fa-trash"></i></a></div>'); //add input field
        }
    });  
    $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});

</script>
</html>
