<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Appointment|Request</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container"> 
             <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Message By User
                                    </div>
                                    <!-- <div class="actions">
                                    </div>    --> 
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>User Name</center></th>
                                                <th><center>User Email</center></th>
                                                <th><center>Message</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>S.No</center></th>
                                                <th><center>User Name</center></th>
                                                <th><center>User Email</center></th>
                                                <th><center>Message</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($contactus))
                                        {   $i = 0;
                                            foreach($contactus as $key)
                                            { $i++; ?>
                                             <tr id="xxx<?php echo $key->id;?>">
                                                <td><center><?php echo $i;?></center></td> 
                                                <td><center><?php echo $key->username;?></center></td>
                                                <td><center><?php echo $key->useremail;?></center></td>
                                                <td><center><?php echo $key->message;?></center></td>
                                                <td><center><a class="btn1<?php echo $key->id;?>" onclick='deletemain("<?php echo $key->id;?>");' href="javascript:void(0);" title="click here to delete"><span class="label label-sm label-danger"><i class="fa fa-trash-o"></i></span></a> </center></td>
                                            </tr>
                                            <?php  
                                            } }
                                          else
                                          {?>
                                         <tr class="even pointer">
                                              <td class="" ></td>
                                              <td class="" ><center><?php echo "Record not found";?></center></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                              <td class="" ></td>
                                         </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
           
        </div>
      <?php $this->load->view("admin/footer");?>
    </body>
<script type="text/javascript">
function deletemain(id)
{
    var r = confirm('Are you really want to delete?');
    if(r==true)
    {
        $.ajax({
           url:"<?php echo base_url('contactus/delete')?>/"+id,
           success:function(data)
           {
              if(data==1000)
              {
                $('#xxx'+id).closest('tr').fadeOut("fast");
              }
           }
        });
    }
   
}
</script>
</html>