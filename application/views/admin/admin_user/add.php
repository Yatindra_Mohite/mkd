<!DOCTYPE html>
<html lang="en">
  <head>
    <?php $this->load->view("admin/head.php"); ?>
  </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
      
        <div class="clearfix"> </div>
      
        <div class="page-container">
           
           <?php $this->load->view('admin/new_sidebar1'); ?>
           
            <div class="page-content-wrapper">
                
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Admin</div>
                                            </div>
        <div class="portlet-body form">
         <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Name<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Name" name="name" class="form-control" data-parsley-required-message="Barber Name is required" data-parsley-pattern="^[A-Za-z ]*$" required/>
                             <?php echo form_error('b_name', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Image<span class="required"> * </span><br> <span> Size (Max 200*150 px, 200kb)</span></label>
                        <div class="col-md-7">
                            <input type="file"  name="image" id="inputfile" class="form-control" />
                            <?php if($this->session->flashdata('image_error')){ echo "<div style='color:red;list-style-type: none;font-size: 0.9em;line-height: 0.9em;'>",$this->session->flashdata('image_error'),"</div>"; }?>
                        </div>
                        <span id="file411" style="margin-left: 27%;list-style-type: none;font-size: 0.9em; line-height: 0.9em;"></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Email<span class="required"> * </span></label>
                        <div class="col-md-7">
                            <input type="text" id="email" placeholder="Email" name="email" class="form-control" data-parsley-type="email" data-parsley-required-message="Email is required" required/>
                            <div id="err" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;margin-top: 0.4em;"></div>
                            <?php echo form_error('email', "<span class='error'>", "</span>"); ?>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" value="Submit" >
                            <a href="<?php echo base_url()?>admin_user/add_admin"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
       
     <?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
  $('#form11').parsley();  
</script>

<script>
   $("#submit").click(function() {
    var fileExtension = ['jpeg', 'jpg', 'png'];
    var inputfile =  $("#inputfile").val();

   if(inputfile == '') 
   {
      $("#file411").html('Image is required').css("color", "red");
       setTimeout(function(){ 
        $('#file411').html('');
        },4000);
      return false;
   }
   else if(inputfile != '' && $.inArray($("#inputfile").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        
            //alert("Only formats are allowed : "+fileExtension.join(', '));
        $("#file411").html('Only formats are allowed :' +fileExtension.join(', ')).css("color", "red");
        setTimeout(function(){ 
        $('#file411').html('');
        },4000);
        return false;
    }


    return true;
  });
 </script>
 <script>
$(document).ready(function(){
    $("#submit").click(function(){ 
        var email  = $("#email").val();
        var str = 0;
             $.ajax({
                    type:'POST',
                    url: '<?php echo base_url();?>admin_user/check_email',
                    data: "email="+email,
                    async: false,
                    success: function(data){ 
                        if(data==10000)
                        {
                          str = 1;  
                          $("#err").html('Email already exists.').css('color','red');
                        }
                        else
                        { 
                          $("#err").html('');
                        }
                    },
        
                });
             if(str == 1)
             {
                setTimeout(function(){ 
                $('#err').html('');
                },4000);
                return false;
             }  
        });

});

</script>        
    </body>

</html>