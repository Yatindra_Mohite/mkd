<!DOCTYPE html>
<html lang="en">
    <head>
    <link href="<?php echo base_url()?>template/assets/global/css/jquery.multiselect.css" rel="stylesheet" type="text/css">
    <?php $this->load->view("admin/head.php"); ?>
    <style type="text/css">
        input,
        input::-webkit-input-placeholder {
            font-size: 10px;
            line-height: 3;
        }
    </style>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
        <div class="clearfix"> </div>
        <div class="page-container">
           <?php $this->load->view('admin/new_sidebar1'); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Validity Plan</div>
                                            </div>
        <div class="portlet-body form">
        <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success alert-dismissable'><a class='close' data-dismiss='alert' aria-label='close'>×</a><strong>Success! </strong> ",$this->session->flashdata('success'),"</div>"; 

           }elseif($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger alert-dismissable'><a class='close' data-dismiss='alert' aria-label='close'>×</a><strong>Error!</strong>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
           <div id="eror"></div>
            <form  id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                        <div class="col-cm-1"><button class="btn btn-sm add_more_button">Add More</button></div>
                        <div class="form-group">
                            <label class="control-label col-md-1" style="text-align: center; font-size: 11px;">Min Amount<span class="required">*</span></label>
                            <div class="col-md-2">
                               <input type="number" min="0" id="minamt" name="minamount[]" placeholder="Minimum amount" class="form-control" required>
                            </div>
                            <label class="control-label col-md-1" style="text-align: center; font-size: 11px;">Max Amount<span class="required">*</span></label>
                            <div class="col-md-2">
                               <input type="number" min="0" id="maxamt" name="maxamount[]" placeholder="Maximum amount" class="form-control" required>
                            </div>
                            <label class="control-label col-md-1" style="text-align: center; font-size: 11px;">Validity Day(s)<span class="required">*</span></label>
                            <div class="col-md-2">
                               <input type="number" min="0" id="dayss" name="days[]" placeholder="Total days" class="form-control" required>
                            </div>
                        </div>
                        <div class="input_fields_container"></div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" name="formsubmit" id="submit" value="Submit" >&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="javascript:history.go(0)"><button type="button" class="btn default">Clear</button></a>
                        </div>
                        
                    </div>
                </div>
            </form>
        </div>
            </div>
                </div>
                    </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php $this->load->view('admin/footer'); ?>
    </body>
    <!-- <script src="<?php //echo base_url()?>template/assets/global/plugins/jquery.min.js"></script> -->
    <!-- <script src="<?php //echo base_url()?>template/assets/global/plugins/jquery.multiselect.js"></script> -->
   
    <script>
    $(document).ready(function() {
        var max_fields_limit      = 10; //set limit for maximum input fields
        var x = 1; //initialize counter for text box
        $('.add_more_button').click(function(e){ //click event on add more fields button having class add_more_button
          
                e.preventDefault();
                if(x < max_fields_limit){ //check conditions
                    x++; //counter increment
                    $('.input_fields_container').append('<div class="form-group"><label class="control-label col-md-1" style="text-align: center; font-size: 11px;">Min Amount<span class="required">*</span></label><div class="col-md-2"><input type="number" min="0" id="minamt" name="minamount[]" placeholder="Minimum amount" class="form-control" required></div><label class="control-label col-md-1" style="text-align: center; font-size: 11px;">Max Amount<span class="required">*</span></label><div class="col-md-2"><input type="number" min="0" id="maxamt" name="maxamount[]" placeholder="Maximum amount" class="form-control" required></div><label class="control-label col-md-1" style="text-align: center; font-size: 11px;">Validity Day(s)<span class="required">*</span></label><div class="col-md-2"><input type="number" min="0" id="dayss" name="days[]" placeholder="Total days" class="form-control" required></div><button type="button" href="#" class="remove_field" style="margin-left:10px;">Remove</button></div>'); //add input field
                }
            
        });  
        $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text links
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
 });
</script>
</html>
