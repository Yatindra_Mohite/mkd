<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Verified | Barber</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
             <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    
                    <div class="row">
                        <div class="col-md-12">
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Dummy Barber</div>
                                    <div class="actions">
                                          <a href="<?php echo base_url()?>barber/add_dummy"><button type="button" class="btn green 
                                          delete pull-right"><i class="fa fa-plus"></i><span>ADD DUMMY BARBER</span></button></a>
                                    </div>    
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>Image</center></th>
                                                <th><center>Name </center></th>
                                                <th><center>Email</center></th>
                                                <th><center>Create Date</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>Image</center></th>
                                                <th><center>Name </center></th>
                                                <th><center>Email</center></th>
                                                <th><center>Create Date</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($dummy_barber))
                                        {
                                            foreach($dummy_barber as $key)
                                            { $date = substr($key->create_date,0,10);
                                              ?>
                                            <tr>
                                                <td><center><?php if($key->user_image){ $image = $key->user_image;}else{ $image  = 'default-medium.png'; };?>
                                                      <img src="<?php echo base_url('uploads/dummy_barber/'.$image); ?>" width="60px" height="60px" class="img-circle">  
                                                    </center> </td>
                                                    <td><center><?php echo $key->user_name;?>
                                                    </center></td>
                                                    <td><center><?php echo $key->user_email;?></center></td>
                                                    <td><center><span class="label label-sm label-success badge"><?php echo 'Date - '. $date;?></span> </center></td>
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
       
    </body>
</html>







     


