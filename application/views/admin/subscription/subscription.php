<!DOCTYPE html>
<html lang="en">
    <head>
    <!-- <link href="<?php //echo base_url()?>template/assets/global/css/jquery.multiselect.css" rel="stylesheet" type="text/css"> -->
    <?php $this->load->view("admin/head.php"); ?>
    <style type="text/css">
        .label {
            color: red;
        }
        input,
            input::-webkit-input-placeholder {
            font-size: 12px;
            line-height: 3;
        }
         .stt{
           text-align: center; 
           font-size: 12px;"
        }
     
    </style>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
        <div class="clearfix"> </div>
        <div class="page-container">
           <?php $this->load->view('admin/new_sidebar1'); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Subscription Plan</div>
                                            </div>
        <div class="portlet-body form">
        <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }elseif($this->session->flashdata('failed'))
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="<?php echo base_url('menu/subscription');?>" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-2 stt">Name</label>
                        <div class="col-md-3">
                           <input type="text" name="name" placeholder="Subscription Name" data-parsley-pattern="/^[a-zA-Z\s]*$/" class="form-control" data-parsley-required-message="Plan name is required" >
                        </div>
                        <label class="control-label col-md-1 stt">Main Category<span class="required">*</span></label>
                        <div class="col-md-3">
                            <select class="form-control" style="font-size:12px" name="category" id="category" data-parsley-required-message="Category selection is required" required>
                                <option value="">Select Category</option>
                                <option value="1">BREAKFAST</option>
                                <option value="2">LUNCH</option>
                                <option value="3">DINNER</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label col-md-3">Select Day<span class="required">*</span></label>
                        <div class="col-md-7">
                            <select class="form-control" name="week_name" data-parsley-required-message="Category selection is required" required>
                                <option value="">Select Day</option>
                                <option value="1">Monday</option>
                                <option value="2">Tuesday</option>
                                <option value="3">Wednesday</option>
                                <option value="4">Thusday</option>
                                <option value="5">Friday</option>
                                <option value="6">Saturday</option>
                                <option value="7">Sunday</option>
                            </select>
                        </div>
                    </div> -->
                    
                    <div class="form-group">
                        <label class="control-label col-md-2 stt">Select Product<span class="required">*</span></label>
                            <div class="col-md-3">
                                <select name="langOpt3[]" style="font-size:12px" class="form-control" id="product" required>
                                    <option value="">Select product category</option>
                                    <?php 
                                        if(!empty($category))
                                        {
                                            foreach ($category as $key) { 
                                               ?>
                                                <option value="<?php echo $key->Id; ?>"><?php echo $key->CategoryName; ?></option>
                                                <?php }
                                        }
                                    ?>
                                </select>
                                <span data-parsley-required-message="Category selection is required"> </span>
                            </div>
                        <label class="control-label col-md-1 stt">Quantity<span class="required">*</span></label>
                            <div class="col-md-3">
                               <input type="number" min="0" id="qty" name="qty[]" placeholder="Select quantity number" class="form-control" data-parsley-required-message="Quantity is required" required>
                            </div>
                        <div class="col-cm-3"><button class="btn btn-sm add_more_button">Add</button></div>
                    </div>
                    <div class="input_fields_container"></div>
                    <div class="form-group">
                        <label class="control-label col-md-2 stt">Subscription Price<span class="required">*</span></label>
                        <div class="col-md-3">
                           <input type="text" name="price" placeholder="Subscription Amount" class="form-control" data-parsley-required-message="Price is required" required>
                        </div>
                        <label class="control-label col-md-1 stt">Valid till<span class="required">*</span></label>
                        <div class="col-md-3">
                           <input type="number" min="1" name="month_count" placeholder="Validity in month count" class="form-control" data-parsley-required-message="Validity is required" required>
                        </div>
                    </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-9">
                            <input type="submit" class="btn green" id="submit" value="Submit" >&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="javascript:history.go(-0)"><button type="button" class="btn default">Clear</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
            </div>
                </div>
                    </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php $this->load->view('admin/footer'); ?>
    </body>
    <!-- <script src="<?php //echo base_url()?>template/assets/global/plugins/jquery.min.js"></script> -->
    <!-- <script src="<?php //echo base_url()?>template/assets/global/plugins/jquery.multiselect.js"></script> -->
    <script type="text/javascript">
        $('#category').change(function(){
            var cate = $('#category').val();
            if(cate==1)
            {
                $('#name').prop('required',false);
                //$('#qty').prop('required',false);
                $('#product').prop('required',false);
            }else
            {
                $('#name').prop('required',true);
                //$('#qty').prop('required',true);
                $('#product').prop('required',true);
            }
        });


       // $('#langOpt3').multiselect({
       //      columns: 4,
       //      placeholder: 'Select product category',
       //      search: true,
       //      selectAll: true
       //  });
    </script>
    <script>
    $(document).ready(function() {
    var max_fields_limit      = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_button').click(function(e){ //click event on add more fields button having class add_more_button
        e.preventDefault();
        if(x < max_fields_limit){ //check conditions
            x++; //counter increment
            $('.input_fields_container').append('<div class="form-group"><label class="control-label col-md-2 stt">Select Product<span class="required">*</span></label><div class="col-md-3"><select name="langOpt3[]" style="font-size:12px" class="form-control" id="" required><option value="">Select product category</option><?php if(!empty($category)){ foreach($category as $key){ ?><option value="<?php echo $key->Id; ?>"><?php echo $key->CategoryName; ?></option> <?php } } ?></select><span data-parsley-required-message="Category selection is required"></span></div><label class="control-label col-md-1 stt">Qantity<span class="required">*</span></label><div class="col-md-3"><input type="number" min="1" name="qty[]" placeholder="Select quantity number" class="form-control" ></div><a href="#" class="remove_field" style="margin-left:10px;">Remove</a></div>'); //add input field
        }
    });  
    $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
</html>
