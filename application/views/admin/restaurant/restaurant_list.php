<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Restaurant | Detail</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        <div class="page-header navbar navbar-fixed-top">
           <?php $this->load->view("admin/new_header1"); ?>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
             <?php $this->load->view("admin/new_sidebar1"); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                                <?php if($this->session->flashdata('error')){?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('error');?></span>
                                    </div>
                                <?php }?>
                                <?php if($this->session->flashdata('success')){?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span> <?php echo $this->session->flashdata('success');?></span>
                                    </div>
                                <?php }?>
                          
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Restaurant Detail</div>
                                    <div class="actions">
                                          <a href="<?php echo base_url()?>restaurant/add_restaurant"><button type="button" class="btn green 
                                          delete pull-right"><i class="fa fa-plus"></i><span>Add Restaurant</span></button></a>
                                    </div>    
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th><center>Restaurant Name</center></th>
                                                <th><center>Contact No</center></th>
                                                <th><center>Email</center></th>
                                                <th><center>Create Date</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                           <tr>
                                                <th><center>Restaurant Name</center></th>
                                                <th><center>Contact No</center></th>
                                                <th><center>Email</center></th>
                                                <th><center>Create Date</center></th>
                                                <th><center>Action</center></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        if(!empty($restaurant_data))
                                        {
                                            foreach($restaurant_data as $key)
                                            { $date = substr($key->AddedOn,0,10);
                                              ?>
                                            <tr>
                                                <td><center><?php echo $key->Name;?></center></td>
                                                <td><center><?php echo $key->Phone;?></center></td>
                                                <td><center><?php echo $key->Email;?></center></td>
                                                <td><center><span class="label label-sm label-success badge"><?php echo 'Date - '. $date;?></span> </center></td>
                                                <td><center><a href="<?php echo base_url('restaurant/edit/'.$key->Id);?>"><span class="label label-sm btn green"><i class="fa fa-pencil"></i></span></a></center></td>
                                            </tr>
                                                <?php  
                                            } }
                                          else
                                          {?>
                                        <tr class="even pointer">
                                                <td class="" ></td>
                                                <td class="" ><center><?php echo "Record not found";?></center></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                                <td class="" ></td>
                                        </tr>
                                        <?php
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
        </div>
      <?php $this->load->view("admin/footer"); ?>
       
    </body>
</html>







     


