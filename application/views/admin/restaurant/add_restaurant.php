<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("admin/head.php"); ?>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
       
       <?php $this->load->view('admin/new_header1'); ?>
        <div class="clearfix"> </div>
        <div class="page-container">
           <?php $this->load->view('admin/new_sidebar1'); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                </ul>
                                <div class="">
                                    <div class="tab-pane" id="tab_4">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Add Restaurant</div>
                                            </div>
        <div class="portlet-body form">
           <?php 
           if($this->session->flashdata('success'))
           {
             echo "<div class='alert alert-success'>",$this->session->flashdata('success'),"</div>"; 
           }else
           {
             echo "<div class='alert alert-danger'>",$this->session->flashdata('failed'),"</div>"; 
           }
           ?>
            <form action="" id="form11" class="form-horizontal form-row-seperated" method="post" enctype="multipart/form-data" data-parsley-validate='' >
                <div class="form-body">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3">Restaurant Name<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Restaurant Name" name="restaurant_Name" class="form-control" data-parsley-required-message="Restaurant Name is required" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Address<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Address" name="address" class="form-control" data-parsley-required-message="Address is required" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Email<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Email" name="emailid" class="form-control" data-parsley-required-message="Last Name is required" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Contact No<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="Mobile" id="mobile_no" name="mobileno" class="form-control" data-parsley-required-message="Last Name is required" required/>
                            <div id="err" style="list-style-type: none;font-size: 0.9em; line-height: 0.9em;"></div>

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">State<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="State" name="state" class="form-control" data-parsley-required-message="Last Name is required" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">City<span class="required">*</span></label>
                        <div class="col-md-7">
                            <input type="text" placeholder="City" name="city" class="form-control" data-parsley-required-message="Last Name is required" required/>
                        </div>
                    </div>
                   
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" id="submit" value="Submit" >
                            <a href="javascript:history.go(-1)"><button type="button" class="btn default">Cancel</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     <?php $this->load->view('admin/footer'); ?>
    </body>

</html>
